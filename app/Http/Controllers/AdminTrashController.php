<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Sound;
use App\Playlist;
use App\Coupon;
use App\Course;
use App\Group;
use App;

class AdminTrashController extends Controller {
	
    public function __construct() {
        $this->middleware('auth');
    }
	
	public function restore($locale, $table, $id) {
		
		App::setLocale($locale);
		
		if ($table == 'users') {
			$hash = 'users';
            if ($rec = User::withoutGlobalScope('trash')->scopeusers()->find($id)) {
                $rec->coupons()->withoutGlobalScope('trash')->update(['trash' => 0]);
                $rec->categories()->withoutGlobalScope('trash')->update(['trash' => 0]);
                $rec->courses()->withoutGlobalScope('trash')->update(['trash' => 0]);
                $rec->playlists()->withoutGlobalScope('trash')->update(['trash' => 0]);
            }
		}
		if ($table == 'emp') {
			$hash = 'emp';
			$rec = User::withoutGlobalScope('trash')->scopeemployee()->find($id);
		}
		if ($table == 'cats') {
			$hash = 'cats';
			$rec = Category::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'sounds') {
            if(!Auth::user()->isAdmin()){
                abort(404);
            }
			$hash = 'sounds';
			$rec = Sound::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'playlist') {
			$hash = 'playlist';
			$rec = Playlist::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'coupon') {
			$hash = 'coupon';
			$rec = Coupon::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'courses') {
			$hash = 'courses';
			$rec = Course::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'groups') {
			$hash = 'groups';
			$rec = Group::withoutGlobalScope('trash')->find($id);
		}
		
		$rec->trash = false;
		$rec->save();
		
		return redirect('/'.App::getLocale().'/admin/trash/#'.$hash)->with('success', @trans('trash.restored'));
		
	}
	
	public function delete($locale, $table, $id) {
		
		App::setLocale($locale);
		
		if ($table == 'users') {
			$hash = 'users';
			$rec = User::withoutGlobalScope('trash')->scopeusers()->find($id);
			$rec->groups()->detach();
		}
		if ($table == 'emp') {
			$hash = 'emp';
			$rec = User::withoutGlobalScope('trash')->scopeemployee()->find($id);
		}
		if ($table == 'cats') {
			$hash = 'cats';
			$rec = Category::withoutGlobalScope('trash')->find($id);
			$rec->courses()->detach();
		}
		if ($table == 'sounds') {
            if(!Auth::user()->isAdmin()){
                abort(404);
            }
			$hash = 'sounds';
			$rec = Sound::withoutGlobalScope('trash')->find($id);
			$rec->playlists()->detach();
			$rec->courses()->detach();
		}
		if ($table == 'playlist') {
			$hash = 'playlist';
			$rec = Playlist::withoutGlobalScope('trash')->find($id);
			$rec->sounds()->detach();
		}
		if ($table == 'coupon') {
			$hash = 'coupon';
			$rec = Coupon::withoutGlobalScope('trash')->find($id);
		}
		if ($table == 'courses') {
			$hash = 'courses';
			$rec = Course::withoutGlobalScope('trash')->find($id);
			$rec->categories()->detach();
			$rec->sounds()->detach();
		}
		if ($table == 'groups') {
			$hash = 'groups';
			$rec = Group::withoutGlobalScope('trash')->find($id);
			$rec->users()->detach();
		}
		
		$rec->trash = false;
		$rec->delete();
		
		return redirect('/'.App::getLocale().'/admin/trash/#'.$hash)->with('success', @trans('trash.restored'));
		
	}

	public function index($locale) {
		
		App::setLocale($locale);

		$emp = User::withoutGlobalScope('trash')->scopeemployee()->where(['trash' => true])->orderBy('id', 'desc')->get();
		$users = User::withoutGlobalScope('trash')->scopeusers()->where(['trash' => true])->orderBy('id', 'desc')->get();
		$coupon = Coupon::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		$cats = Category::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		$courses = Course::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		$sounds = Sound::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		$playlist = Playlist::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		$groups = Group::withoutGlobalScope('trash')->where(['trash' => true])->orderBy('id', 'desc')->get();
		
		/* */
		$return = [
		
			'emp' => $emp,
			'users' => $users,
			'coupon' => $coupon,
			'cats' => $cats,
			'courses' => $courses,
			'sounds' => Auth::user()->isAdmin() ? $sounds : new Sound(),
			'playlist' => $playlist,
			'groups' => $groups,
			'page_title' => @trans('trash.title'),
		
		];
		
		return view('trash', $return);
		
	}
	
}
