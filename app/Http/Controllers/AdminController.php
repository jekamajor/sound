<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Sound;
use App\Playlist;
use App\Group;
use App\Course;
use App\Coupon;
use App;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function delete($locale, $table, $id)
    {
        App::setLocale($locale);

        $rec = false;

        switch ($table) {
            // когда удаляем юзера - нужно что б все его созданные Категории / Курсы / Плейлисты тоже автоматически улетали в корзину
            case 'users': //
                if ($rec = User::scopeusers()->find($id)) {
                    $rec->coupons()->update(['trash' => 1, 'status' => 'ended']);
                    $rec->categories()->update(['trash' => 1, 'status' => 'deleted']);
                    $rec->courses()->update(['trash' => 1, 'status' => 'deleted']);
                    $rec->playlists()->update(['trash' => 1, 'status' => 'deleted']);
                    $msg = @trans('common.del_user');
                }
                break;

            case 'cats':
                if ($rec = Category::find($id)) {
                    $rec->status = 'deleted';
                    $msg = @trans('common.del_cat');
                }
                break;

            case 'sounds':
                if ($rec = Sound::find($id)) {
                    $rec->status = 'deleted';
                    $msg = @trans('common.del_sound');
                }
                break;

            case 'playlist':
                if ($rec = Playlist::find($id)) {
                    $rec->status = 'deleted';
                    $msg = @trans('common.del_playlist');
                }
                break;

            case 'groups':
                if ($rec = Group::find($id)) {
                    $rec->status = 'deleted';
                    $msg = @trans('common.del_group');
                }
                break;

            case 'courses':
                if ($rec = Course::find($id)) {
                    $rec->status = 'deleted';
                    $msg = @trans('common.del_course');
                }
                break;

            case 'coupon':
                if ($rec = Coupon::find($id)) {
                    $rec->status = 'ended';
                    $msg = @trans('common.del_coupon');
                }
                break;
        }

        if ($rec) {
            $rec->trash = 1;
            $rec->save();
            return redirect('/' . App::getLocale() . '/admin/' . $table)->with('success', $msg);
        }

        return redirect('/' . App::getLocale() . '/admin/')->with('error', @trans('common.del_err'));
    }

}
