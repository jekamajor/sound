<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Playlist;
use App\Sound;
use App\Coupon;
use App\Group;
use App\Role;
use App\Course;
use App;

class AdminUsersController extends Controller {
	
    public function __construct() {
        $this->middleware('auth');
        Coupon::checkAllEndedCoupons();
    }
	
	public function deletefg($locale, $group_id, $user_id) {

        App::setLocale($locale);

        $redirectUrl = '/'.App::getLocale().'/admin/groups/edit/'.$group_id;

		$user = User::scopeUsersAndEmployee()->find($user_id);
		if (!$user) {
			return redirect($redirectUrl)->with('error', @trans('users.notfound'));
		}

		$deleted = $user->deleteGroup($group_id);

		return redirect($redirectUrl)->with(
		    $deleted ? 'success' : 'error',
            $deleted ? @trans('groups.u_deleted') : @trans('groups.u_cannot_delete')
        );
		
	}
	
    public function dash($locale = 'ru', Request $request) {
		
		App::setLocale($locale);
		Coupon::getAllSettings();
		
		/* Список пользователей */
		$order_by = 'id';
		$order = 'desc';
		
		$list = User::scopeusers()->orderBy($order_by, $order)->get();
		$opts = [];
		$search = '';
		
		if ($request->input('nr')) {
			
			$sq = $request->input('nr');
			$sq = urldecode($sq);
			
			$list = User::scopeusers()->where('name', 'LIKE', '%'.$sq.'%');
			$list = $list->orWhere('phone', 'LIKE', '%'.$sq.'%');
			$list = $list->orWhere('email', 'LIKE', '%'.$sq.'%');
			$list = $list->get();

			$search = $sq;
			
		}
		
		if ($list->count()) {
			foreach ($list as $user) {
				
				/* Купон / Подписка пользователя */
				$opts[$user->id]['date_expires'] = '&mdash;';
				$opts[$user->id]['coupon_status'] = '&mdash;';
				$opts[$user->id]['date_activated'] = '&mdash;';
				$opts[$user->id]['names'] = null;
				$opts[$user->id]['price'] = null;
				$opts[$user->id]['coupon_id'] = null;
				$opts[$user->id]['coupon_nr'] = null;
				
				$oby = 'id';
				$o = 'desc';
				
				if ($request->input('order_by') && $request->input('order')) {
					
					$oby = $request->input('order_by');
					$o = $request->input('order');
					
				}
				
				$coupon = Coupon::findActualCoupon($user->id);
			
				if ($coupon) {
					
					/* Цены и названия купонов */
					$opts[$user->id]['date_expires'] = $coupon->date_expires;
					$opts[$user->id]['coupon_status'] = $coupon->status;
					$opts[$user->id]['date_activated'] = $coupon->date_activated;
					$opts[$user->id]['date_expires'] = $coupon->date_expires;
					$opts[$user->id]['name'] = $coupon->packname;
					$opts[$user->id]['price'] = $coupon->packprice;
					$opts[$user->id]['coupon_nr'] = $coupon->number;
					$opts[$user->id]['coupon_id'] = $coupon->id;
				
				}
				
				$opts[$user->id]['invited'] = $user->invitedLink;
			
			}
			
		}
						
		/* */
		$return = [
		
			'page_title' => @trans('dashboard.title'),
			
			'list' => $list,
			'opts' => $opts,
			'search' => $search,
			'order_by' => $order_by,
			'order' => $order,
		
		];
		
        return view('dashboard', $return);
		
    }
	
	/* Группы */
	public function index_groups($locale, Request $request) {
		
		App::setLocale($locale);

		$list = Group::with('owner')->orderBy('id', 'desc')->get();

		/* */
		$return = [
		
			'page_title' => @trans('groups.title'),
			'list' => $list,
			'order_by' => 'id',
			'order' => 'desc',
			
		];
		
		return view('groups', $return);
		
	}
	
	public function add_g($locale, Request $request) {
		
		App::setLocale($locale);
		
		$rec = [
		
			'name' => '',
			'status' => 'active',
			
		];
		$rec = (object)$rec;
		
		if ($request->isMethod('post')) {
			
			/* Правила валидации */
			$rules = [
				'name' => ['required'],			
			];

			$validator_msg = [ 
				'name.required' => @trans('groups.name.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			$rec = new Group;
			
			$rec->created_by = Auth()->user()->id;
			$rec->name = $request->input('name');
			$rec->status = $request->input('status');
			
			$rec->save();
			
			if ($request->input('users')) {

				if (is_array($request->input('users'))) {

				    $r_users = explode(',', $request->input('users')[0]);
					foreach ($r_users as $uid) {

						$this_user = User::find($uid);
						if (!$this_user) {
							continue;
						}

						$this_user->addGroup($rec->id);
						
					}
					
				}
			}
		
			
			return redirect('/'.App::getLocale().'/admin/groups')->with('success', @trans('groups.added'));
			
		}
		
		/* */
		
		$return = [
		
			'page_title' => @trans('groups.title_add'),
			'rec' => $rec,
			'usersToAddInGroup' => User::scopeUsersAndEmployee()->orderBy('name', 'asc')->get(),
			
		];
		
		return view('group_form', $return);
	
	}
	
	public function edit_g($locale, $id, Request $request) {
		
		App::setLocale($locale);

		$rec = Group::find($id);

		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin/groups')->with('error', @trans('groups.notfound'));
		}
		
		if ($request->isMethod('post')) {

			/* Правила валидации */
			$rules = [
				'name' => ['required'],			
			];

			$validator_msg = [ 
				'name.required' => @trans('groups.name.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
						
			$rec->name = $request->input('name');
			$rec->status = $request->input('status');
			
			$rec->save();
			$rec->touch(); 
						
			if ($request->input('users')) {
				if (is_array($request->input('users'))) {
					
					$r_users = explode(',', $request->input('users')[0]);
			
					foreach ($r_users as $uid) {
						
						$this_user = User::find($uid);
						if (!$this_user) {
							continue;
						}

                        $this_user->addGroup($id);
						
					}
			
				}
			}
			
			return redirect('/'.App::getLocale().'/admin/groups/info/'.$id)->with('success', @trans('groups.updated'));
			
		}

		/* */
		$return = [
		
			'page_title' => @trans('groups.title_edit'),
			'rec' => $rec,
			'id' => $id,
			'usersToAddInGroup' => User::scopeUsersAndEmployee()->orderBy('name', 'asc')->get(),
			'list1' => User::scopeUsersAndEmployee()->whereHas('groups', function($q) use ($id){
                $q->where('group_id', $id);
            })->get()
			
		];
		
		return view('group_form', $return);
	
	}
	
	public function info_g($locale, $id) {
		
		App::setLocale($locale);

		$rec = Group::withoutGlobalScopes()->with('users')->with('owner')->find($id); // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin/groups')->with('error', @trans('groups.notfound'));
		}

		$return = [
		
			'page_title' => @trans('groups.one').' '.$rec->name,
			'rec' => $rec,

		];
		
		return view('group_info', $return);
		
	}
	
	/* Сотрудники */
	public function emp_index($locale, Request $request) {

		App::setLocale($locale);

		/* Список сотрудников */
		$order_by = 'id';
		$order = 'desc';
		
		if ($request->input('order_by')) {
			
			$order_by = $request->input('order_by');
			$order = $request->input('order');
			
		}

		$list = User::scopeemployee($includeMyself = false)->orderBy($order_by, $order)->get();
		$search = '';

		if ($request->input('nr')) {

			$search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			// поиск по имени сотрудника или email
			$list1 = User::scopeemployee()
                ->where(function ($query) use($searchQuery){
                    $query->where('name', 'LIKE', $searchQuery)
                        ->orWhere('email', 'LIKE', $searchQuery);
                })->where('id', '!=', 15)->orderBy($order_by, $order)->get();

			// поиск по коду сотрудника
            $list2 = $list->filter(function ($value, $key) use($search) {
                return stripos($value->empcode, $search) !== false;
            });

            $list = $list1->merge($list2);

			// поиск по админу
            if(stripos(config('app.officialName'), $search) !== false){
			    $list3 = User::scopeemployee($includeMyself = false)->where('id', 15)->get();
			    $list = $list->merge($list3);
            }
					
		}

		/* */
		$return = [
		
			'page_title' => @trans('emp.title'),
			
			'list' => $list,
			'search' => $search,
			'order_by' => $order_by,
			'order' => $order,
		
		];
		
        return view('emp', $return);
		
	}
	
	public function add_emp($locale, Request $request) {
		
		App::setLocale($locale);
		
		$rec = [
		
			'role' => 'emp',
			'parent_id' => '',
			'city_id' => '',
			'address' => '',
			'birth' => '',
			'last_name' => '',
			'name' => '',
			'email' => '',
			'phone' => '',
			'active' => 1,
			'emp_status' => 'active',
			'groups' => '',
			'role_id' => '',
			
		];
		$rec = (object)$rec;

		/* Сохранение данных */
		if ($request->isMethod('post')) {

		    $rolesCanCreate = Auth::user()->rolesCanCreate()->pluck('id')->toArray();
			
			/* Правила валидации */
			$rules = [
			
				'name' => ['required'],
				'email' => ['required', 'email', 'unique:users,email'],
				'emp_status' => ['required'],
				'roleparent' => ['required', 'regex:/^['.implode('', $rolesCanCreate).']{1}:[0-9]+$/'],
				'password' => ['required', 'min:6', 'confirmed'],

			];
			
			$validator_msg = [ 
			
				'name.required' => @trans('emp.name.required'),
				'email.required' => @trans('emp.email.required'),
				'email.email' => @trans('emp.email.email'),
				'email.unique' => @trans('emp.email.unique'),
				'min' => @trans('emp.min'),
				'confirmed' => @trans('emp.confirmed'),
				'password.required' => @trans('emp.password.required'),
				'roleparent.required' => @trans('emp.role_id.required'),
				'roleparent.regex' => @trans('emp.role_id.required'),

			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			$rec = new User;

			// $role_id - роль, которую собираемся присвоить новому сотруднику (уже проверена на валидность выше)
			// $parent_id - id родителя, которому собираемся присвоить нового сотрудника (родителя еще нужно проверить)
			list($role_id, $parent_id) = explode(':', $request->input('roleparent'));

			// новый сотрудник создается под текущим юзером
			if($parent_id == Auth::user()->id) {
                $parentUser = Auth::user();
            }
            // если новый сотрудник создается не под текущим юзером, а под другим сотрудником, проверяем, чтобы
            // этот другой сотрудник был потомком текущего юзера
            elseif (Auth::user()->empChildrenExist($parent_id)) {
			    $parentUser = User::where('id', $parent_id)->where('role', 'emp')->first();
            }
            else {
			    return redirect('/' . App::getLocale() . '/admin/emp/add')->with('error', @trans('emp.parent.error'));
            }
            if(!$parentUser) {
                return redirect('/' . App::getLocale() . '/admin/emp/add')->with('error', @trans('emp.parent.error'));
            }
            if($parentUser->role_id >= $role_id) {
                return redirect('/' . App::getLocale() . '/admin/emp/add')->with('error', @trans('emp.parent.error'));
            }

            if ($parentUser->branch_id == '000') {
                $branch = $rec->generateUniqueBranch(); // для дистрибьютора создаем новую ветку
            } else {
                $branch = $parentUser->branch_id; // для всех остальных ветка сохраняется
            }

			$rec->role = 'emp';
			$rec->name = $request->input('name');
			$rec->email = $request->input('email');
			$rec->password = Hash::make($request->input('password'));
			$rec->active = 1;
			$rec->avatar = null;
			$rec->emp_status = $request->input('emp_status');
			$rec->created_by = Auth()->user()->id;
			$rec->role_id = $role_id;
			$rec->address = $request->input('address');
			$rec->unique_id = $rec->generateUniqueId();
            $rec->branch_id = $branch;
            $rec->parent_id = $parent_id;

			if ($request->file('avatar')) {
				$rec->avatar = $request->file('avatar')->store(
					'ava/'.rand(100000, 999999), 'ava'
				);
			}

			$rec->save();
			$rec->syncGroupsByNames(explode(',', $request->input('groups')));
			
			return redirect('/'.App::getLocale().'/admin/emp')->with('success', @trans('emp.added'));
			
		}

		/* */
		$return = [
		
			'page_title' => @trans('emp.title_add'),
			'rec' => $rec,
			'roles' => Auth::user()->rolesCanCreate(),
			'childrens' => Auth::user()->getEmpChildrensByRole(),
            'groups' => json_encode(Group::where('status', 'active')->orderBy('name', 'asc')->pluck('name')->toArray()),
            'ing' => '',

		];

		return view('empform', $return);
		
	}
	
	public function edit_emp($locale, $id, Request $request) {
		
		App::setLocale($locale);

		$rec = User::scopeemployee()->find($id);
		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin/emp')->with('error', 'Сотрудник не найден!');
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {

		    $rolesCanCreate = Auth::user()->rolesCanCreate()->pluck('id')->toArray();

		    if ($request->input('delete_avatar') == 1) {
		        $rec->avatar = '';
		        $rec->save();
		        return redirect('/'.App::getLocale().'/admin/emp/edit/'.$id)->with('success', @trans('emp.updated'));
            }
			
			/* Правила валидации */
			$rules = [
				'name' => ['required'],
				'email' => ['required', 'email', 'unique:users,email,'.$rec->id],
			];

			// роль и статус можно менять, только если мы редактируем НЕ свой собственный профайл
            if(Auth::user()->id != $id){
                $rules['roleparent'] = ['required', 'regex:/^['.implode('', $rolesCanCreate).']{1}:[0-9]+$/'];
                $rules['emp_status'] = ['required'];
            }
			
			if ($request->input('password') !== null) {
				$rules['password'] = ['required', 'min:6', 'confirmed'];
			}
			
			$validator_msg = [ 
			
				'name.required' => @trans('emp.name.required'),
				'email.required' => @trans('emp.email.required'),
				'email.email' => @trans('emp.email.email'),
				'email.unique' => @trans('emp.email.unique'),
				'emp_status.required' => @trans('emp.emp_status.required'),
				'min' => @trans('emp.min'),
				'confirmed' => @trans('emp.confirmed'),
				'password.required' => @trans('emp.password.required'),
				'roleparent.required' => @trans('emp.role_id.required'),
				'roleparent.regex' => @trans('emp.role_id.required'),
			
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

            if(!empty($request->input('roleparent'))) {

                // $role_id - роль, которую собираемся присвоить новому сотруднику (уже проверена на валидность выше)
                // $parent_id - id родителя, которому собираемся присвоить нового сотрудника (родителя еще нужно проверить)
                list($role_id, $parent_id) = explode(':', $request->input('roleparent'));

                // собираемся менять ветку или родителя (из ТЗ):
                //1) возможность повышения
                //2) возможность перекидывать с ветки на ветку - даже если все сотрудники на одной ветке - например у нас есть 2 мастера с одной ветки - у каждого мастера по 2 директора - над мастерами начальник Дистрибютор ОЕМ - ну и он взял такой и захотел перекинуть всех директоров одного мастера другому....естественно за деректорами подтягиваються и их нижние сотрудники
                if ($parent_id != $rec->parent_id || $role_id != $rec->role_id) {

                    if ($role_id > $rec->role_id) {
                        return redirect('/' . App::getLocale() . '/admin/emp/edit/' . $id)->with('error', @trans('emp.parent.downrole'));
                    }
                    if (!Auth::user()->empChildrenExist($parent_id, true)) {
                        return redirect('/' . App::getLocale() . '/admin/emp/edit/' . $id)->with('error', @trans('emp.parent.error'));
                    }
                    if ($parentUser = User::where('id', $parent_id)->where('role', 'emp')->first()) {
                        if ($parentUser->role_id >= $role_id) {
                            return redirect('/' . App::getLocale() . '/admin/emp/add')->with('error', @trans('emp.parent.error'));
                        }
                        $rec->parent_id = $parent_id;
                        $rec->role_id = $role_id;
                        if ($rec->branch_id != $parentUser->branch_id) {
                            if ($parentUser->branch_id == '000') {
                                $branch = $rec->generateUniqueBranch(); // для дистрибьютора создаем новую ветку
                            } else {
                                $branch = $parentUser->branch_id; // для всех остальных ветка сохраняется
                            }
                        }
                    }
                }
            }

			if ($request->file('avatar')) {
				$rec->avatar = $request->file('avatar')->store(
					'ava/'.rand(100000, 999999), 'ava'
				);
			}

			$rec->role = 'emp';
			$rec->name = $request->input('name');
			$rec->email = $request->input('email');
			$rec->active = 1;
			$rec->address = $request->input('address');

			if (!empty($request->input('password'))){
			    $rec->password = Hash::make($request->input('password'));
            }

			if(Auth::user()->id != $id) {
                //$rec->role_id = $role_id;
                $rec->emp_status = $request->input('emp_status');
            }

			$rec->save();
			if(isset($branch)){
			    $rec::filterUsersAndEmployee($rec->id)->update(['branch_id' => $branch]);
            }
			$rec->syncGroupsByNames(explode(',', $request->input('groups')));
			
			return redirect('/'.App::getLocale().'/admin/emp/info/'.$id)->with('success', @trans('emp.updated'));
			
		}


		/* */
		$return = [
		
			'page_title' => @trans('emp.title_edit'),
			'rec' => $rec,
			'roles' => Auth::user()->rolesCanCreate(),
            'childrens' => Auth::user()->getEmpChildrensByRole(),
			'groups' => json_encode(Group::where('status', 'active')->orderBy('name', 'asc')->pluck('name')->toArray()),
            'ing' => implode(',', $rec->groups()->pluck('name')->toArray()),
			'id' => $id,

		];
		
		return view('empform', $return);
		
	}

    public function index($locale, Request $request) {
		
		App::setLocale($locale);

		/* Список пользователей */
		$order_by = 'id';
		$order = 'desc';
		
		if ($request->input('order_by')) {
			
			$order_by = $request->input('order_by');
			$order = $request->input('order');
			
		}

		$list = User::scopeusers()->orderBy($order_by, $order)->get();

		$opts = [];
		$search = '';
		
		if ($request->input('nr')) {
			
			$sq = $request->input('nr');
			$sq = urldecode($sq);
			
			$list = User::scopeusers()->where('name', 'LIKE', '%'.$sq.'%');
			$list = $list->orWhere('phone', 'LIKE', '%'.$sq.'%');
			$list = $list->orWhere('email', 'LIKE', '%'.$sq.'%');
			$list = $list->get();

			$search = $sq;
			
		}
		
		if ($list->count()) {
			foreach ($list as $user) {
				
				/* Купон / Подписка пользователя */
				$opts[$user->id]['date_expires'] = '&mdash;';
				$opts[$user->id]['coupon_status'] = '&mdash;';
				$opts[$user->id]['date_activated'] = '&mdash;';
				$opts[$user->id]['name'] = null;

				$coupon = Coupon::findActualCoupon($user->id);

				if ($coupon) {

					$opts[$user->id]['date_expires'] = $coupon->date_expires;
					$opts[$user->id]['coupon_status'] = $coupon->status;
					$opts[$user->id]['date_activated'] = $coupon->date_activated;
					$opts[$user->id]['name'] = $coupon->packname;
				
				}
				
                $opts[$user->id]['invited'] = $user->invitedLink;
			
			}
			
		}
		
		//var_dump($opts); exit;
				
		/* */
		$return = [
		
			'page_title' => @trans('users.title'),
			
			'list' => $list,
			'opts' => $opts,
			'search' => $search,
			
			'order_by' => $order_by,
			'order' => $order,
		
		];
		
        return view('users', $return);
		
    }

    /**
     * @param $locale
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function info_emp($locale, $id) {
		
		App::setLocale($locale);

		/* Ищем сотрудника */
		$rec = User::scopeemployee()->with('groups')->find($id);

		if (!$rec) {
			return redirect('/admin/emp')->with('error', @trans('emp.notfound'));
		}
		
		$settings = DB::table('settings')->get();
		$set = [];
		
		if ($settings->count()) {
			foreach ($settings as $sett) {
				$set[$sett->name] = $sett->value;
			}
		}			
	
		/* Его сотрудники и пользователи */
		$opts = [];
		$list = User::where(['created_by' => $id, 'role' => 'user'])->orderBy('name', 'asc')->get();
		if ($list->count()) {
			foreach ($list as $user) {
				
				/* Купон / Подписка пользователя */
				$opts[$user->id]['date_expires'] = '&mdash;';
				$opts[$user->id]['coupon_status'] = '&mdash;';
				$opts[$user->id]['date_activated'] = '&mdash;';
				$opts[$user->id]['name'] = '';


				$coupon = Coupon::findActualCoupon($user->id);
			
				if ($coupon) {

					$opts[$user->id]['date_expires'] = $coupon->date_expires;
					$opts[$user->id]['coupon_status'] = $coupon->status;
					$opts[$user->id]['date_activated'] = $coupon->date_activated;
					$opts[$user->id]['name'] = $coupon->packname;
				
				}
				
                $opts[$user->id]['invited'] = $user->invitedLink;
			
			}
			
		}
	

		/* */
		$return = [
		
			'page_title' => @trans('users.title_info').$rec->email,
			'rec' => $rec,
			'list' => $list,
			'opts' => $opts,
		
		];
		
		return view('emp_info', $return);
		
	}
	
	public function info($locale, $id) {
		
		App::setLocale($locale);
		
		/* Ищем пользователя */
		$rec = User::withoutGlobalScope('trash')->scopeusers()->with('groups')->find($id); // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/admin/users')->with('error', @trans('users.notfound'));
		}

        // если мы смотрим карточку юзера в корзине, то и все его курсы-купоны-категории тоже берем из корзины //'trash' => $rec->trash


		$list = Coupon::withoutGlobalScope('trash')->where(['user_id' => $id, 'trash' => $rec->trash])->orderBy('id', 'desc')->get();
		$opts = [];
		
		if ($list->count()) {
			foreach ($list as $co) {
				
				$opts[$co->id]['price'] = $co->packprice;
				$opts[$co->id]['names'] = $co->packname;;
				
			}
		}

		Coupon::getAllSettings();
		$coupon_first = Coupon::findActualCoupon($id);
			
		/* Созданные категории */
		$list1 = Category::withoutGlobalScope('trash')->where(['user_id' => $rec->id, 'trash' => $rec->trash])->get();
		
		/* Созданные плейлисты */
		$list2 = Playlist::withoutGlobalScope('trash')->where(['user_id' => $rec->id, 'trash' => $rec->trash])->get();
		
		/* Созданные курсы */
		$list3 = Course::withoutGlobalScope('trash')->where(['user_id' => $rec->id, 'trash' => $rec->trash])->get();

		/* */
		$return = [
		
			'page_title' => @trans('users.title_info').' '.$rec->email,
			
			'rec' => $rec,
			'list' => $list,
			'opts' => $opts,
			'coupon_list' => null,
			'coupon_first' => $coupon_first,
			'list1' => $list1,
			'list2' => $list2,
			'list3' => $list3,
		
		];
				
		return view('user_info', $return);
		
	}
	
	public function add($locale, Request $request) {
		
		App::setLocale($locale);
		
		$rec = [
		
			'role' => 'user',
			'city_id' => '',
			'branch_id' => '',
			'address' => '',
			'last_name' => '',
			'name' => '',
			'email' => '',
			'phone' => '',
			'active' => 1,
			'comment' => '',
			'status' => '',
			'parent_id' => '',

		];
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
			
				'city_id' => ['required'],
				'last_name' => ['required'],
				'name' => ['required'],
				'email' => ['required', 'email', 'unique:users,email'],
				'phone' => ['required'],
                'password' => ['required', 'min:6', 'confirmed'],
			
			];
			
			$validator_msg = [ 
			
				'city_id.required' => @trans('users.city_id.required'),
				'last_name.required' => @trans('users.last_name.required'),
				'name.required' => @trans('users.name.required'),
				'email.required' => @trans('users.email.required'),
				'email.email' => @trans('users.email.email'),
                'email.unique' => @trans('emp.email.unique'),
				'phone.required' => @trans('users.phone.required'),
				'min' => @trans('users.min'),
				'confirmed' => @trans('users.confirmed'),
				'password.required' => @trans('users.password.required'), 
			
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */
			$new = new User;
			
			$new->role = 'user'; //$request->input('role');
			$new->city_id = $request->input('city_id');
			$new->address = $request->input('address');
			$new->last_name = $request->input('last_name');
			$new->name = $request->input('name');
			$new->email = $request->input('email');
			$new->phone = $request->input('phone');
			$new->comment = $request->input('comment');
			$new->unique_id = $new->generateUniqueId();
			$new->created_by = Auth()->user()->id;
			$new->parent_id = Auth()->user()->id;
            $new->branch_id = Auth()->user()->branch_id;
			//$new->status = $request->input('status');
			
			if ($request->input('password') !== null) {
				$new->password = Hash::make($request->input('password'));
			}
			
			$new->save();
            $new->syncGroupsByNames(explode(',', $request->input('groups')));
			
			return redirect('/'.App::getLocale().'/admin/users')->with('success', @trans('users.added'));
			
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('users.title_add'),
			'cities' => City::orderBy('name', 'asc')->get(),
			'rec' => (object)$rec,
			'groups' => json_encode(Group::where('status', 'active')->orderBy('name', 'asc')->pluck('name')->toArray()),
            'ing' => '',
			
		];
		
		return view('user_form', $return);
		
	}
	
	public function edit($locale, $id, Request $request) {
		
		App::setLocale($locale);

		$rec = User::scopeusers()->find($id);
		
		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin/users')->with('error', @trans('users.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
			
			/* Правила валидации */
			$rules = [
			
				'city_id' => ['required'],
				'last_name' => ['required'],
				'name' => ['required'],
				'email' => ['required', 'email', 'unique:users,email,'.$rec->id],
				'phone' => ['required'],
			
			];
			
			if ($request->input('password') !== null) {
				$rules['password'] = ['required', 'min:6', 'confirmed'];
			}
			
			$validator_msg = [
			
				'city_id.required' => @trans('users.city_id.required'),
				'last_name.required' => @trans('users.last_name.required'),
				'name.required' => @trans('users.name.required'),
				'email.required' => @trans('users.email.required'),
				'email.email' => @trans('users.email.email'),
                'email.unique' => @trans('emp.email.unique'),
				'phone.required' => @trans('users.phone.required'),
				'min' => @trans('users.min'),
				'confirmed' => @trans('users.confirmed'),
				'password.required' => @trans('users.password.required'),
			
			];
			 
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */
			$new = $rec;
			
			//$new->role == $request->input('role');
			$new->city_id = $request->input('city_id');
			$new->address = $request->input('address');
			$new->last_name = $request->input('last_name');
			$new->name = $request->input('name');
			$new->email = $request->input('email');
			$new->phone = $request->input('phone');
			$new->comment = $request->input('comment');
			
			if ($request->input('password') !== null) {
				$new->password = Hash::make($request->input('password'));
			}
			
			$new->save();
			$new->syncGroupsByNames(explode(',', $request->input('groups')));
			
			return redirect('/'.App::getLocale().'/admin/users')->with('success', @trans('users.updated'));
			
		}

		/* */
		$return = [
		
			'page_title' => @trans('users.updated'),
			'cities' => City::orderBy('name', 'asc')->get(),
			'id' => $id,
			'rec' => $rec,
			'groups' => json_encode(Group::where('status', 'active')->orderBy('name', 'asc')->pluck('name')->toArray()),
			'ing' => implode(',', $rec->groups()->pluck('name')->toArray()),
			'coupon' => null,
			
		];
		
		return view('user_form', $return);
		
	}
	
	public function playlist($locale, Request $request) {
		
		App::setLocale($locale);
		
		$order_by = 'id';
		$order = 'desc';
		$search = '';
		
		if ($request->input('order_by')) {
			
			$order_by = $request->input('order_by');
			$order = $request->input('order');
			
		}
		
		$playlists = Playlist::with('owner')->orderBy($order_by, $order)->get();

		if ($request->input('nr')) {

			$search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			// поиск по названию плейлиста
			$playlists1 = Playlist::with('owner')->where('name', 'LIKE', $searchQuery)->orderBy($order_by, $order)->get();

			// поиск по владельцу плейлиста
			$playlists2 = Playlist::with('owner')->whereHas('owner', function($q) use ($searchQuery){
                $q->where('last_name', 'LIKE', $searchQuery)
                    ->orWhere('name', 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(name, " ", last_name)'), 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(last_name, " ", name)'), 'LIKE', $searchQuery);
            })->where('user_id', '!=', 15)->orderBy($order_by, $order)->get();

            $playlists = $playlists1->merge($playlists2);

			// поиск по админу
            if(stripos(config('app.officialName'), $search) !== false){
			    $playlists3 = Playlist::with('owner')->where('user_id', 15)->orderBy($order_by, $order)->get();
			    $playlists = $playlists->merge($playlists3);
            }

		}
		
		/* */
		$return = [
		
			'page_title' => @trans('playlist.title'),
			'order_by' => $order_by,
			'order' => $order,
			'search' => $search,
			'list' => $playlists,
		
		];
		
		return view('playlists', $return);
		
	}
	
	public function user_playlist($locale, $user_id, Request $request) {
		
		App::setLocale($locale);
		
		$user = User::scopeusers()->find($user_id);
		
		if (!$user) {
			return redirect('/admin')->with('error', @trans('users.notfound'));
		}
		
		$playlists = Playlist::where(['user_id' => $user_id])->get();
		$opts = [];
		
		if ($playlists->count()) {
			
			foreach ($playlists as $pl) {
				
				$opts[$pl->id]['sounds'] = 0;
				
				$pl_s = json_decode($pl->sounds, true);
				if (!is_array($pl_s)) {
					continue;
				}
				$pl_s = explode(',', $pl_s[0]);
				
				$opts[$pl->id]['sounds'] = sizeof($pl_s);
				
			}
			
		}
		
		
		/* */
		$return = [
		
			'page_title' => @trans('playlist.title1'),
			'list' => $playlists,
			'opts' => $opts,
			'user' => $user,
		
		];
		
		return view('user_playlists', $return);
		
	}
	
	public function playlist_add($locale, $user_id, Request $request) {
		
		App::setLocale($locale);
		
		$user = User::scopeusers()->find($user_id);
		
		if (!$user) {
			return redirect('/admin')->with('error', @trans('users.notfound'));
		}
		
		$rec = [
		
			'name' => '',
			'status' => 'active',
			
		];
		$rec = (object)$rec;
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
				'name' => ['required'],
			];
			
			$validator_msg = [ 
				'name.required' => @trans('playlist.name.required')
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */
			$rec = new Playlist;
			
			$rec->user_id = $request->input('user_id');
			$rec->name = $request->input('name');
			$rec->sounds = json_encode($request->input('sounds'));

			$rec->save();
			
			return redirect('/'.App::getLocale().'/admin/playlist/user/'.$request->input('user_id'))->with('success', @trans('playlist.added'));
			
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('playlist.title_add'),
			'rec' => $rec,
			'user_id' => $user_id,
			'list' => User::scopeusers()->orderBy('name', 'asc')->get(),
			'user' => User::scopeusers()->find($user_id),
			'sounds' => Sound::orderBy('id', 'desc')->get(),
			//'json' => json_encode($json),
			'my_sounds' => [],
			
		];
		
		return view('playlist_form', $return);
		
	}

	public function playlist_add1($locale, Request $request) {
		
		App::setLocale($locale);
		
		$rec = [
		
			'name' => '',
			'status' => 'active',
			
		];
		$rec = (object)$rec;
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
				'name' => ['required'],
			];
			
			$validator_msg = [ 
				'name.required' => @trans('playlist.name.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */
			$rec = new Playlist;
			
			$rec->user_id = $request->input('user_id');
			$rec->name = $request->input('name');
			$rec->sounds = ''; //json_encode($request->input('sounds'));

			$rec->save();

			if (is_array($request->input('sounds'))) {
                $sounds = explode(',', $request->input('sounds')[0]);
                foreach ($sounds as $sname) {
                    $rec->addSoundByName($sname);
                }
			}
			
			if (!$request->input('inside')) {
				return redirect('/'.App::getLocale().'/admin/playlist')->with('success', @trans('playlist.added'));
			}
			else {
				return redirect('/'.App::getLocale().'/admin/users/info/'.$request->input('user_id'))->with('success', @trans('playlist.added'));
			}
			
		}

		/* */
		$return = [
		
			'page_title' => @trans('playlist.title_add'),
			'rec' => $rec,
			'list' => User::scopeusers()->orderBy('name', 'asc')->get(),
			//'sounds' => Sound::where(['trash' => 0])->orderBy('id', 'desc')->get(),
			//'my_sounds' => json_encode([], JSON_UNESCAPED_UNICODE),
			'json_sounds' => json_encode(Sound::allSoundNames(), JSON_UNESCAPED_UNICODE),
			
		];
		
		return view('playlist_form', $return);
		
	}
	
	public function playlist_edit($locale, $id, $user_id, Request $request) {
		
		App::setLocale($locale);
		
		$rec = Playlist::find($id);
		
		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin')->with('error', @trans('playlist.notfound'));
		}
		
		$user = User::scopeusers()->find($user_id);
		
		if (!$user) {
			return redirect('/'.App::getLocale().'/admin')->with('error', @trans('users.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
				'name' => ['required'],
			];
			
			$validator_msg = [ 
				'name.required' => @trans('playlist.name.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */			
			$rec->user_id = $request->input('user_id');
			$rec->name = $request->input('name');
			$rec->sounds = json_encode($request->input('sounds'));
			$rec->status = $request->input('status');
			
			$rec->save();
			$rec->touch();
			
			return redirect('/'.App::getLocale().'/admin/playlist/info/'.$id)->with('success', @trans('playlist.updated'));
			
		}
		
		
			$json = [];
		/* Звуки */
		$sounds = Sound::where(['status' => 'active'])->orderBy('id', 'desc')->get();
		if ($sounds->count()) {
			foreach ($sounds as $sou) {
				
				$json[] = $sou->name;
				
			}
		}		
		$sounds = $rec->sounds;
		
		/* */
		$return = [
		
			'page_title' => @trans('playlist.title_edit'),
			'rec' => $rec,
			'id' => $id,
			'user' => $user,
			'list' => User::scopeusers()->orderBy('name', 'asc')->get(),
			'sounds' => Sound::orderBy('id', 'desc')->get(),
			'my_sounds' => $sounds,
			'json_sounds' => json_encode($json, JSON_UNESCAPED_UNICODE),
		
		];
		
		return view('playlist_form', $return);
		
	}
	
	public function playlist_edit1($locale, $id, Request $request) {

		App::setLocale($locale);

		$rec = Playlist::find($id);
		
		if (!$rec) {
			return redirect('/admin')->with('error', @trans('playlist.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
				'name' => ['required'],
			];
			
			$validator_msg = [ 
				'name.required' => @trans('playlist.name.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */			
			$rec->user_id = $request->input('user_id');
			$rec->name = $request->input('name');
			//$rec->sounds = json_encode($request->input('sounds'));
			$rec->status = $request->input('status');
			
			$rec->save();

            if (is_array($request->input('sounds'))) {
                $soundNames = explode(',', $request->input('sounds')[0]);
                $rec->syncSoundsByNames($soundNames);
            }
			
			return redirect('/'.App::getLocale().'/admin/playlist/info/'.$id)->with('success', @trans('playlist.updated'));
			
		}
		
		$mySounds = $rec->sounds()->pluck('name')->toArray();
		
		/* */
		$return = [
		
			'page_title' => @trans('playlist.title_edit'),
			'rec' => $rec,
			'id' => $id,
			'list' => User::scopeusers()->orderBy('name', 'asc')->get(),
			'sounds' => Sound::where(['status' => 'active'])->orderBy('id', 'desc')->get(),
			'my_sounds' => json_encode($mySounds),
			'json_sounds' => json_encode(Sound::allSoundNames(), JSON_UNESCAPED_UNICODE),
		
		];
		
		return view('playlist_form', $return);
		
	}
	
	public function playlist_info($locale, $id) {
		
		App::setLocale($locale);
		
		$rec = Playlist::withoutGlobalScopes()->with('owner')->find($id); // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/'.App::getLocale().'/admin/playlist')->with('error', @trans('playlist.notfound'));
		}

		$created_by = 'не указано';
		if ($rec->user_id !== 15) {
			$created_by = '<a href="/'.App::getLocale().'/admin/users/info/'.$rec->owner['id'].'">'.$rec->owner['last_name'].' '.$rec->owner['name'].'</a>';
		}
		else {
			$created_by = '<a href="/'.App::getLocale().'/admin/users/info/15">ADMINISTRATOR</a>';
		}

		/* */
		$return = [
		
			'page_title' => @trans('playlist.one').' '.$rec->name,
			'rec' => $rec,
			'list' => $rec->sounds()->where('status', 'active')->get(),
			'created_user_name' => $created_by,
			
		];

		return view('playlist_info', $return);
		
	}
	
}