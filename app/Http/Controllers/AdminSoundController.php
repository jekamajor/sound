<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Sound;
use App;

class AdminSoundController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index($locale, Request $request) {
		
		App::setLocale($locale);

        if(!Auth::user()->isAdmin()){
            abort(404);
        }
		
		/* Список пользователей */
		$list = Sound::where(['trash' => 0])->orderBy('id', 'desc')->get();
		$opts = [];
		$search = '';
		$order_by = 'id';
		$order = 'desc';
				
		if ($request->input('nr')) {

            $search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			$list = Sound::where('name', 'LIKE', $searchQuery)
                ->orWhere('dandzy', 'LIKE', $searchQuery)
                ->orderBy('id', 'desc')->get();
		}
		
		if ($list->count()) {
			foreach ($list as $s) {
				
				$opts[$s->id] = ['category' => @trans('sounds.cat_not_selected'), 'hz' => '&mdash;'];
				
				/* Категория */
				$cat = Category::find($s->cat_id);
				if ($cat) {
					$opts[$s->id]['category'] = $cat->name;
				}
				
				/* Частоты */
				$opts[$s->id]['hz'] = $s->hz1.' - '.$s->hz5;
				
			}
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('sounds.title'),
			
			'list' => $list,
			'opts' => $opts,
			'search' => $search,
			'order_by' => $order_by,
			'order' => $order,
		
		];
		
        return view('sounds', $return);
		
    }
	
	public function add($locale, Request $request) {
		
		App::setLocale($locale);

        if(!Auth::user()->isAdmin()){
            abort(404);
        }
		
		$rec = [
		
			'name' => '',
			'dandzy' => '',
			'status' => 'active',
			'duration' => '',
			'hz1' => '',
			'hz2' => '',
			'hz3' => '',
			'hz4' => '',
			'hz5' => '',
			
		];
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
			
				'name' => ['required'],
				'dandzy' => ['required'],
				'duration' => ['required'],
				
			];
			
			$validator_msg = [ 
			
				'name.required' => @trans('sounds.name.required'),
				'dandzy.required' => @trans('sounds.dandzy.required'),
				'duration.required' => @trans('sounds.duration.required'),
				
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */
			$new = new Sound;
			
			$new->name = $request->input('name');
			$new->dandzy = $request->input('dandzy');
			$new->duration = $request->input('duration');
			$new->hz1 = $request->input('hz1');
			$new->hz2 = $request->input('hz2');
			$new->hz3 = $request->input('hz3');
			$new->hz4 = $request->input('hz4');
			$new->hz5 = $request->input('hz5');
			$new->status = $request->input('status');

			$new->save();
			
			return redirect('/'.App::getLocale().'/admin/sounds')->with('success', @trans('sounds.added'));
			
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('sounds.title_add'),
			'rec' => (object)$rec,
			'cats' => Category::orderBy('name', 'asc')->get(),
			
		];
		
		return view('sound_form', $return);
		
	}
	
	public function edit($locale, $id, Request $request) {
				
		App::setLocale($locale);

        if(!Auth::user()->isAdmin()){
            abort(404);
        }

		$rec = Sound::find($id);
		
		if (!$rec) {
			return redirect('/admin/sounds')->with('error', @trans('sounds.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
			
				'name' => ['required'],
				'dandzy' => ['required'],
				'duration' => ['required'],
				
			];
			
			$validator_msg = [ 
			
				'name.required' => @trans('sounds.name.required'),
				'dandzy.required' => @trans('sounds.dandzy.required'),
				'duration.required' => @trans('sounds.duration.required'),		
				
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			/* */			
			$rec->name = $request->input('name');
			$rec->dandzy = $request->input('dandzy');
			$rec->duration = $request->input('duration');
			$rec->hz1 = $request->input('hz1');
			$rec->hz2 = $request->input('hz2');
			$rec->hz3 = $request->input('hz3');
			$rec->hz4 = $request->input('hz4');
			$rec->hz5 = $request->input('hz5');
			$rec->status = $request->input('status');

			$rec->save();

			// (заказчик просил в Телеграме) При смене статуса на неактивный нужно:
            // удалить его полностью из плейлиста и курсов, тогда он не появится при смене статуса на активный, пока его не добавишь снова
			if($rec->status != 'active'){
			    $rec->playlists()->detach();
			    $rec->courses()->detach();
			}
			Coupon::actualizeSettingSounds(); // при изменении звука актуализируем также настройки купонов
			
			return redirect('/'.App::getLocale().'/admin/sounds')->with('success', @trans('sounds.updated'));
			
		}	
	
		/* */
		$return = [
		
			'page_title' => @trans('sounds.title_edit'),
			'id' => $id,
			'rec' => $rec,
			'cats' => Category::orderBy('name', 'asc')->get(),
			
		];
		
		return view('sound_form', $return);
		
	}
	
	public function info($locale, $id) {
		
		App::setLocale($locale);

        if(!Auth::user()->isAdmin()){
            abort(404);
        }

		$rec = Sound::withoutGlobalScopes()->find($id); // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/admin/sounds')->with('error', @trans('sounds.notfound'));
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('sounds.one').' '.$rec->name,
			'rec' => $rec,
			
		];
		
		return view('sound_info', $return);
	}
	
}
