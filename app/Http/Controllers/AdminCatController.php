<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Course;
use App;

class AdminCatController extends Controller {

    protected $order = ['id', 'name', 'owner'];
	
    public function __construct() {
        $this->middleware('auth');
    }

    public function index($locale, Request $request) {

		App::setLocale($locale);
		
		$order_by = 'id';
		$order = 'desc';
		
		/* Список категорий */
		$list = Category::with('owner')->orderBy($order_by, $order)->get();

		$search = '';
		$opts = [];
				
		if ($request->input('nr')) {

			$search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			// поиск по названию категории
			$list1 = Category::with('owner')->where('name', 'LIKE', $searchQuery)->orderBy($order_by, $order)->get();

			// поиск по владельцу категории
			$list2 = Category::with('owner')->whereHas('owner', function($q) use ($searchQuery){
                $q->where('last_name', 'LIKE', $searchQuery)
                    ->orWhere('name', 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(name, " ", last_name)'), 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(last_name, " ", name)'), 'LIKE', $searchQuery);
            })->orderBy($order_by, $order)->get();

            $list = $list1->merge($list2);

			// поиск по админу
            if(stripos(config('app.officialName'), $search) !== false){
			    $list3 = Course::with('owner')->where('official', 'on')->orderBy($order_by, $order)->get();
			    $list = $list->merge($list3);
            }
		
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('cats.title'),
			
			'list' => $list,
			'opts' => $opts,
			'search' => $search,
			'order_by' => $order_by,
			'order' => $order,
			'adminUser' => 15,
		
		];
		
        return view('categories', $return);
		
    }
	
	public function add($locale, Request $request) {
				
		App::setLocale($locale);
		
		$rec = [
		
			'user_id' => $request->input('user_id'),
			'name' => '',
			'status' => 'active',
			'created_user_id' => 0,
			'official' => 'off',
			
		];
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */

			$rules = [
			    'user_id' => 'required_if:official,',
				'name' => ['required'],
				'courses_id' => ['required'],
			];
			
			$validator_msg = [ 
			    'user_id.required_if' => @trans('cats.user_id.required'),
				'name.required' => @trans('cats.name.required'),		
				'courses_id.required' => @trans('cats.courses_id.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			if (Auth::user()->isAdmin() && $request->input('official')) {
				$off = 'on';
				$userId = 0;
			}
			else{
			    $off = 'off';
			    $userId = $request->input('user_id');
            }
			
			/* */
			$new = new Category;
			
			$new->user_id = $userId;
			$new->created_user_id = Auth()->user()->id;
			$new->official = $off;
			$new->name = $request->input('name');
			$new->status = $request->input('status');

			$new->save();

			$new->syncCoursesByNames(explode(',', $request->input('courses_id')[0]));
			
			return redirect('/'.App::getLocale().'/admin/cats')->with('success', @trans('cats.added'));
			
		}
		
		/* Курсы */
		$courses_off = Course::where(['official' => 'off', 'status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
		$courses_on = Course::where(['official' => 'on', 'status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
		$courses = Course::where(['status' => 'active'])->orderBy('name', 'asc');
		
		/* */
		$return = [
		
			'page_title' => @trans('cats.title_add'),
			'rec' => (object)$rec,
			'courses' => $courses->get(),
			'users' => User::scopeusers()->orderBy('name', 'asc')->get(),
			'json_official' => json_encode($courses_on, JSON_UNESCAPED_UNICODE),
			'json_neof' => json_encode($courses_off, JSON_UNESCAPED_UNICODE),
			'course_ids' => json_encode($courses->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE),
			
		]; 
		
		return view('category_form', $return);
		
	}
	
	public function edit($locale, $id, Request $request) {
		
		App::setLocale($locale);
				
		$rec = Category::find($id);
		if (!$rec) {
			return redirect('/admin/cats')->with('error', @trans('cats.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
			
			/* Правила валидации */
			$rules = [
			    'user_id' => 'required_if:official,',
				'name' => ['required'],
				'courses_id' => ['required'],
			];

			$validator_msg = [
			    'user_id.required_if' => @trans('cats.user_id.required'),
				'name.required' => @trans('cats.name.required'),
				'courses_id.required' => @trans('cats.courses_id.required'),
			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			if (Auth::user()->isAdmin() && $request->input('official')) {
				$off = 'on';
				$userId = 0;
			}
			else{
			    $off = 'off';
			    $userId = $request->input('user_id');
            }
			
			/* */
			$new = $rec;

			$new->user_id = $userId;
			$new->name = $request->input('name');
			$new->official = $off;
			$new->status = $request->input('status');
			
			$new->save();

			$new->syncCoursesByNames(explode(',', $request->input('courses_id')[0]));

			Coupon::actualizeSettingCats(); // при изменении категории актуализируем настройки купонов
			
			return redirect('/'.App::getLocale().'/admin/cats/info/'.$id)->with('success', @trans('cats.updated'));
			
		}	
		
		/* Курсы */
        $courses_off = Course::where(['official' => 'off', 'status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
		$courses_on = Course::where(['official' => 'on', 'status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
		$courses = Course::where(['status' => 'active'])->orderBy('name', 'asc');

		/* */
		$return = [
		
			'page_title' => @trans('cats.title_edit'),
			'id' => $id,
			'rec' => $rec,
			'courses' => $rec->courses()->pluck('name')->toArray(),
			'course_ids' => json_encode($courses->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE),
			'json_official' => json_encode($courses_on, JSON_UNESCAPED_UNICODE),
			'json_neof' => json_encode($courses_off, JSON_UNESCAPED_UNICODE),
			'users' => User::scopeusers()->orderBy('name', 'asc')->get(),
			
		];
		
		return view('category_form', $return);
		
	}
	
	public function info($locale, $id) {
		
		App::setLocale($locale);
		
		$rec = Category::withoutGlobalScopes()->with('courses')->with('owner')->find($id);
		if (!$rec) {
			return redirect('/admin/cats')->with('error', @trans('cats.notfound'));
		}

		$return = [
		
			'page_title' => @trans('cats.one').' '.$rec->name,
			'rec' => $rec,
			'users' => User::scopeusers()->orderBy('name', 'asc')->get(),
			
		];
		
		return view('category_info', $return);
	
	}
	
}
