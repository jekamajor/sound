<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Sound;
use App\Course;
use App;

class AdminCourseController extends Controller {
	
    public function __construct() {
        $this->middleware('auth');
    }

    public function index($locale, Request $request) {
		
		App::setLocale($locale);
		
		$order_by = 'id';
		$order = 'desc';
		$search = '';
		
		/* Список пользователей */
		$list = Course::with('owner')->orderBy($order_by, $order)->get();

		if ($request->input('nr')) {

			$search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			// поиск по названию курса
			$list1 = Course::with('owner')->where('name', 'LIKE', $searchQuery)->orderBy($order_by, $order)->get();

			// поиск по владельцу курса
			$list2 = Course::with('owner')->whereHas('owner', function($q) use ($searchQuery){
                $q->where('last_name', 'LIKE', $searchQuery)
                    ->orWhere('name', 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(name, " ", last_name)'), 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(last_name, " ", name)'), 'LIKE', $searchQuery);
            })->orderBy($order_by, $order)->get();

            $list = $list1->merge($list2);

			// поиск по админу
            if(stripos(config('app.officialName'), $search) !== false){
			    $list3 = Course::with('owner')->where('official', 'on')->orderBy($order_by, $order)->get();
			    $list = $list->merge($list3);
            }
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('course.title'),
			'list' => $list,
			'order_by' => $order_by,
			'order' => $order,
			'search' => $search,
		
		];
		
        return view('courses', $return);
		
    }
	
	public function add($locale, Request $request) {
		
		App::setLocale($locale);
		
		$rec = [
		
			'user_id' => $request->input('user_id'),
			'cat_id' => '',
			'name' => '',
			'status' => 'active',
			'sound_ids' => '',
			'official' => 'off',
			
		];
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
						
			/* Правила валидации */
			$rules = [
				'cat_id_official' => 'required_if:official,1',
				'cat_id_not_official' => 'required_if:official,',
                'user_id' => 'required_if:official,',
                'name' => 'required',
                'sounds_id' => 'required',
				
			];
			
			$validator_msg = [ 
			
				'cat_id_official.required_if' => @trans('course.cat_id.required'),
				'cat_id_not_official.required_if' => @trans('course.cat_id.required'),
				'user_id.required_if' => @trans('course.user_id.required'),
                'name.required' => @trans('course.name.required'),
				'sounds_id.required' => @trans('course.sounds_id.required'),
				
			];
			 
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();
			
			if (Auth::user()->isAdmin() && $request->input('official')) {
				$off = 'on';
				$categories = $request->input('cat_id_official');
				$userId = Auth()->user()->id;
			}
			else{
			    $off = 'off';
			    $categories = $request->input('cat_id_not_official');
			    $userId = $request->input('user_id');
            }
			
			/* */
			$new = new Course;
			
			$new->user_id = $userId;
			$new->created_user_id = Auth()->user()->id;
			$new->official = $off;
			$new->name = $request->input('name');
			$new->status = $request->input('status');

			$new->save();

			$new->syncSoundsByNames( explode(',', $request->input('sounds_id')) );
			$new->syncCategoriesByNames( explode(',', $categories) );
			
			return redirect('/'.App::getLocale().'/admin/courses')->with('success', @trans('course.added'));
			
		}
		
		/* Категории */
		$categoriesOfficial = Category::where(['status' => 'active', 'official' => 'on'])->orderBy('name', 'asc')->get();
		$categoriesNotOfficial = Category::where(['status' => 'active', 'official' => 'off'])->orderBy('name', 'asc')->get();


		//var_dump($json); exit;
				
		/* */
		$return = [
		
			'page_title' => @trans('course.title_add'),
			'rec' => (object)$rec,
            'users' => User::scopeusers()->where(['role' => 'user'])->orderBy('name', 'asc')->get(),

            // все звуки и категории
            'json' => json_encode(Sound::allSoundNames(), JSON_UNESCAPED_UNICODE),
			'cats_names_official' => json_encode( $categoriesOfficial->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),
			'cats_names_not_official' => json_encode( $categoriesNotOfficial->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),
            'cats_names' => json_encode( $categoriesOfficial->merge($categoriesNotOfficial)->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),

		];
				
		return view('course_form', $return);
		
	}
	
	public function edit($locale, $id, Request $request) {

		App::setLocale($locale);
				
		$rec = Course::with('owner')->find($id);
		if (!$rec) {
			return redirect('/admin/courses')->with('error', @trans('course.notfound'));
		}
		
		/* Категории */
		$categories = Category::where(['status' => 'active'])->orderBy('name', 'asc')->get();
		if ($categories) {
			foreach ($categories as $cat) {
				//$cnames
			}
		}		
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {

			/* Правила валидации */
			$rules = [

				'cat_id_official' => 'required_if:official,1',
				'cat_id_not_official' => 'required_if:official,',
                'user_id' => 'required_if:official,',
                'name' => 'required',
                'sounds_id' => 'required',

			];
			
			$validator_msg = [
			
				'cat_id_official.required_if' => @trans('course.cat_id.required'),
				'cat_id_not_official.required_if' => @trans('course.cat_id.required'),
				'user_id.required_if' => @trans('course.user_id.required'),
                'name.required' => @trans('course.name.required'),
				'sounds_id.required' => @trans('course.sounds_id.required'),
				
			];
			 
			Validator::make($request->all(), $rules, $validator_msg)->validate();

			if (Auth::user()->isAdmin() && $request->input('official')) {
				$off = 'on';
				$categories = $request->input('cat_id_official');
				$userId = Auth()->user()->id;
			}
			else{
			    $off = 'off';
			    $categories = $request->input('cat_id_not_official');
			    $userId = $request->input('user_id');
            }
			
			/* */			
			$rec->user_id = $userId;
			$rec->created_user_id = Auth()->user()->id;
			$rec->name = $request->input('name');
			$rec->status = $request->input('status');
			$rec->official = $off;

			$rec->save();

			$rec->syncSoundsByNames( explode(',', $request->input('sounds_id')) );
			$rec->syncCategoriesByNames(  explode(',', $categories)  );
			
			return redirect('/'.App::getLocale().'/admin/courses/info/'.$id)->with('success', @trans('course.updated'));
			
		}	
	
		/* Категории */
		$categoriesOfficial = Category::where(['status' => 'active', 'official' => 'on'])->orderBy('name', 'asc')->get();
		$categoriesNotOfficial = Category::where(['status' => 'active', 'official' => 'off'])->orderBy('name', 'asc')->get();

		/* Звуки */
		$mySounds = $rec->sounds()->pluck('name')->toArray();
		
		/* */
		$return = [
		
			'page_title' => @trans('course.title_edit'),
			'rec' => (object)$rec,
			'users' => User::scopeusers()->orderBy('name', 'asc')->get(),
			'id' => $id,

            // мои звуки и категории
			'sound_ids' => implode(',', $mySounds),
            'incat_official' => implode(',', $rec->categories()->where(['status' => 'active', 'official' => 'on'])->pluck('name')->toArray()),
            'incat_not_official' => implode(',', $rec->categories()->where(['status' => 'active', 'official' => 'off'])->pluck('name')->toArray()),

            // все звуки и категории
            'json' => json_encode(Sound::allSoundNames(), JSON_UNESCAPED_UNICODE),
			'cats_names_official' => json_encode( $categoriesOfficial->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),
			'cats_names_not_official' => json_encode( $categoriesNotOfficial->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),
            'cats_names' => json_encode( $categoriesOfficial->merge($categoriesNotOfficial)->pluck('name')->toArray(), JSON_UNESCAPED_UNICODE ),

		];
				
		return view('course_form', $return);
		
	}

	public function info($locale, $id) {

		App::setLocale($locale);
		
		$rec = Course::withoutGlobalScopes()->with('owner')->find($id); // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/admin/courses')->with('error', @trans('course.notfound'));
		}

		/* */
		$return = [
		
			'page_title' => @trans('course.one').' '.$rec->name,
			'rec' => $rec,
			'cats' => $rec->categories()->get(),
			'list' => $rec->sounds()->where('status', 'active')->get(),
		];
		
		return view('course_info', $return);
		
	}
	
}
