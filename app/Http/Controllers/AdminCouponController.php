<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Category;
use App\City;
use App\Sound;
use App\Coupon;

use DateTime;
use DateInterval;
use DB;
use App;

class AdminCouponController extends Controller {
	
    public function __construct() {
        $this->middleware('auth');
        Coupon::checkAllEndedCoupons();
    }
	
	public function settings($locale, Request $request) {

		App::setLocale($locale);

        if(!Auth::user()->isAdmin()){
            return redirect('/'.App::getLocale().'/admin/coupon')->with('error', @trans('common.permission_denied'));
        }

		Coupon::getAllSettings();
		Coupon::actualizeSettingCats();
		Coupon::actualizeSettingSounds();

		/* Сохранение настроек */
		if ($request->isMethod('post')) {

		    foreach(Coupon::$couponNames as $couponId => $couponName){

		        // сохраняем ID звуков для каждого купона
		        if(isset($request->input('sounds_id')[$couponName][0])){
		            $ids = Sound::where('status', 'active')
                        ->whereIn('name', explode(',', $request->input('sounds_id')[$couponName][0]))
                        ->pluck('id');
                }
                else {
		            $ids = [];
                }

                DB::table('settings')
                    ->where(['name' => $couponName.'_sounds'])
                    ->update(['value' => json_encode($ids)]);

                // сохраняем ID категорий для каждого купона
		        if(isset($request->input('cats_id')[$couponName][0])){
		            $ids = Category::where('status', 'active')
                        ->whereIn('name', explode(',', $request->input('cats_id')[$couponName][0]))
                        ->pluck('id');
                }
                else {
		            $ids = [];
                }

                DB::table('settings')
                    ->where(['name' => $couponName.'_cats'])
                    ->update(['value' => json_encode($ids)]);

                // сохранение цен
                if(isset($request->input('prices')[$couponName])) {
		            foreach(Coupon::$couponPeriods as $couponPeriod => $translate){
		                if(isset($request->input('prices')[$couponName][$couponPeriod])){
		                    $price = $request->input('prices')[$couponName][$couponPeriod];
		                    DB::table('settings')
                                ->where(['name' => $couponName . '_' . $couponPeriod])
                                ->update(['value' => $price]);
                        }
                    }
                }
            }
			
			return redirect('/'.App::getLocale().'/admin/coupon/settings')->with('success', @trans('common.settings_updated'));
			
		}

        $prices = [];
        $sounds = [];
        $cats = [];

		foreach(Coupon::$couponNames as $couponId => $couponName){

		    // цены
		    foreach(Coupon::$couponPeriods as $period => $translate){
                $prices[$couponName][$period] = Coupon::getSetting($couponName . '_' . $period);
		    }

		    // звуки
            $sounds[$couponName] = Sound::where('status', 'active')
                ->whereIn('id', json_decode(Coupon::getSetting($couponName . '_sounds')))
                ->pluck('name')
                ->toArray();

            // категории
            $cats[$couponName] = Category::where('status', 'active')
                ->whereIn('id', json_decode(Coupon::getSetting($couponName . '_cats')))
                ->pluck('name')
                ->toArray();
        }

		$allSounds = Sound::where(['status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
		$allCats = Category::where(['official' => 'on', 'status' => 'active'])->orderBy('name', 'asc')->get()->pluck('name')->toArray();

		$return = [
		
			'page_title' => 'Настройка цен',
			'prices' => $prices,
			'sounds' => $sounds,
			'cats' => $cats,
			'json' => json_encode($allSounds, JSON_UNESCAPED_UNICODE),
			'cats_names' => json_encode($allCats, JSON_UNESCAPED_UNICODE),
		
		];



		return view('settings', $return);
		
	}

    public function index($locale, Request $request) {

		App::setLocale($locale);
		Coupon::getAllSettings();
		Coupon::checkAllEndedCoupons();
		
		/* Список пользователей */
		$order_by = 'id';
		$order = 'desc';
		
		$list = Coupon::with('owner')->orderBy($order_by, $order)->get();
		$opts = [];
		$search = '';
		
		/* Поиск */
		if ($request->input('nr')) {

			$search = $request->input('nr');
			$searchQuery = '%'.urldecode($search).'%';

			// поиск по названию купона
			$list1 = Coupon::with('owner')->where('number', 'LIKE', $searchQuery)->orderBy($order_by, $order)->get();

			// поиск по владельцу купона и его email
			$list2 = Coupon::with('owner')->whereHas('owner', function($q) use ($searchQuery){
                $q->where('last_name', 'LIKE', $searchQuery)
                    ->orWhere('name', 'LIKE', $searchQuery)
                    ->orWhere('email', 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(name, " ", last_name)'), 'LIKE', $searchQuery)
                    ->orWhere(DB::raw('CONCAT(last_name, " ", name)'), 'LIKE', $searchQuery);
            })->orderBy($order_by, $order)->get();

            $list = $list1->merge($list2);
			
		}

		/* */
		$return = [
		
			'page_title' => @trans('coupon.title'),
			'search' => $search,
			'order_by' => $order_by,
			'order' => $order,
            'list' => $list,
		
		];
		
        return view('coupons', $return);
		
    }
	
	public function info($locale, $id) {

		App::setLocale($locale);
		Coupon::getAllSettings();
		
		/* Ищем купон */
		$rec = Coupon::withoutGlobalScopes()->with('owner')->find($id);  // allow to view from trash  ::withoutGlobalScopes()
		if (!$rec) {
			return redirect('/admin/coupon')->with('error', @trans('coupon.notfound'));
		}
		
		/* */
		$return = [
		
			'page_title' => @trans('coupon.infoabout').' '.$rec->number,
			'rec' => $rec,
		
		];
		
		return view('coupon_info', $return);
		
	}
	
	public function add($locale, Request $request) {
		
		App::setLocale($locale);
		
		/* Пользователи */
		$users = User::scopeusers()->orderBy('id', 'desc')->get();
		
		$rec = [
			'name' => '',
			'user_id' => '',
			'expires' => '',
			'status' => 'active',
		];
		
		if ($request->input('user_id')) {
			$rec['user_id'] = $request->input('user_id');
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {

			/* Правила валидации */
			$rules = [
			
				'pack_name' => 'required|in:'.implode(',', Coupon::$couponTitles),
				'user_id' => 'required'

			];
			
			$validator_msg = [ 
				
				'pack_name.required' => @trans('coupon.pack_name.required'),
				'pack_name.in' => @trans('coupon.pack_name.required'),
				'user_id.required' => @trans('coupon.user_id.required'),

			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			$new = new Coupon;

			$new->user_id = $request->input('user_id');
			$new->number = $request->input('number');
			$new->pack_id = Coupon::getPackIdByTitle($request->input('pack_name'));
			$new->status = $request->input('status');
			$new->expires = $request->input('date_expires');

			$new->save();

			if($request->input('status') == 'active') {
			    $new->activate();

			    // (из ТЗ) когда мы создаем юзеру еще один купон, статус старого купона должен автоматически меняться на Закончен
                $new->setAnotherCouponsEnded();
            }

			return redirect('/'.App::getLocale().'/admin/coupon')->with('success', @trans('coupon.added'));
			
		}
		
		/* Генерируем номер купона */
		$length = 10;
		$symb = 'qwertyuiopasdfghjklzxcvbnm0123456789';
		$code = '';
		
		for ($a = 0; $a < $length; $a++) {
			
			$sy = substr($symb, rand(0, strlen($symb)), 1);
			$code .= strtoupper($sy);
			
		}

		$rec = json_decode(json_encode($rec));
		$rec->packname = '';
				
		/* */
		$return = [
		
			'page_title' => @trans('coupon.title_add'),
			'rec' => $rec,
			'users' => $users,
			'code' => $code,
			
		];

		if ($request->input('user_id')) {
			$user = User::scopeusers()->find($request->input('user_id'));
		}
		
		if (isset($user)) {
			$return['user'] = $user;
		}
				
		return view('coupon_form', $return);
		
	}
	
	public function edit($locale, $id, Request $request) {
		
		App::setLocale($locale);
		
		/* Пользователи */
		$users = User::scopeusers()->orderBy('id', 'desc')->get();
				
		$rec = Coupon::find($id);
		
		if (!$rec) {
			return redirect('/admin/coupon')->with('error', @trans('coupon.notfound'));
		}
		
		/* Сохранение данных */
		if ($request->isMethod('post')) {
			
			/* Правила валидации */
			$rules = [
			
				'pack_name' => 'required|in:'.implode(',', Coupon::$couponTitles),
				'user_id' => 'required',

			];

			$validator_msg = [

				'pack_name.required' => @trans('coupon.pack_name.required'),
				'pack_name.in' => @trans('coupon.pack_name.required'),
				'user_id.required' => @trans('coupon.user_id.required'),

			];
			
			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			/* */
			$rec->user_id = $request->input('user_id');
			$rec->number = $request->input('number');
			$rec->pack_id = Coupon::getPackIdByTitle($request->input('pack_name'));
			$rec->expires = $request->input('date_expires');
			$rec->status = $request->input('status');

			$rec->save();

			if($request->input('status') == 'active') {
			    $rec->activate( );

			    // (из ТЗ) когда мы создаем юзеру еще один купон, статус старого купона должен автоматически меняться на Закончен
                $rec->setAnotherCouponsEnded();
            }
			
			return redirect('/'.App::getLocale().'/admin/coupon')->with('success', @trans('coupon.updated'));
			
		}	
	
		/* */
		$return = [
		
			'page_title' => @trans('coupon.title_edit'),
			'id' => $id,
			'users' => $users,
			'rec' => $rec,
			'user' => User::scopeusers()->find($rec->user_id),
			
		];
		
		return view('coupon_form', $return);
		
	}
	
	public function pause($locale, $id, Request $request) {

        App::setLocale($locale);
		
		if($rec = Coupon::find($id)){
		    $rec->pause();
        }

        return redirect('/'.App::getLocale().'/admin/coupon')->with('success', @trans('coupon.updated'));
		
	}
	
	public function start($locale, $id, Request $request) {

        App::setLocale($locale);

		if($rec = Coupon::find($id)) {
            $rec->activate();
            $rec->setAnotherCouponsEnded();
        }

        return redirect('/'.App::getLocale().'/admin/coupon')->with('success', @trans('coupon.updated'));
		
	}

	public function upause($locale, $id, $user_id, Request $request) {

        App::setLocale($locale);

		if($rec = Coupon::find($id)){
		    $rec->pause();
        }

        return redirect('/'.App::getLocale().'/admin/users/info/'.$user_id)->with('success', @trans('coupon.updated'));

	}

	public function ustart($locale, $id, $user_id, Request $request) {

        App::setLocale($locale);

		if($rec = Coupon::find($id)) {
            $rec->activate();
            $rec->setAnotherCouponsEnded();
        }

        return redirect('/'.App::getLocale().'/admin/users/info/'.$user_id)->with('success', @trans('coupon.updated'));

	}

}
