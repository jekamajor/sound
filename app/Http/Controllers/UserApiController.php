<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Category;
use App\City;
use App\Playlist;
use App\Sound;
use App\Coupon;
use App\Group;
use DB;

class UserApiController extends Controller
{
    public function getUserList(Request $request)
    {
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
        $list = User::orderBy('id', 'desc')->get();
		$opts = [];


		if ($list->count()) {
			foreach ($list as $user) {

				/* Купон / Подписка пользователя */
				$opts[$user->id]['date_expires'] = '&mdash;';
				$opts[$user->id]['coupon_status'] = '&mdash;';
				$opts[$user->id]['date_activated'] = '&mdash;';
				$opts[$user->id]['names'] = null;

				$coupon = Coupon::where(['user_id' => $user->id])->first();
				$names = [];

				if ($coupon) {

					$price = 0;

					/* Цены и названия купонов */
					$prl = json_decode($coupon->list, true);
					foreach ($prl as $pack) {

						if ($pack == 'advanced_pack') {
							$price += 100;
							$names[] = 'Advanced Pack';
						}
						elseif ($pack == 'beauty_health_pack') {
							$price += 150;
							$names[] = 'Beauty & Health Pack';
						}
						elseif ($pack == 'salon_pack') {
							$price += 200;
							$names[] = 'Salon Pack';
						}

					}

					$opts[$user->id]['date_expires'] = $coupon->date_expires;
					$opts[$user->id]['coupon_status'] = $coupon->status;
					$opts[$user->id]['date_activated'] = $coupon->date_activated;
					$opts[$user->id]['names'] = $names;

				}

			}

		}

		/* */
		return [
			'list' => $list,
			'opts' => $opts
		];
    }
    public function userAdd(Request $request)
    {
        $this->setLocale();
      // dd($request);
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
        $rec = [

			'role' => 'user',
			'city_id' => '',
			'address' => '',
			'last_name' => '',
			'name' => '',
			'email' => '',
			'phone' => '',
			'active' => 1,

		];

		/* Сохранение данных */
		// if ($request->isMethod('post')) {

			/* Правила валидации */
			// $rules = [
      //
			// 	'city_id' => ['required'],
			// 	'last_name' => ['required'],
			// 	'name' => ['required'],
			// 	'email' => ['required', 'email'],
			// 	'phone' => ['required'],
      //
			// ];
      //
			// if ($request->input('password') !== null) {
			// 	$rules['password'] = ['required', 'min:6', 'confirmed'];
			// }
      //
			// $validator_msg = [
      //
			// 	'city_id.required' => @trans('users.city_id.required'),
			// 	'last_name.required' => @trans('users.last_name.required'),
			// 	'name.required' => @trans('users.name.required'),
			// 	'email.required' => @trans('users.email.required'),
			// 	'email.email' => @trans('users.email.email'),
			// 	'phone.required' => @trans('users.phone.required'),
			// 	'min' => @trans('users.min'),
			// 	'confirmed' => @trans('users.confirmed'),
			// 	'password.required' => @trans('users.password.required'),
      //
			// ];
      //
			// $valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			/* */
			$new = new User;

			$new->role = 'user';
			$new->city_id = $request->input('city_id') ?? '123';
			$new->address = $request->input('address') ?? '123';
			$new->last_name = $request->input('last_name') ?? '123';
			$new->name = $request->input('name') ?? '123';
			$new->email = $request->input('email');
			$new->phone = $request->input('phone') ?? '123';
			$new->active = '1';
			$new->created_by = '1';
			$new->password = Hash::make($request->input('password'));
			$new->save();

            return [
                "code" => 200,
                "msg" => @trans('users.added'),
                "data" => $new
            ];
        // }
    }
    public function getGroupList(Request $request)
    {
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
            $list = Group::get();
            return [
                "list" => $list
            ];
    }
    public function addGroup(Request $request)
    {
        $this->setLocale();
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
            if ($request->isMethod('post')) {

                /* Правила валидации */
                $rules = [
                    'name' => ['required'],
                ];

                $validator_msg = [
                    'name.required' => @trans('groups.name.required'),
                ];

                $valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

                $rec = new Group;

                $rec->name = $request->input('name');
                $rec->status = $request->input('status');

                $rec->save();

                return [
                    "code" =>200,
                    "msg" => @trans('groups.added')
                ];

            }

            /* */
            $return = [

                'page_title' => @trans('groups.title_add'),
                'rec' => $rec,

            ];

            return view('group_form', $return);
    }
    public function getEmpList(Request $request)
    {
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
            $list = User::orderBy('id', 'desc')->get();
		    return [
			    'list' => $list,
		    ];

    }
    public function addEmp(Request $request)
    {
        $this->setLocale();
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
		/* Сохранение данных */
		if ($request->isMethod('post')) {

			/* Правила валидации */
			$rules = [

				'name' => ['required'],
				'email' => ['required'],
				'password' => ['required', 'confirmed'],
				'emp_name' => ['required'],
				'emp_status' => ['required'],

			];

			if ($request->input('password') !== null) {
				$rules['password'] = ['required', 'min:6', 'confirmed'];
			}

			$validator_msg = [

				'name.required' => @trans('emp.name.required'),
				'email.required' => @trans('emp.email.required'),
				'email.email' => @trans('emp.email.email'),
				'emp_name.required' => @trans('emp.emp_name.required'),
				'emp_status.required' => @trans('emp.emp_status.required'),
				'min' => @trans('emp.min'),
				'confirmed' => @trans('emp.confirmed'),
				'password.required' => @trans('emp.password.required'),

			];

			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			$rec = new User;

			$rec->role = 'emp';
			$rec->emp_name = $request->input('emp_name');
			$rec->name = $request->input('name');
			$rec->email = $request->input('email');
			$rec->password = Hash::make($request->input('password'));
			$rec->active = 1;
			$rec->emp_status = $request->input('emp_status');

			$rec->save();

			return [
                "code" => 200,
                "msg" => @trans('emp.added')
            ];

		}
    }
    public function getPlayList(Request $request) {
        $this->setLocale();
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }

		$order_by = 'id';
		$order = 'desc';
		$search = '';

		$playlists = Playlist::orderBy($order_by, $order)->get();
		$opts = [];

		if ($playlists->count()) {

			foreach ($playlists as $pl) {

				$opts[$pl->id]['sounds'] = 0;

				$pl_s = json_decode($pl->sounds, true);
				if (!is_array($pl_s)) {
					continue;
				}

				$opts[$pl->id]['sounds'] = sizeof($pl_s);

			}

		}

		/* */
		return [
			'page_title' => @trans('playlist.title_all'),
			'order_by' => $order_by,
			'order' => $order,
			'list' => $playlists,
			'opts' => $opts,

		];

	}
	public function addPlayList($user_id, Request $request) {
        $this->setLocale();
		// if (!$this->valiToken($request->input('id'), $request->input('token'))) {
		// 	return [
    //             "code" => 403,
    //             "msg" => "Wrong Token!"
    //         ];
		// }
		$user = User::find($user_id);

		if (!$user) {
            return [
                "code" => 400,
                "msg" => @trans('users.notfound')
            ];
		}

		/* Сохранение данных */
		if ($request->isMethod('post')) {

			/* Правила валидации */
			$rules = [
				'name' => ['required'],
			];

			$validator_msg = [
				'name.required' => @trans('playlist.name.required'),
			];

			$valid = Validator::make($request->all(), $rules, $validator_msg)->validate();

			/* */
			$rec = new Playlist;

			$rec->user_id = $request->input('user_id');
			$rec->name = $request->input('name');
			$rec->sounds = json_encode($request->input('sounds'));

			$rec->save();
			return [
                "code" => 200,
                "msg" => @trans('playlist.added')
            ];

        }
	}
	public function getToken(Request $request)
	{

		$token = $request->input('id');
		$token = md5($token);
		return [
			"code" => 200,
			"token" => $token
		];
	}
	public function valiToken($id, $token)
	{
		$tok = md5($id);
		if ($tok == $token) {
			return true;
		}
		else {
			return false;
		}
	}
  public function login(Request $r)
  {
      $this->setLocale();

    // $user = User::whereEmail($r->get('email'))->with('coupons')->first();
    // $user = Auth::attempt([]);
    if(Auth::attempt(['email' => $r->get('email'), 'password' => $r->get('password')])){
      $user = User::whereEmail($r->get('email'))->with('coupons')->first();
    // if(Hash::check($r->get('password'), $user->password)){
      return [
                "code" => 200,
                "msg" => "success",
                "data" => $user
                // "data" => User::find(Auth::id())->with('coupons')->first()
                // "data" => User::find(Auth::id())->with('coupons')->first()
            ];
    } else {
      return [
                "code" => 200,
                "msg" => @trans('users.notfound')
            ];
    }
  }

  public function getSettings()
  {
    $settings = DB::table('settings')->get();
    $new = array();
    foreach ($settings as $key => $value) {
      $new[$value->name] = $value;
      $new[$value->name]->value = preg_replace("/[^,0-9]/", '', $value->value);
    }
    return $new;
  }

  public function coupons(Request $r){
    return Coupon::where('user_id', $r->get('id'))->get();
  }

  private function setLocale()
  {
      App::setLocale('ru');
  }
}
