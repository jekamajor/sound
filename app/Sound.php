<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Sound extends Authenticatable {


    // playlists relations

    public function playlists()
    {
        return $this->belongsToMany(Playlist::class, 'playlist_sound')->withPivot('created_at');
    }


    // courses relations
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_sound')->withPivot('created_at');
    }


    // массив с именами всех звуков
    public static function allSoundNames(){
        return self::where(['status' => 'active'])->orderBy('name', 'asc')->pluck('name')->toArray();
    }


    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('trash', 0);
        });
    }
	
}
