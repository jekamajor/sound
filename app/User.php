<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class User extends Authenticatable
{
    use Notifiable;

    private $empChildrens = null; // id всех сотрудников-потомков текущего юзера - $empChildrens = [100, 105, 115]
    private $empChildrensByRole = null;  // все сотрудники-потомки текущего юзера по их ролям - $empChildrensByRole[2][] = User
    public $rights = []; // права юзера, определенные его ролью

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];


    protected $appends = ["empcode", "empcodeLink", "rolename", "invitedCode", "invitedLink", "invitedLinkName", "parentLink"];



    // дополнительные свойства класса
    public function getEmpcodeAttribute()
    {
        if($this->role != 'emp') return '';
        return '['.($this->role_id-1).']['.$this->branch_id.']['.$this->unique_id.']';
    }
    public function getEmpcodeLinkAttribute()
    {
        if($this->role != 'emp') return '';
        return '<a href="/'.Lang::locale().'/admin/emp/info/'.$this->id.'">'.$this->empcode.'</a>';
    }
    public function getRolenameAttribute()
    {
        if($this->role != 'emp') return '';
        if($role = Role::find($this->role_id)){
           return $role->name;
        }
        return '';
    }
    public function getInvitedCodeAttribute()
    {
        if($invitedUser = User::find($this->created_by)){
            return $invitedUser->empcode;
        }
        return '';
    }
    public function getInvitedLinkAttribute()
    {
        if($invitedUser = User::find($this->created_by)){
            return $invitedUser->empcodeLink;
        }
        return '';
    }
    public function getParentLinkAttribute()
    {
        if($invitedUser = User::find($this->parent_id)){
            // если это админ, пишем ADMINISTRATOR
            if(!request()->wantsJson() && $invitedUser->id == Auth::user()->id){
                return 'ВЫ';
            }
            // если это админ, пишем ADMINISTRATOR
            if($invitedUser->id == 15){
                return config('app.officialName');
            }
            // если юзер ниже по иерархии, даем ссылку
            if(Auth::user()->empChildrenExist($invitedUser->id)){
                return $invitedUser->empcodeLink;
            }
            // если юзер выше по иерархии, пишем код без ссылки
            return $invitedUser->empcode;
        }
        return '';
    }
    public function getInvitedLinkNameAttribute()
    {
        if($this->role != 'emp') return '';
        if($invitedUser = User::find($this->created_by)){
            return '<a href="/'.Lang::locale().'/admin/emp/info/'.$invitedUser->id.'">'.$invitedUser->name.'</a>';
        }
        return '';
    }



    // groups relations
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_members')->withPivot('created_at');
    }

    public function addGroup($groupId)
    {
        $group = Group::where('id', $groupId)->first();
        if (!empty($group)) {
            return $this->groups()->sync($group, false);
        }
        return 0;
    }
    public function syncGroupsByNames($names)
    {
        // когда мы редактируем группы юзера от имени сотрудника, он не видит группы, созданные сотрудниками выше по иерархии
        // из за этого после groups()->sync() скрытые группы и вовсе удалятся.
        // чтобы этого не произошло, нужно вытащить все скрытые группы и добавить их в коллекцию
        $hiddenItems = $this->groups()->withoutGlobalScope('parents')->hidden()->get(); // все привязанные группы, которые текущий юзер не может видеть
        $newItems = Group::whereIn('name', $names)->get(); // группы, которые выбрал текущий сотрудник
        $merged = $hiddenItems->merge($newItems);
        $ids = $merged->pluck('id')->toArray();
        return $this->groups()->sync($ids);
    }
    public function deleteGroup($groupId)
    {
        return $this->groups()->detach($groupId);
    }
    // coupons relations
    public function coupons()
    {
        return $this->hasMany('App\Coupon', 'user_id');
    }

    // categories relations
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    // courses relations
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    // playlists relations
    public function playlists()
    {
        return $this->hasMany(Playlist::class);
    }


    public function isAdmin()
    {
        return $this->unique_id === str_repeat('0', config('limits.user_personal_code_length'));
    }


    public function has_right($right)
    {

        $role_id = (int)$this->role_id;

        if($role_id > 0){

            // загружаем права из базы только при первом запросе
            if(empty($this->rights)){
                if($role = Role::find($role_id)){
                    $this->rights = json_decode($role->rights, true);
                }
            }

            if (in_array($right, $this->rights)) {
                return true;
            }

        }

        return false;

    }


    // возвращает коллекцию ролей, которые может создать юзер
    public function rolesCanCreate()
    {
        if($canCreate = Role::find($this->role_id)){
            return Role::whereIn('id',
                json_decode($canCreate->can_create, true)
            )->whereNotIn('id', [1,6])->get();
        }
        return new Role();
    }

    public function getAllEmployees()
    {
        return $this->id;
    }





    // получаем всех сотрудников-потомков текущего юзера рекурсивно
    private function fetchAllEmpChildrens(){
        $this->empChildrens = null;
        $this->empChildrensByRole = null;
        $this->unpackEmpChildrens($this->allEmpChildrenAccounts);
    }
    private function unpackEmpChildrens($childs){
        foreach($childs as $child){
            if($child->role != 'emp') continue;
            $this->empChildrens[] = $child->id;
            $this->empChildrensByRole[$child->role_id][] = $child;
            if(!empty($child->allEmpChildrenAccounts)){
                $this->unpackEmpChildrens($child->allEmpChildrenAccounts);
            }
        }
    }
    public function allEmpChildrenAccounts(){ // collection of recursively loaded childrens
        return $this->empChildrenAccounts()->with('allEmpChildrenAccounts');
    }
    private function empChildrenAccounts(){
        return $this->hasMany(User::class, 'parent_id', 'id');
    }
    public function getEmpChildrens($includeMyself = false){
        if(is_null($this->empChildrens)){
            $this->fetchAllEmpChildrens();
        }
        $childrens = $this->empChildrens;
        if($includeMyself){
            $childrens[] = Auth::user()->id;
        }
        return $childrens ?: [];
    }
    public function getEmpChildrensByRole(){
        if(is_null($this->empChildrensByRole)){
            $this->fetchAllEmpChildrens();
        }
        return $this->empChildrensByRole ?: [];
    }



    public function empChildrenExist($userId, $includeMyself = false){
        return in_array($userId, $this->getEmpChildrens($includeMyself));
    }



    public function generateUniqueId()
    {
        $maxI = 1000;
        // генерируем ID пока не найдем уникальное значение или пока не достигнем лимита
        do{
            $randCode = $this->randCode(4);
            $codeFound = User::where('unique_id', $randCode);
        }
        while($codeFound->count() > 0 && --$maxI > 0);

        return $randCode;
    }

    public function generateUniqueBranch()
    {
        $maxI = 1000;
        // генерируем ID пока не найдем уникальное значение или пока не достигнем лимита
        do{
            $randCode = $this->randCode(3);
            $codeFound = User::where('branch_id', $randCode);
        }
        while($codeFound->count() > 0 && --$maxI > 0);

        return $randCode;
    }


    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('trash', 0);
        });
    }


    private function randCode($len)
    {
        $uid = '';
        $dict = array_merge(range('A', 'Z'), range(0, 9));
        for($i=0; $i<$len; $i++){
            $uid .= $dict[mt_rand(0, sizeof($dict)-1)];
        }
        return $uid;
    }



    // ограничения для сотрудников

    // список юзеров (сотрудник видит только своих юзеров и юзеров своих сотрудников)
    public function scopeScopeusers($query, $includeMyself = true)
    {
        $query = $query->where('role', 'user');

        if(!Auth::user()->isAdmin()) {
            $allow_parents = Auth::user()->getEmpChildrens($includeMyself);
            $query = $query->whereIn('parent_id', $allow_parents);
        }

        return $query;
    }


    // список сотрудников (сотрудник видит только сотрудников своей ветки ниже по иерархии)
    public function scopeScopeemployee($query, $includeMyself = true)
    {
        $query = $query->where('role', 'emp');

        if(!Auth::user()->isAdmin()) {
            $allow_parents = Auth::user()->getEmpChildrens($includeMyself);
            $query = $query->whereIn('id', $allow_parents);
        }

        return $query;
    }

    // список юзеров и сотрудников ниже по иерархии текущего сотрудника
    public function scopeScopeUsersAndEmployee($query, $includeMyself = true)
    {

        if(!Auth::user()->isAdmin()) {
            $allow_parents = Auth::user()->getEmpChildrens($includeMyself);
            $query = $query
                ->whereIn('parent_id', $allow_parents)
                ->where('role', 'user')
                ->orWhereIn('id', $allow_parents)
                ->where('role', 'emp');
        }

        return $query;
    }

    // список юзеров и сотрудников ниже по иерархии сотрудника $userId
    public function scopeFilterUsersAndEmployee($query, $userId)
    {
        if($user = User::find($userId)) {
            $allow_parents = $user->getEmpChildrens(false);
            $allow_parents[] = $user->id;
            $query = $query
                ->whereIn('parent_id', $allow_parents)
                ->where('role', 'user')
                ->orWhereIn('id', $allow_parents)
                ->where('role', 'emp');
        }

        return $query;
    }

}
