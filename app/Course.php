<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class Course extends Authenticatable {

	protected $table = 'courses';


	// categories relations
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_course')->withPivot('created_at');
    }

    public function syncCategoriesByNames($names)
    {
        // когда мы редактируем курс от имени сотрудника, он не видит категории, созданные сотрудниками выше по иерархии
        // из за этого после categories()->sync() скрытые категории и вовсе удалятся.
        // чтобы этого не произошло, нужно вытащить все скрытые категории и добавить их в коллекцию
        $allItems = $this->categories()->withoutGlobalScope('parents')->get(); // все привязанные категории
        $visibleItems = $this->categories()->get(); // все привязанные категории, которые видит сотрудник
        $hiddenItems = $allItems->diff($visibleItems);
        $newItems = Category::whereIn('name', $names)->get(); // категории, которые выбрал текущий сотрудник
        $merged = $hiddenItems->merge($newItems);
        $ids = $merged->pluck('id')->toArray();
        return $this->categories()->sync($ids);
    }


    // sounds relations
    public function sounds()
    {
        return $this->belongsToMany(Sound::class, 'course_sound');
    }

    public function addSound($id)
    {
        $sound = Sound::where('id', $id)->first();
        if (!empty($sound)) {
            return $this->sounds()->sync($sound, false);
        }
        return 0;
    }

    public function addSoundByName($name)
    {
        $sound = Sound::where('name', $name)->first();
        if (!empty($sound)) {
            return $this->sounds()->sync($sound, false);
        }
        return 0;
    }

    public function syncSoundsByNames($names)
    {
        $sync = [];
        foreach($names as $soundName) {
            $sound = Sound::where('name', $soundName)->first();
            if (!empty($sound)) {
                $sync[] = $sound->id;
            }
        }
        return $this->sounds()->sync($sync);
    }


    // users relations
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    // автоматически добавляем фильтр ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where('trash', 0);
        });

        // список курсов (сотрудник видит только неофициальные курсы и курсы юзеров, принадлежащих его сотрудникам)
        static::addGlobalScope('parents', function (\Illuminate\Database\Eloquent\Builder $query) {

            if(Auth::user() and !Auth::user()->isAdmin()) {
                $allow_parents = Auth::user()->getEmpChildrens();
                $allow_parents[] = Auth::user()->id;
                $query->where('official', 'off')->whereHas('owner', function ($q) use($allow_parents){
                    $q->whereIn('parent_id', $allow_parents);
                });
            }

        });

    }

}
