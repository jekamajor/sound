<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class Playlist extends Authenticatable {
	
	protected $table = 'playlists';


	// sounds relations
    public function sounds()
    {
        return $this->belongsToMany(Sound::class, 'playlist_sound')->withPivot('created_at');
    }

    public function addSound($id)
    {
        $sound = Sound::where('id', $id)->first();
        if (!empty($sound)) {
            return $this->sounds()->sync($sound, false);
        }
        return 0;
    }

    public function addSoundByName($name)
    {
        $sound = Sound::where('name', $name)->first();
        if (!empty($sound)) {
            return $this->sounds()->sync($sound, false);
        }
        return 0;
    }

    public function syncSoundsByNames($names)
    {
        $sync = [];
        foreach($names as $soundName) {
            $sound = Sound::where('name', $soundName)->first();
            if (!empty($sound)) {
                $sync[] = $sound->id;
            }
        }
        return $this->sounds()->sync($sync);
    }


    // users relations
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where('trash', 0);
        });

        // список плейлистов (сотрудник видит только неофициальные плейлисты и плейлисты юзеров, принадлежащих его сотрудникам)
        static::addGlobalScope('parents', function (\Illuminate\Database\Eloquent\Builder $query) {

            if(!Auth::user()->isAdmin()) {
                $allow_parents = Auth::user()->getEmpChildrens();
                $allow_parents[] = Auth::user()->id;
                $query->whereHas('owner', function ($q) use($allow_parents){
                    $q->whereIn('parent_id', $allow_parents);
                });
            }

        });
    }
}
