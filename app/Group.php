<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Group extends Authenticatable {
	
	protected $table = 'groups';

	
	// users relations
    public function users()
    {
        return $this->belongsToMany(User::class, 'group_members')->withPivot('created_at');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'created_by');
    }



    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where('trash', 0);
        });

        static::addGlobalScope('parents', function (\Illuminate\Database\Eloquent\Builder $query) {

            if(!Auth::user()->isAdmin()) {
                $allow_parents = Auth::user()->getEmpChildrens();
                $allow_parents[] = Auth::user()->id;
                $query->whereIn('created_by', $allow_parents);
            }

        });
    }

    // список групп, которые сотрудник не может видеть
    public function scopeHidden($query)
    {
        if(!Auth::user()->isAdmin()) {
            $allow_parents = Auth::user()->getEmpChildrens();
            $allow_parents[] = Auth::user()->id;
            $query = $query->whereNotIn('created_by', $allow_parents);
        }

        return $query;
    }
}
