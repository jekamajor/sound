<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DateTime;
use DateInterval;

class Coupon extends Authenticatable {

	protected $table = 'coupon';
	protected $appends = ["packname", "packprice", "pack"];
	public $timestamps = false;

	static public $settings = [];
	static public $couponNames = [1 => "beauty_health_pack", 2 => "salon_pack", 3 => "advanced_pack"];
	static public $couponTitles = [1 => "Beauty & Health Pack", 2 => "Salon Pack", 3 => "Advanced Pack"];
	static public $couponPeriods = ["day" => "день", "week" => "неделя", "month" => "месяц", "year" => "год"];

	// appended attributes
    public function getPacknameAttribute()
    {
        if(!isset(self::$couponTitles[$this->pack_id])) return '';
        return self::$couponTitles[$this->pack_id];
    }

		public function getPackpriceAttribute()
    {
        $packName = isset(self::$couponNames[$this->pack_id]) ? self::$couponNames[$this->pack_id] : '';
        $price = self::getSetting($packName.'_'.$this->expires);
        return intval($price);
    }

		public function getPackAttribute()
		{
				return self::$couponNames[$this->pack_id];
		}


    // users relations
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function getPackIdByTitle($title)
    {
        return array_search($title, Coupon::$couponTitles);
    }

    // settings
    public static function getAllSettings()
    {
        // echo ' getAllSettings ';
        self::$settings = DB::table('settings')->pluck('value', 'name');
    }

    public static function getSetting($name)
    {
        if(!isset(self::$settings[$name])) return '';
        return self::$settings[$name];
    }

    // удаляем из настроек купонов все неактивные категории
    public static function actualizeSettingCats()
    {
        if(empty(self::$settings)){
            self::getAllSettings();
        }

        foreach(self::$couponNames as $couponName){

            $settingName = $couponName.'_cats';

            if($currentCats = self::getSetting($settingName)){
                $actualCats = Category::where(['official' => 'on', 'status' => 'active'])->whereIn('id', json_decode($currentCats))->pluck('id')->toArray();
                $actualCats = json_encode($actualCats);

                if($currentCats != $actualCats){
		            DB::table('settings')
                        ->where(['name' => $settingName])
                        ->update(['value' => $actualCats]);
                }
            }
        }
    }

    // удаляем из настроек купонов все неактивные звуки
    public static function actualizeSettingSounds()
    {
        if(empty(self::$settings)){
            self::getAllSettings();
        }

        foreach(self::$couponNames as $couponName){

            $settingName = $couponName.'_sounds';

            if($currentSounds = self::getSetting($settingName)){
                $actualSounds = Sound::where('status', 'active')->whereIn('id', json_decode($currentSounds))->pluck('id')->toArray();
                $actualSounds = json_encode($actualSounds);

                if($currentSounds != $actualSounds){
		            DB::table('settings')
                        ->where(['name' => $settingName])
                        ->update(['value' => $actualSounds]);
                }
            }
        }
    }

    // ищет самый акутальный купон юзера
    public static function findActualCoupon($userId)
    {
        // сначала ищем активный купон
        $coupon = Coupon::where(['user_id' => $userId, 'status' => 'active'])->first();

        if(!$coupon){
            // иначе ищем истекающий в ближайшее время купон в статусе На паузе или Неактивен
            $coupon = Coupon::where('user_id', $userId)
                ->where('status', '!=', 'ended')
                ->orderBy('date_expires', 'asc')
                ->first();
        }

        if(!$coupon){
            // иначе ищем хоть какой-нибудь купон истекающий в ближайшее время
            $coupon = Coupon::where('user_id', $userId)
                ->orderBy('date_expires', 'asc')
                ->first();
        }

        return $coupon;
    }

    // проверяет ВСЕ купоны на дату экспирации и ставит им статус Закончен
    public static function checkAllEndedCoupons()
    {
        $coupons = Coupon::withoutGlobalScopes()->whereDate('date_expires', '<=', Carbon::now())->get();
        foreach($coupons as $coupon){
            $coupon->status = 'ended';
            $coupon->save();
        }
    }

    // ставит все купоны пользователя в статус Закончет, кроме текущего
    public function setAnotherCouponsEnded()
    {
        $coupons = Coupon::withoutGlobalScopes()
            ->where('id', '!=', $this->id)
            ->where('user_id', $this->user_id)
            ->where('status', '!=', 'ended')
            ->where('status', '!=', 'na')
            ->where('status', '!=', 'pause')
            ->get();

        foreach($coupons as $coupon){
            $coupon->status = 'ended';
            $coupon->save();
        }
    }


    // активируем купон
    public function activate()
    {

        if($this->date_expires == null) { // дату истечения и активации меняем только если активируем впервые

            $date = new DateTime(date('Y-m-d H:i:s'));

            switch($this->expires){
                case 'day':
                    $date->add(new DateInterval('P1D'));
                    break;

                case 'week':
                    $date->add(new DateInterval('P7D'));
                    break;

                case 'month':
                    $date->add(new DateInterval('P1M'));
                    break;

                case 'year':
                    $date->add(new DateInterval('P365D'));
                    break;
            }

            $this->date_activated = date('Y-m-d H:i:s');
            $this->date_expires = $date->format('Y-m-d H:i:s');
        }

        $this->status = 'active';
        $this->save();
    }

    // останавливаем купон
    public function pause()
    {
        $this->status = 'pause';
        $this->save();
    }

    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where('trash', 0);
        });

        // список купонов (сотрудник видит только неофициальные купоны и купоны юзеров, принадлежащих его сотрудникам)
        // static::addGlobalScope('parents', function (\Illuminate\Database\Eloquent\Builder $query) {
				//
        //     if(!Auth::user()->isAdmin()) {
        //         $allow_parents = Auth::user()->getEmpChildrens();
        //         $allow_parents[] = Auth::user()->id;
        //         $query->whereHas('owner', function ($q) use($allow_parents){
        //             $q->whereIn('parent_id', $allow_parents);
        //         });
        //     }
				//
        // });
    }

}
