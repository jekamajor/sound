<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class Category extends Authenticatable {

    // courses relations
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'category_course')->withPivot('created_at');
    }

    public function addCourse($id)
    {
        $course = Course::where('id', $id)->first();
        if (!empty($course)) {
            return $this->courses()->sync($course, false);
        }
        return 0;
    }

    public function syncCoursesByNames($names)
    {
        // когда мы редактируем от имени сотрудника, он не видит курсы, созданные сотрудниками выше по иерархии
        // из за этого после courses()->sync() скрытые курсы и вовсе удалятся.
        // чтобы этого не произошло, нужно вытащить все скрытые курсы и добавить их в коллекцию
        $allItems = $this->courses()->withoutGlobalScope('parents')->get(); // все привязанные курсы
        $visibleItems = $this->courses()->get(); // все привязанные курсы, которые видит сотрудник
        $hiddenItems = $allItems->diff($visibleItems);
        $newItems = Course::whereIn('name', $names)->get(); // курсы, которые выбрал текущий сотрудник
        $merged = $hiddenItems->merge($newItems);
        $ids = $merged->pluck('id')->toArray();
        return $this->courses()->sync($ids);
    }



    // users relations
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    // автоматически добавляем where('trash', 0) ко всем запросам
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('trash', function (\Illuminate\Database\Eloquent\Builder $query) {
            $query->where('trash', 0);
        });

        // список категорий (сотрудник видит только неофициальные категорий и категории юзеров, принадлежащих его сотрудникам)
        static::addGlobalScope('parents', function (\Illuminate\Database\Eloquent\Builder $query) {

            if(Auth::user() and !Auth::user()->isAdmin()) {
                $allow_parents = Auth::user()->getEmpChildrens();
                $allow_parents[] = Auth::user()->id;
                $query->where('official', 'off')->whereHas('owner', function ($q) use($allow_parents){
                    $q->whereIn('parent_id', $allow_parents);
                });
            }

        });
    }

}
