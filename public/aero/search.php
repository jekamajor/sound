<?php

header('Access-Control-Allow-Origin: *');
$q = trim($_GET['query']);
$class = trim($_GET['class']); 
$json = json_decode(file_get_contents('json.json'), true);
foreach ($json as $data) {
	
	$info = explode(',', $data);
	$airport = ' '.trim($info[0]);
	$city = ' '.trim($info[1]);
	$country = ' '.trim($info[2]);
	
	if (stripos($airport, $q) > 0 or stripos($city, $q) > 0 or stripos($country, $q) > 0) {
		echo '<li><a href="javascript:void(0);" class="sel_airport '.$class.'">'.$data.'</a></li>'; 
	}
	
}

?>