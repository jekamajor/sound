<?php

/* Парсер BetCity */

/* Ссылка на Футбол. Live */
define('_LIVE_FOOTBALL', 'https://ad.betcity.ru/d/on_air/events?rev=6&ver=253&csn=ooca9s');

/* Минута, на которой нужно высылать уведомление в случае обнаружения условий */
define('_CALL_MIN', 25);

/* Тайм, который проверяем */
define('_TIME', '1-й тайм');

/* Индекс футбола */
$index = false;

/* 

/* Функция возвращает результаты в виде массива */
function get_data() {
	
	$ci = curl_init();
	
	curl_setopt($ci, CURLOPT_URL, _LIVE_FOOTBALL);
	curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, 0);
	
	$ce = curl_exec($ci);
	curl_close($ci);
	
	/* JSON */
	$json = json_decode($ce, true);
	if (!is_array($json)) {
		return false;
	}
	
	/* Ищем индекс футбола */
	if (!$index) {
		foreach ($json['reply']['sports'] as $_i => $sport) {
		
			if ($sport['name_sp'] == 'Футбол') {
				$index = $_i;
				break;
			}
		
		}
	}
	
	if (!$index) {
		return false;
	}
	
	/* Формируем массив матчей */
	$events = [];
	$games = $json['reply']['sports'][$index]['chmps'];
	
	if (!is_array($games)) {
		return false;
	}
	
	foreach ($games as $chmp_id => $chmp) {
		
		if (!is_array($chmp['evts'])) {
			continue;
		}
		
		foreach ($chmp['evts'] as $event_id => $game) {
			
			$pause = false;

			if ($game['time_name'] == 'перерыв') {
				$pause = true;
			}
			
			/* Команды */
			$team1 = $game['name_ht'];
			$team2 = $game['name_at'];
						
			/* Счёт матча */
			$team1_score = $game['sc_ev_cmx']['main'][0][0];
			$team2_score = $game['sc_ev_cmx']['main'][0][1];
			
			/* Вычисляем минуту (и секунду матча) */
			$diff = null;
			$now = new DateTime();
			$began = new DateTime($game['date_ev_str']);
			$diff = $began->diff($now);
			$hour = $diff->h;
			$minute = $diff->i;
			$second = $diff->s;
			
			if ($hour == 1) {
				$minute += 45;
			}
			if ($game['time_name'] == 'перерыв') {
				
				$minute = 'перерыв';
				$second = '';
				
			}
			
			/*
			echo 'Чемпионат: '.$chmp['name_ch'].'<br />';
			echo 'Матч: '.$team1.' - '.$team2.'<br />';
			echo 'Время: '.$minute.':'.$second.'<br />';
			echo 'Счёт в 1-м тайме: '.$team1_score.':'.$team2_score;
			
			if ($pause) {
				echo '<br /><strong>Перерыв</strong>';
			}
			
			echo '<br><hr><br>';
						
			if ($team1 == 'Торпедо М') {
				echo date('d.m.Y - H:i');
				echo '<pre>';
				print_r($game); exit;
			}
			
			if ($minute == _CALL_MIN) {
				
				$events[] = [
				
					'chmp' => $chmp['name_ch'],
					'team1' => $team1,
					'team2' => $team2,
					'score' => $team1_score.':'.$team2_score,
					'time' => $minute.':'.$second,
				
				];
				
			}
			
		}
		
	}
	
	return $events;
	
}

/* 1. Получаем матчи */
$data = get_data(); 

print_r($data); exit;

?>