<?php

return [

    'title' => 'Основная информация',
	
	'table_user' => 'Пользователь',
	'table_nr' => '№ купона',
	'table_coupon' => 'Купон',
	'table_price' => 'Цена купона',
	'table_expires' => 'До какого числа действителен',
	'table_status' => 'Статус купона',
	'table_invited' => 'Кто пригласил',
	'table_activated' => 'Дата активации купона / подписки',

];
