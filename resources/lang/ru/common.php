<?php

return [

    'button_create' => 'Создать',
	'create_cat' => 'Создать категорию',
	'create_course' => 'Создать курс',
	'create_sound' => 'Создать звук',
	'create_user' => 'Создать пользователя',
	'create_emp' => 'Создать сотрудника',
	'create_coupon' => 'Создать купон',
	
	'menu_list' => 'Список',  
	'menu_dashboard' => 'Основная информация',
	'menu_emp' => 'Сотрудники', 
	'menu_users' => 'Пользователи',  
	'menu_coupon' => 'Купоны / Подписки',  
	'menu_coupon_set' => 'Настройки купонов',
	'menu_cats' => 'Категории',
	'menu_courses' => 'Курсы',
	'menu_sounds' => 'Звуки',
	'menu_playlist' => 'Плейлисты',
	'menu_groups' => 'Группы',
	
	'table_actions' => 'Действия',
	
	'admin_panel' => 'Панель администратора',
	'trash' => 'Корзина',
	
	'button_add' => 'Добавить', 
	'button_restore' => 'Восстановить',
	'button_delete' => 'Удалить',

	'nodata' => 'Нет данных!',
	'noright' => 'Нет прав!',
	'nstated' => 'не указано',
	'nstated_he' => 'не указан',
	'na' => 'неизвестно',
	'select' => 'Выберите',
	'logout' => 'Выйти',

    'del_user' => 'Пользователь удален',
    'del_cat' => 'Категория удалена',
    'del_sound' => 'Звук удален',
    'del_playlist' => 'Плейлист удален',
    'del_group' => 'Группа удалена',
    'del_course' => 'Курс удален',
    'del_coupon' => 'Купон удален',
    'del_err' => 'Ошибка удаления',

    'permission_denied' => 'Доступ запрещен',
    'settings_updated' => 'Настройки обновлены',

    'select_image' => 'Выберите изображение для загрузки',
    'cancel' => 'Отмена',
    'save' => 'Сохранить',
    'delete_confirm' => 'Вы действительно хотите удалить эту запись?',
    'yes' => 'Да',
    'no' => 'Нет',
    'empty' => 'пусто',
    'logged_in' => 'Вы вошли!',
    'dashboard' => 'Дашборд',

];
