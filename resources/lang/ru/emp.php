<?php

return [

	'title' => 'Список сотрудников',
	'sec' => 'Сотрудники',
	'one' => 'Сотрудник',
	'title_add' => 'Добавить сотрудника',
	'title_edit' => 'Редактировать сотрудника',
	
	'title_info' => 'Информация о сотруднике',

    'button' => 'Создать сотрудника',
	'button_add' => 'Добавить',
	'button_save' => 'Сохранить',
	'placeholder' => 'Имя, E-mail, Код сотрудника...',
	'search' => 'Поиск',
	
	'table_name' => 'Имя',
	'table_role' => 'Роль',
	'table_email' => 'E-mail',
	'table_status' => 'Статус',
	'table_who' => 'Кто дал доступ',
	'table_when' => 'Когда предоставлен доступ',
	'table_code' => 'Код сотрудника',
	'table_addr' => 'Адрес',
	'table_group' => 'Группа',

	'table1_name' => 'Имя',
	'table1_email' => 'E-mail',
	'table1_coupon' => 'Купон',
	'table1_cstatus' => 'Статус купона',
	'table1_activated' => 'Дата активации',
	'table1_reg' => 'Дата регистрации',
	
	'active' => 'Активный',
	'pause' => 'На паузе',
	'deleted' => 'Удален',
	
	'box_general' => 'Основная информация',
	
	'form_avatar' => 'Аватар',
	'form_role' => 'Роль',
	'form_group' => 'Группа',
	'form_branchcode' => 'Код ветки',
	'form_branchcode_new' => 'Сгенерировать случайный код',
	'form_addr' => 'Адрес',
	'form_city' => 'Город',
	'form_addr_placeholder' => 'Адрес, Город, Страна',
	'form_name' => 'Имя',
	'form_email' => 'E-mail',
	'form_status' => 'Статус',
	'form_pass' => 'Пароль',
	'form_pass1' => 'Ещё раз пароль',
	
	'name.required' => 'Поле "Имя" обязательно для заполнения!',
	'email.required' => 'Поле "E-mail" обязательно для заполнения!',
	'email.email' => 'Некорректный E-mail адрес',
	'email.unique' => 'Указанный E-mail адрес уже используется другим пользователем',
    'emp_name.required' => 'Поле "Роль в программе" обязательно для заполнения!',
	'emp_status.required' => 'Поле "Статус" обязательно для заполнения!',
	'min' => 'Пароль не может быть короче 6 символов',
	'confirmed' => 'Пароли не совпадают',
	'password.required' => 'Поле "Пароль" обязательно для заполнения!',
	'role_id.required' => 'Поле "Роль" не заполнено или у вас нет прав на присваивание этой роли!',
	'branch.not_exist' => 'Выбранный "Код ветки" не существует!',
	'branch.oem_branch_duplicate' => '"Код ветки" дистрибьютора должен быть уникальным!',
	'parent.error' => 'Выбрана неверная родительская роль',
	'parent.downrole' => 'Вы не можете понизить права сотрудника',

	'added' => 'Сотрудник добавлен',
	'updated' => 'Сотрудник обновлен',
	'notfound' => 'Сотрудник не найден',

	'select_role' => 'Выберите роль',
	'create' => 'Создать',
	'under' => 'Под управлением',
	'under_own' => 'Под своим управлением',
	'userlist' => 'Список пользователей',

];
