<?php

return [

	'title' => 'Список купонов / подписок',
	'sec' => 'Купоны / Подписки',
	'one' => 'Купон',
	'title_add' => 'Добавить купон / подписку',
	'title_edit' => 'Редактировать купон / подписку',
	'title_settings' => 'Настройка цен',
	
	'button_new' => 'Создать купон',
	'button_delete' => 'Удалить купон',
	'button_add' => 'Добавить',
	'button_save' => 'Сохранить',	
    'button' => 'Купоны',
	'list' => 'Список купонов',
	'settings' => 'Настройка цен',
	'placeholder' => 'Имя, E-mail, Телефон...',
	'search' => 'Поиск',

	'table_nr' => 'Номер',
	'table_name' => 'Номер',
	'table_email' => 'E-mail',
	'table_price' => 'Цена',
	'table_user' => 'Пользователь',
	'table_expires' => 'До какого числа действителен',
	'table_expires_full' => 'До какого числа действителен',
	'table_lasta' => 'Посл. вход',
	'table_lasta_full' => 'Последний вход в систему',
	'table_coupon' => 'Купон',
	'table_status' => 'Статус',
	'table_activated' => 'Дата актив.',
	'table_activated_full' => 'Дата активации',
	'table_bought' => 'Кто купил',
	
	'box_general' => 'Общая информация',
	'form_coupon' => 'Какой купон',
	'form_days' => 'Срок действия',
	'form_user' => 'Пользователь',
	'form_status' => 'Статус',
	'form_email' => 'E-mail',
	'form_email_hint' => 'Заполняется автоматически при выборе пользователя',
	'form_dash' => '# купона',
	'form_gen' => 'Сгенерирован автоматически',
	
	'form1_sounds' => 'Звуки',
	'form1_sounds_add' => 'Добавить все звуки',
	'form1_cats' => 'Категории',
	
	'pack_name.required' => 'Поле "Какой купон" обязателен для заполнения!',
	'user_id.required' => 'Поле "Пользователь" обязательно для заполнения!',	
	
	'active' => 'Активен',
	'pause' => 'На паузе',
	'expires' => 'Истекает',
	'na' => 'Неактивен',
	'ended' => 'Закончен',
	
	'period' => 'Срок действия Купона/Подписки',
	'day' => 'День',
	'week' => 'Неделя',
	'month' => 'Месяц',
	'year' => 'Год',
	
	'stop' => 'Остановить',
	'set_active' => 'Активировать',
	
	'added' => 'Купон добавлен',
	'updated' => 'Купон обновлен',
    'notfound' => 'Купон не найден',
    'infoabout' => 'Информация о купоне',
    'not_selected' => 'не указан',
    'user_has_coupon' => 'У данного пользователя уже есть купон',
    'currency' => 'руб.',


];
