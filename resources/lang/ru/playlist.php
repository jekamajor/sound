<?php

return [

	'title' => 'Список плейлистов',
	'title1' => 'Плейлисты пользователя',
	'sec' => 'Плейлисты',
	'one' => 'Плейлист',
	'title_add' => 'Добавить плейлист',
	'title_edit' => 'Редактировать плейлист',
	'title_all' => 'Все плейлисты',

	'button' => 'Создать плейлист',
	'button_edit' => 'Изменить плейлист',
    'button_change' => 'Изменить',
	'button_delete' => 'Удалить плейлист',
    'button_del' => 'Удалить',
	'button_add' => 'Добавить',
	'button_save' => 'Сохранить',
	'placeholder' => 'Название плейлиста или пользователь...',
	'search' => 'Поиск',
	
	'table_name' => 'Название плейлиста',
	'table_user' => 'Пользователь',
	'table_status' => 'Статус',
	'table_created' => 'Дата создания',
	'table_updated' => 'Дата изменения',
	'table_who' => 'Кто создал',

	'name.required' => 'Поле "Название" обязательно для заполнения!',
	
	'box_general' => 'Общая информация',
	'form_user' => 'Пользователь',
	'form_name' => 'Название',
	'form_status' => 'Статус',
	'form_sounds' => 'Звуки, которые хотим прикрепить к плейлисту',

    'active' => 'Активный',
	'pause' => 'На паузе',
	'deleted' => 'Удален',
	
	'added' => 'Плейлист добавлен',
	'updated' => 'Плейлист обновлен',
	'notfound' => 'Плейлист не найден',
	'sounds_count' => 'Кол-во звуков',
	'actions' => 'Действия',

];
