<?php

return [

	'title' => 'Список категорий звуков',
	'title1' => 'Список категорий',
	'sec' => 'Категории',
	'one' => 'Категория',
	'title_add' => 'Добавить категорию',
	'title_edit' => 'Редактировать категорию',
	
	'button' => 'Создать категорию',
	'button_edit' => 'Изменить категорию',
	'button_delete' => 'Удалить категорию',
	'button_add' => 'Добавить',
	'button_save' => 'Сохранить',	
	'placeholder' => 'Название категории или кто создал...',
	'search' => 'Поиск',

	'table_cat' => 'Категория',
	'table_who' => 'Кто создал',
	'table_status' => 'Статус',
	'table_created' => 'Дата создания',
	'table_updated' => 'Дата изменения',
	'table_actions' => 'Действия',

	'table1_title' => 'Курсы',
	'table1_course' => 'Курс',
	'table1_status' => 'Статус',
	'table1_created' => 'Дата создания',
	'table1_updated' => 'Дата изменения',
	'table1_added' => 'Дата добавления',

	'box_general' => 'Общая информация',
	'form_user' => 'Пользователь',
	'form_off' => 'Официальная',
	'form_name' => 'Название',
	'form_courses' => 'Курсы которые хотим прикрепить к категории',
	'form_status' => 'Статус',

    'user_id.required' => 'Выберите пользователя неофициальной группы!',
	'name.required' => 'Поле "Название категории" обязательно для заполнения!',		
	'courses_id.required' => 'Выберите курсы, которые нужно прикрепить к категории!',
	
	'active' => 'Активная',
	'pause' => 'На паузе',
	'deleted' => 'Удалена',
	'na' => 'Неактивная',

	'added' => 'Категория добавлена',
	'updated' => 'Категория обновлена',	
	'notfound' => 'Категория не найдена',

    'catname_required' => 'Поле "Название категории" обязательно для заполнения!',
    'courses_required' => 'Выберите курсы, которые нужно прикрепить к категории!',
	'updated' => 'Категория обновлена',
];
