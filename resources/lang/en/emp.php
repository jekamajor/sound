<?php

return [

	'title' => 'Employees',
	'sec' => 'Employees',
	'title_add' => 'New Employee',
	'title_edit' => 'Edit Employee',

    'button' => 'Create Employee',
	'button_add' => 'Create',
	'button_save' => 'Save Updates',
	'placeholder' => 'Name, E-mail...',
	'search' => 'Search',
	
	'table_name' => 'Name',
	'table_role' => 'Role',
	'table_email' => 'E-mail',
	'table_status' => 'Status',
	'table_who' => 'Access Provided By',
	'table_when' => 'When Provided',
	'table_code' => 'Code',
	
	'active' => 'Active',
	'pause' => 'Paused',
	'deleted' => 'Deleted',
	
	'box_general' => 'General Information',
	
	'form_avatar' => 'Photo',
	'form_role' => 'Role',
	'form_group' => 'Group',
	'form_addr' => 'Address',
	'form_name' => 'Name',
	'form_email' => 'E-mail',
	'form_status' => 'Status',
	'form_pass' => 'Password',
	'form_pass1' => 'Confirm Password',
	
	'name.required' => 'Поле "Имя1" обязательно для заполнения!',
	'email.required' => 'Поле "E-mail" обязательно для заполнения!',
	'email.email' => 'Некорректный E-mail адрес',
	'emp_name.required' => 'Поле "Роль в программе" обязательно для заполнения!',
	'emp_status.required' => 'Поле "Статус" обязательно для заполнения!',
	'min' => 'Пароль не может быть короче 6 символов',
	'confirmed' => 'Пароли не совпадают',
	'password.required' => 'Поле "Пароль" обязательно для заполнения!',
	
	'added' => 'Сотрудник добавлен',
	'updated' => 'Сотрудник обновлен',

];
