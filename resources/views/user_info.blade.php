@extends('layouts.admin.main')

@section('content')
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-language"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li><a href="javascript:void(0);">Русский</a></li>
                            <li><a href="javascript:void(0);">English</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="logout_do">
                            <i class="fa fa-sign-out"></i> @lang('common.logout')
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>@lang('users.one') {{ $rec->name }}</h2>
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                @if (session('success'))
                    <div class="alert alert-danger">{{ session('success') }}</div>
                @endif
                <ol class="breadcrumb">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                    </li>
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/users">@lang('users.sec')</a>
                    </li>
                    <li class="active">
                        <strong>@lang('users.one') {{ $rec->name }}</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2 text-right">
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <tr>
                                        <td>@lang('users.table_group')</td>
                                        @if ($rec->groups->count())
                                            <td>
											@foreach ($rec->groups as $key => $group)
												<a href="/{{ App::getLocale() }}/admin/groups/info/{{ $group['id'] }}">{{ $group['name'] }}</a>
												@if ($key < sizeof($rec->groups) - 1)
													,
												@endif
											@endforeach
                                            </td>
                                        @else
                                            <td>@lang('common.nstated')</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_name')</td>
                                        <td>{{ $rec->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_last_name')</td>
                                        <td>{{ $rec->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_email')</td>
                                        <td>
                                            <a href="mailto:{{ $rec->email }}">{{ $rec->email }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_phone')</td>
                                        <td>{{ $rec->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_addr')</td>
                                        <td>{{ $rec->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_comment')</td>
                                        <td>{{ $rec->comment }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_who')</td>
                                        <td>{!! $rec->parentLink !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <tr>
                                        <td>@lang('users.table_lasta')</td>
                                        <td>
                                            @if (strtotime($rec->last_login))
                                                {{ date('d.m.Y - H:i', strtotime($rec->last_login)) }}
                                            @else
                                                &mdash;
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_coupon')</td>
                                        <td>
                                            @if ($coupon_first)
                                                <span class="badge badge-info">{{ $coupon_first->packname }}</span>
                                            @else
                                                @lang('common.no')
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('coupon.period')</td>
                                        <td>
                                            @if ($coupon_first)
												<span class="badge badge-info">{{Lang::get('coupon.'.$coupon_first->expires)}}</span>
                                            @else
                                                &mdash;
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_cexpires')</td>
                                        <td>
                                            @if ($coupon_first && strtotime($coupon_first->date_expires))
                                                {{ date('d.m.Y - H:i', strtotime($coupon_first->date_expires)) }}
                                            @else
                                                &mdash;
										    @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_cactivated')</td>
                                        <td>
											@if ($coupon_first && strtotime($coupon_first->date_activated))
                                                {{ date('d.m.Y - H:i', strtotime($coupon_first->date_activated)) }}
                                            @else
                                                &mdash;
										    @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_cstatus')</td>
                                        <td>
                                            @if ($coupon_first)
                                                @if ($coupon_first->status == 'active')
                                                    <span class="badge badge-success">@lang('coupon.active')</span>
                                                @elseif ($coupon_first->status == 'pause')
                                                    <span class="badge badge-warning">@lang('coupon.pause')</span>
                                                @elseif ($coupon_first->status == 'expires')
                                                    <span class="badge badge-danger">@lang('coupon.expires')</span>
                                                @elseif ($coupon_first->status == 'na')
                                                    <span class="badge badge-primary">@lang('coupon.na')</span>
                                                @elseif ($coupon_first->status == 'ended')
                                                    <span class="badge badge-danger">@lang('coupon.ended')</span>
                                                @endif
                                            @else
                                                &mdash;
										    @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('users.table_device')</td>
                                        <td>&mdash;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                @if($rec->trash == 0)
                    <div class="col-lg-12">
						<a href="/{{ App::getLocale() }}/admin/users/add" class="btn btn-success">@lang('users.button')</a>
                        <a href="/{{ App::getLocale() }}/admin/users/edit/{{ $rec->id }}" class="btn btn-primary">@lang('users.button_edit')</a>
                        <a href="/{{ App::getLocale() }}/admin/delete_record/users/{{ $rec->id }}" class="btn btn-danger">@lang('users.button_delete')</a>
                        <br/><br/>
                    </div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/users/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/users/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
					<br /><br />
				</div>
				@endif

                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="sticked">
                                <h3>@lang('users.table1_title')</h3>
                                @if($rec->trash == 0)
                                    <a href="/{{ App::getLocale() }}/admin/coupon/add?user_id={{ $rec->id }}" class="btn btn-primary">@lang('common.button_add')</a>
                                @endif
                                    </div>
                                <table data-nosort="0,8" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                    <tr>
                                        <th data-toggle="true">#</th>
                                        <th data-toggle="true">@lang('users.table1_nr')</th>
                                        <th data-toggle="true">@lang('users.table1_coupon')</th>
                                        <th data-toggle="true">@lang('users.table1_cprice')</th>
                                        <th data-toggle="true">@lang('users.table1_cexpires')</th>
                                        <th data-toggle="true">@lang('users.table1_lasta')</th>
                                        <th data-toggle="true">@lang('users.table1_cstatus')</th>
                                        <th data-toggle="true">@lang('users.table1_cactivated')</th>
                                        <th class="text-right" data-sort-ignore="true">@lang('common.table_actions')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($list->count())
                                        @foreach ($list as $key => $record)
                                            <tr class="footable-odd">
                                                <td class="footable-visible">{{ ($key + 1) }}</td>
                                                <td class="footable-visible"><a
                                                            href="/{{ App::getLocale() }}/admin/coupon/info/{{ $record->id }}">{{ $record->number }}</a>
                                                </td>
                                                <td class="footable-visible">
                                                    <span class="badge badge-info">{{ $record->packname }}</span>
                                                </td>
                                                <td class="footable-visible">{{ $record->packprice }}</td>
                                                <td class="footable-visible">
                                                    @if (strtotime($record->date_expires))
                                                    {{ date('d.m.Y - H:i', strtotime($record->date_expires)) }}
                                                    @else
                                                    &mdash;
                                                    @endif
                                                </td>
                                                <td class="footable-visible">
                                                    @if (strtotime($rec->last_login))
                                                    {{ date('d.m.Y - H:i', strtotime($rec->last_login)) }}
                                                    @else
                                                    &mdash;
                                                    @endif
                                                </td>
                                                <td class="footable-visible set_st{{ $record->id }}">
                                                    @if ($record->status == 'active')
                                                        <span class="badge badge-success">@lang('coupon.active')</span>
                                                    @endif
                                                    @if ($record->status == 'pause')
                                                        <span class="badge badge-warning">@lang('coupon.pause')</span>
                                                    @endif
                                                    @if ($record->status == 'expires')
                                                        <span class="badge badge-danger">@lang('coupon.expires')</span>
                                                    @endif
                                                    @if ($record->status == 'na')
                                                        <span class="badge badge-primary">@lang('coupon.na')</span>
                                                    @endif
                                                    @if ($record->status == 'ended')
                                                        <span class="badge badge-danger">@lang('coupon.ended')</span>
                                                    @endif													
                                                </td>
                                                <td class="footable-visible set_st{{ $record->id }}_act">
                                                    @if (strtotime($record->date_activated))
                                                    {{ date('d.m.Y - H:i', strtotime($record->date_activated)) }}
                                                    @else
                                                    &mdash;
                                                    @endif
                                                </td>
                                                <td class="text-right footable-visible footable-last-column">
                                                    @if ($record->status == 'active')
                                                        <div class="btn-group">
                                                            <a href="/{{ App::getLocale() }}/admin/coupon/pause/{{ $record->id }}/{{$record->user_id}}"
                                                               data-id="{{ $record->id }}" data-st="@lang('coupon.pause')"
                                                               class="btn-white btn btn-xs coupon_action">@lang('coupon.stop')</a>
                                                            <a href="/{{ App::getLocale() }}/admin/coupon/start/{{ $record->id }}/{{$record->user_id}}"
                                                               data-id="{{ $record->id }}" data-st="@lang('coupon.active')"
                                                               class="btn-white btn btn-xs coupon_action"
                                                               style="display: none;">@lang('coupon.set_active')</a>
                                                        </div>
                                                    @else
                                                        <div class="btn-group">
                                                            <a href="/{{ App::getLocale() }}/admin/coupon/pause/{{ $record->id }}/{{$record->user_id}}"
                                                               data-id="{{ $record->id }}" data-st="@lang('coupon.pause')"
                                                               class="btn-white btn btn-xs coupon_action"
                                                               style="display: none;">@lang('coupon.stop')</a>
                                                            <a href="/{{ App::getLocale() }}/admin/coupon/start/{{ $record->id }}/{{$record->user_id}}"
                                                               data-id="{{ $record->id }}" data-st="@lang('coupon.active')"
                                                               class="btn-white btn btn-xs coupon_action">@lang('coupon.set_active')</a>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="sticked"><h3>@lang('users.table2_title')</h3>
                                @if($rec->trash == 0)
                                    <a href="/{{ App::getLocale() }}/admin/cats/add?user_id={{ $rec->id }}" class="btn btn-primary">@lang('common.button_add')</a>
                                @endif
                                </div>
                                <table data-nosort="0" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                    <tr>
                                        <th data-toggle="true">#</th>
                                        <th data-toggle="true">@lang('users.table2_name')</th>
                                        <th data-toggle="true">@lang('users.table1_cstatus')</th>
                                        <th data-toggle="true">@lang('users.table2_created')</th>
                                        <th data-toggle="true">@lang('users.table2_updated')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (is_object($list1))
                                        @if ($list1->count())
                                            @foreach ($list1 as $key => $record)
                                                <tr class="footable-odd">
                                                    <td class="footable-visible">{{ ($key+1) }}</td>
                                                    <td class="footable-visible"><a
                                                                href="/{{ App::getLocale() }}/admin/cats/info/{{ $record->id }}">{{ $record->name }}</a>
                                                    </td>
                                                    <td class="footable-visible">
                                                        @if ($record->status == 'active')
														<span class="badge badge-success">@lang('cats.active')</span>
                                                        @endif
                                                        @if ($record->status == 'deleted')
                                                            <span class="badge badge-danger">@lang('cats.deleted')</span>
                                                        @endif
                                                        @if ($record->status == 'pause')
                                                            <span class="badge badge-warning">@lang('cats.pause')</span>
                                                        @endif
                                                        @if ($record->status == 'na')
                                                            <span class="badge badge-primary">@lang('cats.na')</span>
                                                        @endif
                                                    </td>
                                                    <td class="footable-visible">{{ date('d.m.Y - H:i', strtotime($record->created_at))}}</td>
                                                    <td class="footable-visible">{{ date('d.m.Y - H:i', strtotime($record->updated_at))}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="sticked"><h3>@lang('users.table3_title')</h3>
                                @if($rec->trash == 0)
                                    <a href="/{{ App::getLocale() }}/admin/playlist/add?user_id={{ $rec->id }}"
                                   class="btn btn-primary">@lang('common.button_add')</a>
                                @endif
                                </div>
                                <table data-nosort="0" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                    <tr>
                                        <th data-toggle="true">#</th>
                                        <th data-toggle="true">@lang('users.table3_name')</th>
                                        <th data-toggle="true">@lang('users.table1_cstatus')</th>
                                        <th data-toggle="true">@lang('users.table3_created')</th>
                                        <th data-toggle="true">@lang('users.table3_updated')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (is_object($list2))
                                        @if ($list2->count())
                                            @foreach ($list2 as $key => $record)
                                                <tr class="footable-odd">
                                                    <td class="footable-visible">{{ ($key+1) }}</td>
                                                    <td class="footable-visible"><a
                                                                href="/{{ App::getLocale() }}/admin/playlist/info/{{ $record->id }}">{{ $record->name }}</a>
                                                    </td>
                                                    <td class="footable-visible">
                                                        @if ($record->status == 'active')
                                                            <span class="badge badge-success">@lang('course.active')</span>
                                                        @endif
                                                        @if ($record->status == 'pause')
                                                            <span class="badge badge-warning">@lang('course.pause')</span>
                                                        @endif
                                                        @if ($record->status == 'deleted')
                                                            <span class="badge badge-danger">@lang('course.deleted')</span>
                                                        @endif
                                                        @if ($record->status == 'none')
                                                            <span class="badge badge-primary">@lang('course.none')</span>
                                                        @endif
                                                    </td>
                                                    <td class="footable-visible">{{ date('d.m.Y - H:i', strtotime($record->created_at)) }}</td>
                                                    <td class="footable-visible">{{ date('d.m.Y - H:i', strtotime($record->updated_at)) }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="sticked"><h3>@lang('users.table4_title')</h3>
                                @if($rec->trash == 0)
                                    <a href="/{{ App::getLocale() }}/admin/courses/add?user_id={{ $rec->id }}" class="btn btn-primary">@lang('common.button_add')</a>
                                @endif
                                </div>
                                <table data-nosort="0" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                    <tr>
                                        <th data-toggle="true">#</th>
                                        <th data-toggle="true">@lang('users.table4_name')</th>
                                        <th data-toggle="true">@lang('users.table4_status')</th>
                                        <th data-toggle="true">@lang('users.table4_created')</th>
                                        <th data-toggle="true">@lang('users.table4_updated')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($list3))
                                        @if ($list3->count())
                                            @foreach ($list3 as $key => $record)
                                                <tr class="footable-odd">
                                                    <td class="footable-visible">{{ ($key+1) }}</td>
                                                    <td class="footable-visible"><a
                                                                href="/{{ App::getLocale() }}/admin/courses/info/{{ $record->id }}">{{ $record->name }}</a>
                                                    </td>
                                                    <td class="footable-visible">
                                                        @if ($record->status == 'active')
                                                            <span class="badge badge-success">@lang('course.active')</span>
                                                        @endif
                                                        @if ($record->status == 'pause')
                                                            <span class="badge badge-warning">@lang('course.pause')</span>
                                                        @endif
                                                        @if ($record->status == 'expires')
                                                            <span class="badge badge-danger">@lang('course.expires')</span>
                                                        @endif
														@if ($record->status == 'none')
															<span class="badge badge-primary">@lang('course.none')</span>
														@endif
														@if ($record->status == 'deleted')
															<span class="badge badge-danger">@lang('course.deleted')</span>
														@endif														
                                                    </td>
                                                    <td class="footable-visible">
                                                        {{ date('d.m.Y - H:i', strtotime($record->created_at)) }}
                                                    </td>
                                                    <td class="footable-visible">
                                                        {{ date('d.m.Y - H:i', strtotime($record->updated_at)) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


        </div>
        <div class="footer">
            <div class="pull-right">

            </div>
            <div>

            </div>
        </div>
    </div>
@endsection
