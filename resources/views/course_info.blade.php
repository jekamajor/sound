@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>				
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('course.one') {{ $rec->name }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/courses">@lang('course.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('course.one') {{ $rec->name }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('course.table_cat')</td>
								<td>
								@if (isset($cats) && $cats->count())
                                    @foreach ($cats as $cat)
                                        <a href="/{{ App::getLocale() }}/admin/cats/info/{{ $cat->id }}">{{ $cat->name }}</a>,
                                    @endforeach
                                @else
                                    @lang('common.empty')
								@endif
                                </td>
							</tr>
							<tr>
								<td>@lang('course.table_name')</td>
								<td>{{ $rec->name }}</td>
							</tr>
							<tr>
								<td>@lang('course.table_who')</td>
                                <td>
								@if ($rec->official == 'on')
									<a href="/{{ App::getLocale() }}/admin/emp/info/15">{{config('app.officialName')}}</a>
                                @elseif (!empty($rec->owner['name']))
									<a href="/{{ App::getLocale() }}/admin/users/info/{{$rec->owner['id']}}">{{$rec->owner['name'].' '.$rec->owner['last_name']}}</a>
								@else
									@lang('common.nstated_he')
                                @endif
                                </td>
							</tr>
							<tr>
								<td>@lang('course.table_status')</td>
								<td>
									@if ($rec->status == 'active')
										<span class="badge badge-success">@lang('course.active')</span>
									@endif
									@if ($rec->status == 'pause')
										<span class="badge badge-warning">@lang('course.pause')</span>
									@endif
									@if ($rec->status == 'deleted')
										<span class="badge badge-danger">@lang('course.deleted')</span>
									@endif
									@if ($rec->status == 'none')
										<span class="badge badge-primary">@lang('course.none')</span>
									@endif
									@if ($rec->status == 'ended')
										<span class="badge badge-danger">@lang('course.ended')</span>
									@endif
								</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('course.table_created')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->created_at)) }}</td>
							</tr>
							<tr>
								<td>@lang('course.table_updated')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->updated_at)) }}</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>

				@if($rec->trash == 0)
				<div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/courses/add" class="btn btn-success">@lang('course.button')</a>
					<a href="/{{ App::getLocale() }}/admin/courses/edit/{{ $rec->id }}" class="btn btn-primary">@lang('course.button_edit')</a>
					<a href="/{{ App::getLocale() }}/admin/delete_record/courses/{{ $rec->id }}" class="btn btn-danger">@lang('course.button_delete')</a>
					<br /><br />
				</div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/courses/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/courses/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
					<br /><br />
				</div>
				@endif

				<form action="/{{ App::getLocale() }}/admin/courses/info/{{ $rec->id }}" method="POST">
				@csrf
				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-content">
							<div class="sticked">
								<h3>@lang('course.table1_title')</h3>
							</div>
                            <table class="sortable footable dataTable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">#</th>
                                    <th data-toggle="true">@lang('course.table1_name')</th>
                                    <th data-toggle="true">@lang('course.table1_pos')</th>
									<th data-toggle="true">@lang('course.table1_ras')</th> 
									<th data-toggle="true">@lang('course.table1_created')</th> 
                                </tr>
                                </thead>
                                <tbody>
									@if ($list->count())
										@foreach ($list as $k => $r)
											<tr>
												<td>{{ $k + 1 }}</td>
												<td>
                                                @if(Auth::user()->isAdmin())
                                                    <a href="/{{ App::getLocale() }}/admin/sounds/info/{{ $r->id }}">{{ $r->name }}</a>
                                                @else
                                                    {{ $r->name }}
                                                @endif
												</td>
												<td>
													<input type="text" name="pos[{{ $r->id }}]" value="{{ $r->pos }}" style="width: 120px;" class="form-control">
												</td>
												<td>{{ $r->dandzy }}</td>
												<td>{{ date('d.m.Y - H:i', strtotime($r->pivot->created_at)) }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<button type="submit" class="btn btn-primary">@lang('course.button_save_pos')</button>
				</div>
				</form>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection