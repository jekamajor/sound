@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('playlist.title1') {{ $user->email }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/admin/">@lang('users.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('playlist.title1') {{ $user->email }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
							<a href="/admin/playlist/add/{{ $user->id }}" class="btn btn-primary">@lang('playlist.title_add')</a>
                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">#</th>
									<th data-toggle="true">@lang('playlist.table_name')</th>
									<th data-toggle="true">@lang('playlist.sounds_count')</th>
                                    <th class="text-right" data-sort-ignore="true">@lang('playlist.actions')</th>
                                </tr>
                                </thead>
                                <tbody>
									@if ($list->count())
										@foreach ($list as $record)
											<tr class="footable-odd">
												<td class="footable-visible">{{ $record->id }}</td>	
												<td class="footable-visible">{{ $record->name }}</td>
												<td class="footable-visible">{{ $opts[$record->id]['sounds'] }}</td>
												<td class="text-right footable-visible footable-last-column">
													<div class="btn-group">
														<a href="/admin/playlist/edit/{{ $record->id }}/{{ $user->id }}" class="btn-white btn btn-xs">@lang('playlist.button_change')</a>
														<a href="/{{ App::getLocale() }}/admin/delete_record/playlist/{{ $record->id }}" class="btn-white btn btn-xs">@lang('playlist.button_del')</a>
													</div>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td>@lang('common.nodata')</td>
										</tr>
									@endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection