@extends('layouts.admin.main')

@section('content')
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-language"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li><a href="javascript:void(0);">Русский</a></li>
                            <li><a href="javascript:void(0);">English</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="logout_do">
                            <i class="fa fa-sign-out"></i> @lang('common.logout')
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>@lang('emp.one') {{ $rec->name }}</h2>
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                @if (session('success'))
                    <div class="alert alert-danger">{{ session('success') }}</div>
                @endif
                <ol class="breadcrumb">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                    </li>
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/emp/">@lang('emp.sec')</a>
                    </li>
                    <li class="active">
                        <strong>@lang('emp.one') {{ $rec->name }}</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox">
                            <div class="ibox-content">
                                @if ($rec->avatar)
                                    <p style="text-align: center;"><img src="/{{ $rec->avatar }}" style="width: 250px;height: 250px;"></p>
                                @endif
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <tr>
                                        <td>@lang('emp.table_name')</td>
                                        <td>{{ $rec->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_role')</td>
                                        <td>{{ $rec->rolename }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_email')</td>
                                        <td><a href="mailto:{{ $rec->email }}">{{ $rec->email }}</a></td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_who')</td>
                                        <td>
                                            {!! $rec->parentLink !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_group')</td>
                                        <td>
											@if ($rec->groups->count())
                                                @foreach ($rec->groups as $key => $group)
                                                    <a href="/{{ App::getLocale() }}/admin/groups/info/{{ $group['id'] }}">{{ $group['name'] }}</a>
                                                    @if ($key < sizeof($rec->groups) - 1)
                                                        ,
                                                    @endif
                                                @endforeach
                                            @else
                                                @lang('common.nstated')
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <tr>
                                        <td>@lang('emp.table_status')</td>
                                        <td>
                                            @if ($rec->emp_status == 'active')
                                                <span class="badge badge-success">@lang('emp.active')</span>
                                            @endif
                                            @if ($rec->emp_status == 'pause')
                                                <span class="badge badge-warning">@lang('emp.pause')</span>
                                            @endif
                                            @if ($rec->emp_status == 'deleted')
                                                <span class="badge badge-danger">@lang('emp.deleted')</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_when')</td>
                                        <td>{{ date('d.m.Y - H:i', strtotime($rec->created_at)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>@lang('emp.table_code')</td>
										@if ($rec->id == 15)
											<td>[0][000][0000]</td>
										@else
											<td>{{ $rec->empcode }}</td>
										@endif
                                    </tr>
									<tr>
										<td>@lang('emp.table_addr')</td>
										<td>{{ $rec->address }}</td>
									</tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding-bottom: 20px;">
                        <a href="/{{ App::getLocale() }}/admin/emp/add" class="btn btn-success">@lang('emp.title_add')</a>
                        <a href="/{{ App::getLocale() }}/admin/emp/edit/{{ $rec->id }}" class="btn btn-primary">@lang('emp.title_edit')</a>
                    </div>


                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <table data-nosort="0" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                    <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            #
                                        </th>
                                        <th data-toggle="true">
                                            @lang('emp.table1_name')
                                        </th>
                                        <th data-toggle="true">
                                            @lang('emp.table1_email')
                                        </th>
                                        <th data-toggle="true">@lang('emp.table1_coupon')</th>
                                        <th data-toggle="true">@lang('emp.table1_cstatus')</th>
                                        <th data-toggle="true">@lang('emp.table1_activated')</th>
                                        <th data-toggle="true">@lang('emp.table1_reg')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (Auth::user()->has_right('emp') or Auth::user()->has_right('emp/'))
                                        @if ($list->count())
                                            @foreach ($list as $key => $record)
                                                <tr class="footable-odd">
                                                    <td class="footable-visible">{{ ($key+1) }}</td>
                                                    <td class="footable-visible"><a
                                                                href="/{{ App::getLocale() }}/admin/users/info/{{ $record->id }}">{{ $record->name }}</a>
                                                    </td>
                                                    <td class="footable-visible">{{ $record->email }}</td>
                                                    <td class="footable-visible">
                                                        <span class="badge badge-info">{{ $opts[$record->id]['name'] }}</span>
                                                    </td>
                                                    <td class="footable-visible">
                                                        @if ($opts[$record->id]['coupon_status'] == 'active')
                                                            <span class="badge badge-success">@lang('coupon.active')</span>
                                                        @endif
                                                        @if ($opts[$record->id]['coupon_status'] == 'pause')
                                                            <span class="badge badge-warning">@lang('coupon.pause')</span>
                                                        @endif
                                                        @if ($opts[$record->id]['coupon_status'] == 'deleted')
                                                            <span class="badge badge-danger">@lang('coupon.deleted')</span>
                                                        @endif
                                                        @if ($opts[$record->id]['coupon_status'] == 'na')
                                                            <span class="badge badge-primary">@lang('coupon.na')</span>
                                                        @endif
                                                        @if ($opts[$record->id]['coupon_status'] == 'ended')
                                                            <span class="badge badge-danger">@lang('coupon.ended')</span>
                                                        @endif															
                                                    </td>
                                                    <td>
                                                        @if (strtotime($opts[$record->id]['date_activated']))
                                                        {{ date('d.m.Y - H:i', strtotime($opts[$record->id]['date_activated'])) }}
                                                        @else
                                                        &mdash;
                                                        @endif
                                                    </td>
                                                    <td>{{ date('d.m.Y - H:i', strtotime($record->created_at)) }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>@lang('common.nodata')</td>
                                            </tr>
                                        @endif
                                    @else
                                        <tr>
                                            <td>@lang('common.noright')</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <ul class="pagination pull-right"></ul>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>


        </div>
        <div class="footer">
            <div class="pull-right">

            </div>
            <div>

            </div>
        </div>
    </div>
@endsection
