@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
					@if (!isset($id))
						<h2>@lang('cats.title_add')</h2>
					@else
						<h2>@lang('cats.title_edit')</h2>
					@endif
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/cats/">@lang('cats.title1')</a>
                        </li>
                        <li class="active">
							@if (!isset($id))
								<strong>@lang('cats.title_add')</strong>
							@else
								<strong>@lang('cats.title_edit')</strong>
							@endif
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}<br/>
                @endforeach
            </div>
            @endif
			@if (!isset($id))
				<form action="{{ route('admin_cadd', App::getLocale()) }}" method="POST" class="form-horizontal">
			@else
				<form action="{{ route('admin_cedit', [App::getLocale(), $id]) }}" method="POST" class="form-horizontal">
			@endif		
			@csrf
			<div class="row">
				<div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>@lang('cats.box_general')</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">						
							<div class="form-group" @if (isset($_GET['user_id'])) style="display: none;" @endif>
								<label class="col-sm-2 control-label">@lang('cats.form_user')</label>
								<div class="col-sm-10">
									<select name="user_id" class="form-control">
										<option value=""></option>
										@if ($users->count())
											@foreach ($users as $user)
												<option value="{{ $user->id }}" @if(old('user_id', $rec->user_id) == $user->id) selected @endif>{{ $user->name }} ({{ $user->address }})</option>
											@endforeach
										@endif
									</select>
								</div>
							</div>
							<div class="hr-line-dashed" @if (isset($_GET['user_id']) or !Auth::user()->isAdmin()) style="display: none;" @endif></div>
								<div class="form-group" @if (isset($_GET['user_id']) or !Auth::user()->isAdmin()) style="display: none;" @endif>
									<label class="col-sm-2 control-label">@lang('cats.form_off')</label>
									<div class="col-sm-10">
										<input type="checkbox" name="official" @if ($rec->official == 'on') checked @endif>
									</div>
								</div>
							<div class="hr-line-dashed" @if (isset($_GET['user_id']) or !Auth::user()->isAdmin()) style="display: none;" @endif></div>
                            <div class="form-group"><label class="col-sm-2 control-label">@lang('cats.form_name') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="name" value="{{ old('name', $rec->name) }}" class="form-control"></div>
                            </div>						      
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('cats.form_courses') <span class="req">*</span></label>
								@if (isset($id))
									<?php
									foreach ($courses as $cc) {
										echo '<input type="hidden" value="'.$cc.'" class="add_tag">';
									}
									?>
								@endif
								<div class="col-sm-10 offdiv">
									<input id="courses_id" type="text" name="courses_id[]" value="" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('cats.form_status') <span class="req">*</span></label>
								<div class="col-sm-10">
									<select name="status" class="form-control">
										<option value="active" @if (old('status', $rec->status) == 'active') selected @endif>@lang('cats.active')</option>
										<option value="pause" @if (old('status', $rec->status) == 'pause') selected @endif>@lang('cats.pause')</option>
										<option value="deleted" @if (old('status', $rec->status) == 'deleted') selected @endif>@lang('cats.deleted')</option>
										<option value="na" @if (old('status', $rec->status) == 'na') selected @endif>@lang('cats.na')</option>
									</select>
								</div>
							</div>														
                        </div>
                    </div>				
				</div>				
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-sm-4">
							@if (!isset($id))
								<button class="btn btn-primary" type="submit">@lang('cats.button_add')</button>
							@else
								<button class="btn btn-primary" type="submit">@lang('cats.button_save')</button>
							@endif
						</div>
					</div>				
				</div>
			</div>
			</form>
        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
            </div>
        </div>
        </div>
@endsection