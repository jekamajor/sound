@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">
                    <h2>@lang('users.title')</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/users">@lang('users.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('users.title')</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-4 text-right">
					@if (Auth::user()->has_right('users/add'))
						<a href="/{{ App::getLocale() }}/admin/users/add" class="btn btn-danger" style="margin-top: 30px;">@lang('users.button')</a>
					@endif
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
				<div class="col-lg-12">
					<form action="/{{ App::getLocale() }}/admin/users" class="form form-inline">
						<div class="form-group">
							<input type="text" name="nr" placeholder="@lang('users.placeholder')" value="{{ $search }}" class="form-control">
							<button type="submit" class="btn btn-default">@lang('users.search')</button>
						</div>
					</form>
					<br />
				</div>			
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <table data-nosort="0,9" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">
										<span>#</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_name')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_email')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_phone')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('users.table_lasta')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_cexpires')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_cstatus')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('users.table_cactivated')</span>
									</th>
									<th data-toggle="true"><span>@lang('users.table_who')</span></th>
                                    <th class="text-right" data-sort-ignore="true"><span>@lang('common.table_actions')</span></th>
                                </tr>
                                </thead>
                                <tbody>
									@if (Auth::user()->has_right('users') or Auth::user()->has_right('users/'))
									@if ($list->count())
										@foreach ($list as $key => $record)
											<tr class="footable-odd">
												<td class="footable-visible">{{ ($key+1) }}</td>		
												<td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/users/info/{{ $record->id }}">{{ $record->name }}</a></td>
												<td class="footable-visible">{{ $record->email }}</td>
												<td class="footable-visible">{{ $record->phone }}</td>
												<td class="footable-visible">
													@if (strtotime($record->last_login))
														{{ date('d.m.Y - H:i', strtotime($record->last_login)) }}
													@else
														&mdash;
													@endif
												</td>
												<td>
													@if (strtotime($opts[$record->id]['date_expires']))
														{{ date('d.m.Y - H:i', strtotime($opts[$record->id]['date_expires'])) }}
													@else
														&mdash;
													@endif
												</td>
												<td>
													@if ($opts[$record->id]['coupon_status'] == 'active')
														<span class="badge badge-success">@lang('coupon.active')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'pause')
														<span class="badge badge-warning">@lang('coupon.pause')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'expires')
														<span class="badge badge-danger">@lang('coupon.expires')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'na')
														<span class="badge badge-primary">@lang('coupon.na')</span>
													@endif		
													@if ($opts[$record->id]['coupon_status'] == 'ended')
														<span class="badge badge-danger">@lang('coupon.ended')</span>
													@endif														
												</td>
												<td>
													@if (strtotime($opts[$record->id]['date_activated']))
														{{ date('d.m.Y - H:i', strtotime($opts[$record->id]['date_activated'])) }}
													@else
														&mdash;
													@endif
												</td>												
												<td>
													{!! $opts[$record->id]['invited'] !!}
												</td>
												<td class="text-right footable-visible footable-last-column">
													<div class="btn-group">
														<a href="/{{ App::getLocale() }}/admin/users/edit/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-pencil-alt"></i></a>
														<a href="/{{ App::getLocale() }}/admin/delete_record/users/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-times"></i></a>
													</div>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="15">@lang('users.nodata')</td> 
										</tr>
									@endif
									@else
										<tr><td colspan="15">@lang('users.noright')</td></tr>
									@endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection