@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
					@if (!isset($id))
						<h2>@lang('coupon.title_settings')</h2>
					@else
						<h2>@lang('coupon.title_settings')</h2>
					@endif
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/coupon/">@lang('coupon.sec')</a>
                        </li>
                        <li class="active">
							@if (!isset($id))
								<strong>@lang('coupon.title_settings')</strong>
							@else
								<strong>@lang('coupon.title_settings')</strong>
							@endif
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			<form action="{{ route('admin_cs', App::getLocale()) }}" method="POST" class="form-horizontal">
			@csrf
			<div class="row">
				<div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Beauty & Health Pack</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_sounds') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_tags" type="hidden">
									<input id="sounds_input" type="text" name="sounds_id[beauty_health_pack][]" value="{{ implode(',', $sounds['beauty_health_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>	
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_cats') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_cats" type="hidden">
									<input id="cats_input" type="text" name="cats_id[beauty_health_pack][]" value="{{ implode(',', $cats['beauty_health_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>								
							<div class="form-group">
								<div class="col-sm-12 pull-right">
									<input type="button" value="@lang('coupon.form1_sounds_add')" class="btn btn-danger add_all" style="width: 100%;">
								</div>
							</div>
							<div class="hr-line-dashed"></div>							
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.day') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[beauty_health_pack][day]" value="{{ $prices['beauty_health_pack']['day'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.week') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[beauty_health_pack][week]" value="{{ $prices['beauty_health_pack']['week'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.month') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[beauty_health_pack][month]" value="{{ $prices['beauty_health_pack']['month'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.year') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[beauty_health_pack][year]" value="{{ $prices['beauty_health_pack']['year'] }}" class="form-control">
								</div>
							</div>							
                        </div>
                    </div>				
				</div>
				<div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Salon Pack</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_sounds') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_tags" type="hidden">
									<input id="sounds_input2" type="text" name="sounds_id[salon_pack][]" value="{{ implode(',', $sounds['salon_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_cats') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_cats" type="hidden">
									<input id="cats_input1" type="text" name="cats_id[salon_pack][]" value="{{ implode(',', $cats['salon_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>							
							<div class="form-group">
								<div class="col-sm-12 pull-right">
									<input type="button" value="@lang('coupon.form1_sounds_add')" class="btn btn-danger add_all" style="width: 100%;">
								</div>
							</div>
							<div class="hr-line-dashed"></div>							
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.day') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[salon_pack][day]" value="{{ $prices['salon_pack']['day'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.week') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[salon_pack][week]" value="{{ $prices['salon_pack']['week'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.month') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[salon_pack][month]" value="{{ $prices['salon_pack']['month'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.year') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[salon_pack][year]" value="{{ $prices['salon_pack']['year'] }}" class="form-control">
								</div>
							</div>							
                        </div>
                    </div>				
				</div>
				<div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Advanced Pack</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_sounds') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_tags" type="hidden">
									<input id="sounds_input3" type="text" name="sounds_id[advanced_pack][]" value="{{ implode(',', $sounds['advanced_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.form1_cats') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input id="add_cats" type="hidden">
									<input id="cats_input2" type="text" name="cats_id[advanced_pack][]" value="{{ implode(',', $cats['advanced_pack']) }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>							
							<div class="form-group">
								<div class="col-sm-12 pull-right">
									<input type="button" value="@lang('coupon.form1_sounds_add')" class="btn btn-danger add_all" style="width: 100%;">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.day') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[advanced_pack][day]" value="{{ $prices['advanced_pack']['day'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.week') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[advanced_pack][week]" value="{{ $prices['advanced_pack']['week'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.month') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[advanced_pack][month]" value="{{ $prices['advanced_pack']['month'] }}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">@lang('coupon.year') <span class="req">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="prices[advanced_pack][year]" value="{{ $prices['advanced_pack']['year'] }}" class="form-control">
								</div>
							</div>							
                        </div>
                    </div>				
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-sm-4">
							<button class="btn btn-primary" type="submit">@lang('coupon.button_save')</button>
						</div>
					</div>				
				</div>
			</div>
			</form>
        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
            </div>
        </div>
        </div>
@endsection