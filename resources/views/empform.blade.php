@extends('layouts.admin.main')

@section('content')

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-language"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li><a href="javascript:void(0);">Русский</a></li>
                            <li><a href="javascript:void(0);">English</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="logout_do">
                            <i class="fa fa-sign-out"></i> @lang('common.logout')
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                @if (!isset($id))
                    <h2>@lang('emp.title_add')</h2>
                @else
                    <h2>@lang('emp.title_edit')</h2>
                @endif
                <ol class="breadcrumb">
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                    </li>
                    <li>
                        <a href="/{{ App::getLocale() }}/admin/emp/">@lang('emp.sec')</a>
                    </li>
                    <li class="active">
                        @if (!isset($id))
                            <strong>@lang('emp.title_add')</strong>
                        @else
                            <strong>@lang('emp.title_edit')</strong>
                        @endif
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            @if (!isset($id))
                <form action="{{ route('admin_eadd', App::getLocale()) }}" method="POST" enctype="multipart/form-data"
                      class="form-horizontal">
                    @else
                        <form action="{{ route('admin_eedit', [App::getLocale(), $id]) }}" method="POST" enctype="multipart/form-data"
                              class="form-horizontal">
                            @endif
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br/>
                                        @endforeach
                                    </div>
                                    @endif
                                    @if (session('error'))
                                        <div class="alert alert-danger">{{ session('error') }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>@lang('emp.box_general')</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            @if (isset($rec->avatar) && $rec->avatar != '')
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">@lang('emp.form_avatar') </label>
                                                    <div class="col-sm-10">
                                                        <p style="text-align: center;"><img
                                                                    src="/{{ old('avatar', $rec->avatar) }}"
                                                                    style="width: 250px;height: 250px;">
                                                            <button id="delete_avatar">&#10006;</button>
                                                            <input type="checkbox" name="delete_avatar" value="1" style="display:none" />
                                                            <br>
                                                            <input type="file" name="avatar" class="form-control"></p>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                            @else
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">@lang('emp.form_avatar') </label>
                                                    <div class="col-sm-10">
                                                        <input type="file" name="avatar" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                            @endif


                                            @if(!isset($id) || Auth::user()->id != $id)
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_role') <span
                                                            class="req">*</span></label>
                                                <div class="col-sm-10">
                                                    <select name="roleparent" class="form-control">
                                                        <option value="">@lang('emp.select_role')</option>

                                                        @foreach($roles as $role)
                                                            @if (isset($id) && $role['id'] > $rec->role_id) @continue @endif
                                                        <optgroup label="{{ $role['name'] }}">
														    <option value="{{ $role['id'] }}:{{ Auth()->user()->id }}"
                                                                @if($role['id'].':'.Auth()->user()->id == $rec->role_id.':'.$rec->parent_id)
                                                                    selected
                                                                @endif
                                                                >{{ $role['name'] }} - @lang('emp.under_own')</option>
                                                            @if(isset($childrens[$role['id']-1]))
                                                                @foreach($childrens[$role['id']-1] as $children)
                                                                    <option value="{{ $role['id'] }}:{{ $children->id }}"
                                                                    @if($role['id'].':'.$children->id == $rec->role_id.':'.$rec->parent_id)
                                                                        selected
                                                                    @endif
                                                                    >{{ $role['name'] }} - @lang('emp.under') {{ $children->empcode }}</option>
                                                                @endforeach
                                                            @endif
                                                        </optgroup>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>
                                            @endif

                                            {{--
											@if (Auth()->user()->id == 15)
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Ветка <span
                                                                class="req">*</span></label>
                                                    <div class="col-sm-10">
                                                        <select name="invite_id" class="form-control">
    														@if (isset($invites))
                                                            @if ($invites)
                                                                @foreach ($invites as $invite)
                                                                    <option value="{{ $invite }}" @if ($invite == $rec->invite_id) selected @endif>{{ $invite }}</option>
                                                                @endforeach
                                                            @endif
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
											@endif

                                            --}}


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_group') <span class="req">*</span></label>
                                                <div class="col-sm-10">
                                                    <input id="groups_input" type="text" name="groups" value="{{$ing}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed" style="display: none;"></div>
                                            <div class="form-group" style="display: none;">
                                                <label class="col-sm-2 control-label">@lang('emp.form_city') <span
                                                            class="req">*</span></label>
                                                <div class="col-sm-10"><input type="text" name="city_id" value="n/a"
                                                                              class="form-control"></div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_addr') <span
                                                            class="req">*</span></label>
                                                <div class="col-sm-10"><input type="text" name="address"
                                                                              placeholder="@lang('emp.form_addr_placeholder')"
                                                                              value="{{ old('address', $rec->address) }}"
                                                                              class="form-control"></div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_name') <span
                                                            class="req">*</span></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="name" value="{{ old('name', $rec->name) }}"
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_email') <span class="req">*</span></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="email"
                                                           value="{{ old('email', $rec->email) }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>

                                            @if(!isset($id) || Auth::user()->id != $id)
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_status') <span class="req">*</span></label>
                                                <div class="col-sm-10">
                                                    <select name="emp_status" class="form-control">
                                                        <option value="active"
                                                                @if (old('emp_status', $rec->emp_status) == 'active') selected @endif>
                                                            @lang('emp.active')
                                                        </option>
                                                        <option value="pause"
                                                                @if (old('emp_status', $rec->emp_status) == 'pause') selected @endif>
                                                            @lang('emp.pause')
                                                        </option>
                                                        <option value="deleted"
                                                                @if (old('emp_status', $rec->emp_status) == 'deleted') selected @endif>
                                                            @lang('emp.deleted')
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            @endif

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_pass') @if (!isset($id))<span
                                                            class="req">*</span>@endif</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="password" value="" class="form-control">
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">@lang('emp.form_pass1') @if (!isset($id))
                                                        <span class="req">*</span>@endif</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="password_confirmation" value=""
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            @if (!isset($id))
                                                <button class="btn btn-primary" type="submit">@lang('emp.button_add')</button>
                                            @else
                                                <button class="btn btn-primary" type="submit">@lang('emp.button_save')</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
        </div>
        <div class="footer">
            <div class="pull-right">

            </div>
            <div>
            </div>
        </div>
    </div>
@endsection
