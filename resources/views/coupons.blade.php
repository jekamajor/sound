@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">
                    <h2>@lang('coupon.title')</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/coupon/">@lang('coupon.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('coupon.title')</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-4 text-right">
					@if (Auth::user()->has_right('coupon/add'))
					<a href="/{{ App::getLocale() }}/admin/coupon/add" class="btn btn-danger" style="margin-top: 30px;">@lang('coupon.button_new')</a>
					@endif
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
				<div class="col-lg-12">
					<form action="/{{ App::getLocale() }}/admin/coupon" class="form form-inline">
						<div class="form-group">
							<input type="text" name="nr" placeholder="" value="{{ $search }}" class="form-control">
							<button type="submit" class="btn btn-default">@lang('coupon.search')</button>
						</div>
					</form>
					<br />
				</div>
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <table data-nosort="0,10" class="dataTable display footable table table-stripped toggle-arrow-tiny" data-page-size="25">
                                <thead>
                                <tr>
                                    <th data-toggle="true">
                                        <span>#</span>
                                    </th>
                                    <th data-toggle="true">
										<span>@lang('coupon.table_name')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('coupon.table_email')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('coupon.table_price')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('coupon.table_user')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('coupon.table_expires')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('coupon.table_lasta')</span>
									</th> 
									<th data-toggle="true"><span>@lang('coupon.table_coupon')</span></th>
									<th data-toggle="true"><span>@lang('coupon.table_status')</span></th>
									<th data-toggle="true"><span>@lang('coupon.table_activated')</span></th>
                                    <th class="text-right" data-sort-ignore="true"><span>@lang('common.table_actions')</span></th>
                                </tr>
                                </thead>
                                <tbody>
									@if (Auth::user()->has_right('coupon') or Auth::user()->has_right('coupon/'))
									@if ($list->count())
										@foreach ($list as $key => $record)
											<tr class="footable-odd">
												<td class="footable-visible">{{ ($key+1) }}</td>	
												<td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/coupon/info/{{ $record->id }}">{{ $record->number }}</a></td>
												<td class="footable-visible">{{ $record->owner['email'] }}</td>
												<td class="footable-visible">{{ $record->packprice }}</td>
												<td class="footable-visible">
													@if (!is_null($record->owner))
														<a href="/{{ App::getLocale() }}/admin/users/info/{{ $record->owner['id'] }}">{{ $record->owner['name'] }}</a>
													@else
														@lang('common.nstated_he')
													@endif
												</td>
												<td class="footable-visible">
													@if (strtotime($record->date_expires))
														{{ date('d.m.Y - H:i', strtotime($record->date_expires)) }}
													@else
														&mdash;
													@endif
												</td>
												<td class="footable-visible">
													@if (strtotime($record->owner['last_login']))
														{{ date('d.m.Y - H:i', strtotime($record->owner['last_login'])) }}
													@else
														&mdash;
													@endif
												</td>
												<td class="footable-visible">
													<span class="badge badge-info">{{ $record->packname }}</span>
												</td>
												<td class="footable-visible set_st{{ $record->id }}">
													@if ($record->status == 'active')
														<span class="badge badge-success">@lang('coupon.active')</span>
													@endif
													@if ($record->status == 'pause')
														<span class="badge badge-warning">@lang('coupon.pause')</span>
													@endif							
													@if ($record->status == 'expires')
														<span class="badge badge-danger">@lang('coupon.expires')</span>
													@endif
													@if ($record->status == 'na')
														<span class="badge badge-primary">@lang('coupon.na')</span>
													@endif
													@if ($record->status == 'ended')
														<span class="badge badge-danger">@lang('coupon.ended')</span>
													@endif
												</td>
												<td class="footable-visible set_st{{ $record->id }}_act">
													@if (strtotime($record->date_activated))
														{{ date('d.m.Y - H:i', strtotime($record->date_activated)) }}
													@else
														&mdash;
													@endif
												</td>
												<td class="text-right footable-visible footable-last-column">
													@if ($record->status == 'active')
														<div class="btn-group">
															<a href="/{{ App::getLocale() }}/admin/coupon/pause/{{ $record->id }}" data-id="{{ $record->id }}" data-st="@lang('coupon.pause')" class="btn-white btn btn-xs coupon_action">@lang('coupon.stop')</a>
															<a href="/{{ App::getLocale() }}/admin/coupon/start/{{ $record->id }}" data-id="{{ $record->id }}" data-st="@lang('coupon.active')" class="btn-white btn btn-xs coupon_action" style="display: none;">@lang('coupon.set_active')</a>
														</div>
													@else
														<div class="btn-group">
															<a href="/{{ App::getLocale() }}/admin/coupon/start/{{ $record->id }}" data-id="{{ $record->id }}" data-st="@lang('coupon.active')" class="btn-white btn btn-xs coupon_action">@lang('coupon.set_active')</a>
															<a href="/{{ App::getLocale() }}/admin/coupon/pause/{{ $record->id }}" data-id="{{ $record->id }}" data-st="@lang('coupon.pause')" class="btn-white btn btn-xs coupon_action" style="display: none;">@lang('coupon.stop')</a>
														</div>														
													@endif
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td>@lang('common.nodata')</td>
										</tr>
									@endif
									@else
										<tr><td>@lang('common.noright')</td></tr>
									@endif									
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection