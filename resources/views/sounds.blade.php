@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">
                    <h2>@lang('sounds.title')</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/sounds/">@lang('sounds.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('sounds.title')</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-4 text-right">
					@if (Auth::user()->has_right('sounds/add'))
					<a href="/{{ App::getLocale() }}/admin/sounds/add" class="btn btn-danger" style="margin-top: 30px;">@lang('sounds.button')</a>
					@endif
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
				<div class="col-lg-12">
					<form action="/{{ App::getLocale() }}/admin/sounds" class="form form-inline">
						<div class="form-group">
							<input type="text" name="nr" placeholder="@lang('sounds.placeholder')" value="{{ $search }}" class="form-control" style="width:300px;">
							<button type="submit" class="btn btn-default">@lang('sounds.search')</button>
						</div>
					</form>
					<br />
				</div>			
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <table data-nosort="0,10" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">
										<span>#</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.table_name')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('sounds.table_ras')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.wawe_1')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.wawe_2')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.wawe_3')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.wawe_4')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.wawe_5')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.table_status')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('sounds.table_created')</span>
									</th>
                                    <th class="text-right" data-sort-ignore="true"><span>@lang('common.table_actions')</span></th>
                                </tr>
                                </thead>
                                <tbody>
									@if (Auth::user()->has_right('sounds') or Auth::user()->has_right('sounds/'))
									@if ($list->count())
										@foreach ($list as $key => $record)
											<tr class="footable-odd">
												<td class="footable-visible">{{ ($key+1) }}</td>	
												<td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/sounds/info/{{ $record->id }}">{{ $record->name }}</a></td>
												<td class="footable-visible">{{ $record->dandzy }}</td>
												<td class="footable-visible">{{ $record->hz1 }}</td>
												<td class="footable-visible">{{ $record->hz2 }}</td>
												<td class="footable-visible">{{ $record->hz3 }}</td>
												<td class="footable-visible">{{ $record->hz4 }}</td>
												<td class="footable-visible">{{ $record->hz5 }}</td>
												<td class="footable-visible">
													@if ($record->status == 'active')
														<span class="badge badge-success">@lang('sounds.active')</span>
													@elseif ($record->status == 'deleted')
														<span class="badge badge-danger">@lang('sounds.deleted')</span>
													@else
														<span class="badge badge-primary">@lang('sounds.nactive')</span>
													@endif
												</td>
												<td class="footable-visible">{{ date('d.m.Y - H:i', strtotime($record->created_at)) }}</td>
												<td class="text-right footable-visible footable-last-column">
													<div class="btn-group">
														<a href="/{{ App::getLocale() }}/admin/sounds/edit/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-pencil-alt"></i></a>
														<a href="/{{ App::getLocale() }}/admin/delete_record/sounds/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-times"></i></a>
													</div>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td>@lang('common.nodata')</td>
										</tr>
									@endif
									@else
										<tr><td>@lang('common.noright')</td></tr>
									@endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
				<div class="col-lg-12">
					
				</div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection