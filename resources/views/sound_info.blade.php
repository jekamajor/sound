@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('sounds.one') {{ $rec->name }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/sounds/">@lang('sounds.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('sounds.one') {{ $rec->name }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			<form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('sounds.table_name')</td>
								<td>{{ $rec->name }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.table_ras')</td>
								<td>{{ $rec->dandzy }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.table_status')</td>
								<td>
									@if ($rec->status == 'active')
										<span class="badge badge-success">@lang('sounds.active')</span>
									@endif
									@if ($rec->status == 'pause')
										<span class="badge badge-warning">@lang('sounds.pause')</span>
									@endif
									@if ($rec->status == 'deleted')
										<span class="badge badge-danger">@lang('sounds.deleted')</span>
									@endif
									@if ($rec->status == 'nactive')
										<span class="badge badge-primary">@lang('sounds.nactive')</span>
									@endif	
									@if ($rec->status == 'none')
										<span class="badge badge-primary">@lang('sounds.none')</span>
									@endif										
								</td>
							</tr>
							<tr>
								<td>@lang('sounds.table_created')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->created_at)) }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.table_updated')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->updated_at)) }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.table_d')</td>
								<td>{{ $rec->duration }}</td>
							</tr>							
							</table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('sounds.wawe_1')</td>
								<td>{{ $rec->hz1 }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.wawe_2')</td>
								<td>{{ $rec->hz2 }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.wawe_3')</td>
								<td>{{ $rec->hz3 }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.wawe_4')</td>
								<td>{{ $rec->hz4 }}</td>
							</tr>
							<tr>
								<td>@lang('sounds.wawe_5')</td>
								<td>{{ $rec->hz5 }}</td>
							</tr>							
							</table>
                        </div>
                    </div>
                </div>

				@if($rec->trash == 0)
				<div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/sounds/add" class="btn btn-success">@lang('sounds.button_new')</a>
					<a href="/{{ App::getLocale() }}/admin/sounds/edit/{{ $rec->id }}" class="btn btn-primary">@lang('sounds.button_edit')</a>
					<a href="/{{ App::getLocale() }}/admin/delete_record/sounds/{{ $rec->id }}" class="btn btn-danger">@lang('sounds.button_delete')</a>
				</div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/sounds/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/sounds/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
				</div>
				@endif

            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection