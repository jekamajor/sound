@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('coupon.one') {{ $rec->number }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.sec')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/coupon/">@lang('coupon.sec')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('coupon.one') {{ $rec->number }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			<form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('coupon.table_nr')</td>
								<td>{{ $rec->number }}</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_coupon')</td>
								<td>
									<span class="badge badge-info">{{ $rec->packname }}</span>
								</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_price')</td>
								<td>{{ $rec->packprice }} @lang('coupon.currency')</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_bought')</td>
								<td><a href="/{{ App::getLocale() }}/admin/users/info/{{ $rec->user_id }}">{{ $rec->owner['name'] }}</a></td>
							</tr>
							<tr>
								<td>@lang('coupon.table_status')</td>
								<td>
									@if ($rec->status == 'active')
										<span class="badge badge-success">@lang('coupon.active')</span>
									@endif
									@if ($rec->status == 'pause')
										<span class="badge badge-warning">@lang('coupon.pause')</span>
									@endif							
									@if ($rec->status == 'expires')
										<span class="badge badge-danger">@lang('coupon.expires')</span>
									@endif
									@if ($rec->status == 'na')
										<span class="badge badge-primary">@lang('coupon.na')</span>
									@endif
									@if ($rec->status == 'ended')
										<span class="badge badge-danger">@lang('coupon.ended')</span>
									@endif									
								</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('coupon.table_email')</td>
								<td>{{ $rec->owner['email'] }}</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_expires_full')</td>
								<td>
									@if (strtotime($rec->date_expires))
										{{ date('d.m.Y - H:i', strtotime($rec->date_expires)) }}
									@else
										&mdash;
									@endif							
								</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_lasta_full')</td>
								<td>
									@if (strtotime($rec->owner['last_login']))
										{{ date('d.m.Y - H:i', strtotime($rec->owner['last_login'])) }}
									@else
										&mdash;
									@endif								
								</td>
							</tr>
							<tr>
								<td>@lang('coupon.table_activated_full')</td>
								<td>
									@if (strtotime($rec->date_activated))
										{{ date('d.m.Y - H:i', strtotime($rec->date_activated)) }}
									@else
										&mdash;
									@endif								
								</td>
							</tr>
							<tr>
								<td>@lang('coupon.period')</td>
								<td>
									<span class="badge badge-info">{{Lang::get('coupon.'.$rec->expires)}}</span>
								</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>
				@if($rec->trash == 0)
				<div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/coupon/add" class="btn btn-success">@lang('coupon.button_new')</a>
					<a href="/{{ App::getLocale() }}/admin/delete_record/coupon/{{ $rec->id }}" class="btn btn-danger">@lang('coupon.button_delete')</a>
				</div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/coupon/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/coupon/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
				</div>
				@endif
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection