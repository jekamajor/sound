@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">

        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('groups.one') {{ $rec->name }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/groups/">@lang('groups.one')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('groups.one') {{ $rec->name }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			<form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('groups.table_name')</td>
								<td>{{ $rec->name }}</td>
							</tr>
							<tr>
								<td>@lang('groups.table_status')</td>
								<td>
									@if ($rec->status == 'active')
										<span class="badge badge-success">@lang('groups.active')</span>
									@endif
									@if ($rec->status == 'pause')
										<span class="badge badge-warning">@lang('groups.pause')</span>
									@endif							
									@if ($rec->status == 'deleted')
										<span class="badge badge-danger">@lang('groups.deleted')</span>
									@endif
								</td>
							</tr>
							<tr>
								<td>@lang('groups.table_who')</td>
								<td>
                                    @if ($rec->created_by == 15)
                                        <a href="/{{ App::getLocale() }}/admin/emp/info/15">ADMINISTRATOR</a>
                                    @else
                                        @if (!empty($rec->owner['name']))
                                            <a href="/{{ App::getLocale() }}/admin/emp/info/{{$rec->owner['id']}}">{{$rec->owner['name'].' '.$rec->owner['last_name']}}</a>
                                        @else
                                            @lang('groups.na')
                                        @endif
                                    @endif
								</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('groups.table_created')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->created_at)) }}</td>
							</tr>
							<tr>
								<td>@lang('groups.table_updated')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->updated_at)) }}</td>
							</tr>							
							</table>
                        </div>
                    </div>
                </div>

                @if($rec->trash == 0)
				<div class="col-lg-12">
                    <a href="/{{ App::getLocale() }}/admin/groups/add" class="btn btn-primary" style="background-color: #3c8dbb;border-color: #3c8dbb;">@lang('groups.button')</a>
					<a href="/{{ App::getLocale() }}/admin/groups/edit/{{ $rec->id }}" class="btn btn-primary">@lang('groups.button_edit')</a>
					<a href="/{{ App::getLocale() }}/admin/delete_record/groups/{{ $rec->id }}" class="btn btn-danger">@lang('groups.button_delete')</a>
					<br />
					<br />
				</div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/groups/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/groups/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
                    <br />
					<br />
				</div>
				@endif

				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-content">
							<h3>@lang('groups.members')</h3>
                            <table class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">@lang('groups.table1_user')</th>
									<th data-toggle="true">@lang('groups.table1_added')</th> 
                                </tr>
                                </thead>
                                <tbody>
									@if (isset($rec->users))
										@foreach ($rec->users as $u)
											<tr>
												<td>

                                                @if($u->role == 'user' && in_array($u->parent_id, Auth::user()->getEmpChildrens(true)))
                                                    <a href="/{{ App::getLocale() }}/admin/users/info/{{ $u->id }}">{{ $u->name }}</a>
                                                @elseif($u->role == 'emp' && Auth::user()->empChildrenExist($u->id, true))
                                                    <a href="/{{ App::getLocale() }}/admin/emp/info/{{ $u->id }}">{{ $u->name }}</a>
                                                @else
                                                    {{ $u->name }}
                                                @endif

                                                </td>
												<td>{{ date('d.m.Y - H:i', strtotime($u->pivot->created_at)) }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection