@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">

        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>		
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
					@if (Route::current()->getName() == 'admin_emp')
						<h2>@lang('emp.title')</h2>
					@else
						<h2>@lang('emp.userlist')</h2>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/emp/">@lang('emp.sec')</a>
                        </li>
                        <li class="active">
							@if (Route::current()->getName() == 'admin_emp')
								<strong>@lang('emp.title')</strong>
							@else
								<strong>@lang('emp.userlist')</strong>
							@endif
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
					@if (Auth::user()->has_right('emp/add'))
					<a href="/{{ App::getLocale() }}/admin/emp/add" class="btn btn-danger" style="margin-top: 30px;">@lang('emp.button')</a>
					@endif
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">
				<div class="col-lg-12">
					<form action="/{{ App::getLocale() }}/admin/emp" class="form form-inline">
						<div class="form-group">
							<input type="text" name="nr" placeholder="@lang('emp.placeholder')" value="{{ $search }}" class="form-control" style="width:240px">
							<button type="submit" class="btn btn-default">@lang('emp.search')</button>
						</div>
					</form>
					<br />
				</div>				
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <table data-nosort="0,8" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">
										<span>#</span>
									</th>
									<th data-toggle="true">
										<span>@lang('emp.table_name')</span>
									</th>
									<th data-toggle="true"><span>@lang('emp.table_role')</span>
                                    </th>
									<th data-toggle="true">
										<span>@lang('emp.table_email')</span>
									</th> 
									<th data-toggle="true">
										<span>@lang('emp.table_status')</span>
									</th>
									<th data-toggle="true"><span>@lang('emp.table_who')</th>
									<th data-toggle="true">
										<span>@lang('emp.table_when')</span>
									</th>
									<th data-toggle="true"><span>@lang('emp.table_code')</span></th>
                                    <th class="text-right" data-sort-ignore="true"><span>@lang('common.table_actions')</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!isset($_GET['nr']))
                                        @if (Auth()->user()->isAdmin())
                                        <tr class="footable-odd">
                                            <td class="footable-visible">1</td>
                                            <td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/emp/info/15">{{config('app.officialName')}}</a></td>
                                            <td class="footable-visible">[0] {{config('app.officialName')}}</td>
                                            <td class="footable-visible">{{ Auth::user()->email }}</td>
                                            <td class="footable-visible"><span class="badge badge-success">@lang('emp.active')</span></td>
                                            <td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/emp/info/{{ Auth::user()->id }}">{{config('app.officialName')}}</a></td>
                                            <td class="footable-visible"></td>
                                            <td class="footable-visible">[0][000][0000]</td>
                                            <td class="text-right footable-visible footable-last-column">
                                                <div class="btn-group">
                                                    <a href="/{{ App::getLocale() }}/admin/emp/edit/{{ Auth::user()->id }}" class="btn-white btn btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @else
                                        <tr class="footable-odd">
                                            <td class="footable-visible">1</td>
                                            <td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/emp/info/{{ Auth()->user()->id }}">{{ Auth()->user()->name }}</a></td>
                                            <td class="footable-visible">{{ Auth::user()->rolename }}</td>
                                            <td class="footable-visible">{{ Auth::user()->email }}</td>
                                            <td class="footable-visible"><span class="badge badge-success">@lang('emp.active')</span></td>
                                            <td class="footable-visible">
                                                {!! Auth::user()->parentLink !!}
                                            </td>
                                            <td class="footable-visible">{{ date('d.m.Y - H:i', strtotime(Auth::user()->created_at)) }}</td>
                                            <td class="footable-visible">{{ Auth::user()->empcode }}</td>
                                            <td class="text-right footable-visible footable-last-column">
                                                <div class="btn-group">
                                                    <a href="/{{ App::getLocale() }}/admin/emp/edit/{{ Auth::user()->id }}" class="btn-white btn btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
									@endif

									@if (Auth::user()->has_right('emp') or Auth::user()->has_right('emp/'))
									@if ($list->count())
										@foreach ($list as $key => $record)
											@if ($record->role_id == 6)
												@continue
											@endif
											@if ($record->id == 15 && !isset($_GET['nr']))
												@continue;
											@endif
											<tr class="footable-odd">
												<td class="footable-visible">{{ ($key+2) }}</td>		
												<td class="footable-visible">
                                                    @if ($record->id == 15)
														<a href="/{{ App::getLocale() }}/admin/emp/info/15">{{config('app.officialName')}}</a>
													@else
                                                        <a href="/{{ App::getLocale() }}/admin/emp/info/{{$record->id}}">{{ $record->name }}</a>
                                                    @endif
                                                </td>
												<td class="footable-visible">{{ $record->rolename }}</td>
												<td class="footable-visible">{{ $record->email }}</td>
												<td class="footable-visible">
													@if ($record->emp_status == 'active')
														<span class="badge badge-success">@lang('emp.active')</span>
													@endif
													@if ($record->emp_status == 'pause')
														<span class="badge badge-warning">@lang('emp.pause')</span>
													@endif
													@if ($record->emp_status == 'deleted')
														<span class="badge badge-danger">@lang('emp.deleted')</span>
													@endif
												</td>
												<td>
                                                    {!! $record->parentLink !!}
												</td>
												<td>{{ date('d.m.Y - H:i', strtotime($record->created_at)) }}</td>
												<td>{{ $record->empcode }}</td>
												<td class="text-right footable-visible footable-last-column">
													<div class="btn-group"> 
														<a href="/{{ App::getLocale() }}/admin/emp/edit/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-pencil-alt"></i></a>
														<a style="display: none;" href="/{{ App::getLocale() }}/admin/delete_record/emp/{{ $record->id }}" class="btn-white btn btn-xs"><i class="fas fa-times"></i></a>
													</div>
												</td>
											</tr>
										@endforeach
									@else
					
									@endif
									@else
										<tr><td colspan="9">@lang('common.noright')</td></tr>
									@endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
				<div class="col-lg-12">
					
				</div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection
