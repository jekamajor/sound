@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>			
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('cats.one') {{ $rec->name }}</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/cats">@lang('cats.title1')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('cats.one') {{ $rec->name }}</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			<form action="{{ route('admin_cats', App::getLocale()) }}" method="GET">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('cats.table_cat')</td>
								<td>{{ $rec->name }}</td>
							</tr>
							<tr>
								<td>@lang('cats.table_who')</td>
                                <td>
                                    @if ($rec->official == 'on')
                                        <a href="/{{ App::getLocale() }}/admin/emp/info/15">{{config('app.officialName')}}</a>
                                    @elseif (!empty($rec->owner['name']))
                                        <a href="/{{ App::getLocale() }}/admin/users/info/{{$rec->owner['id']}}">{{$rec->owner['name'].' '.$rec->owner['last_name']}}</a>
                                    @else
                                        @lang('common.nstated_he')
                                    @endif
                                </td>
							</tr>
							<tr>
								<td>@lang('cats.table_status')</td>
								<td>
									@if ($rec->status == 'active')
										<span class="badge badge-success">@lang('cats.active')</span>
									@endif
									@if ($rec->status == 'pause')
										<span class="badge badge-warning">@lang('cats.pause')</span>
									@endif
									@if ($rec->status == 'deleted')
										<span class="badge badge-danger">@lang('cats.deleted')</span>
									@endif
									@if ($rec->status == 'na')
										<span class="badge badge-primary">@lang('cats.na')</span>
									@endif
								</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox">
                        <div class="ibox-content">
							<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
							<tr>
								<td>@lang('cats.table_created')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->created_at)) }}</td>
							</tr>
							<tr>
								<td>@lang('cats.table_updated')</td>
								<td>{{ date('d.m.Y - H:i', strtotime($rec->updated_at)) }}</td>
							</tr>
							</table>
                        </div>
                    </div>
                </div>

                @if($rec->trash == 0)
				<div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/cats/add" class="btn btn-success">@lang('cats.button')</a>
					<a href="/{{ App::getLocale() }}/admin/cats/edit/{{ $rec->id }}" class="btn btn-primary">@lang('cats.button_edit')</a>
					<a href="/{{ App::getLocale() }}/admin/delete_record/cats/{{ $rec->id }}" class="btn btn-danger">@lang('cats.button_delete')</a>
					<br /><br />
				</div>
                @else
                 <div class="col-lg-12">
					<a href="/{{ App::getLocale() }}/admin/trash/restore/cats/{{ $rec->id }}" class="btn btn-restore">@lang('common.button_restore')</a>
					<a href="/{{ App::getLocale() }}/admin/trash/delete/cats/{{ $rec->id }}" class="btn btn-danger">@lang('common.button_delete')</a>
                    <br /><br />
				</div>
				@endif

				<div class="col-lg-12">
					<div class="ibox">
						<div class="ibox-content">
							<h3>@lang('cats.table1_title')</h3>
                            <table class="footable table dataTable table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">#</th>
                                    <th data-toggle="true">@lang('cats.table1_course')</th>
                                    <th data-toggle="true">@lang('cats.table1_status')</th>
									<th data-toggle="true">@lang('cats.table1_added')</th>
									<!--th data-toggle="true">@lang('cats.table1_updated')</th-->
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rec->courses as $key => $r)
                                        <tr>
                                            <td>{{ ($key + 1) }}</td>
                                            <td><a href="/{{ App::getLocale() }}/admin/courses/info/{{ $r->id }}">{{ $r->name }}</a></td>
                                            <td>
                                                @if ($r->status == 'active')
                                                    <span class="badge badge-success">@lang('course.active')</span>
                                                @endif
                                                @if ($r->status == 'none')
                                                    <span class="badge badge-primary">@lang('course.none')</span>
                                                @endif
                                                @if ($r->status == 'pause')
                                                    <span class="badge badge-warning">@lang('course.pause')</span>
                                                @endif
                                                @if ($r->status == 'deleted')
                                                    <span class="badge badge-danger">@lang('course.deleted')</span>
                                                @endif
                                            </td>
                                            <td>{{ date('d.m.Y - H:i', strtotime($r->pivot->created_at)) }} </td>
                                            <!--td>{{ date('d.m.Y - H:i', strtotime($r->pivot->updated_at)) }}</td-->
                                        </tr>
                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection
