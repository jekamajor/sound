@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
					@if (!isset($id))
						<h2>@lang('users.title_add')</h2>
					@else
						<h2>@lang('users.title_edit')</h2>
					@endif
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/users">@lang('users.sec')</a>
                        </li>
                        <li class="active">
							@if (!isset($id))
								<strong>@lang('users.title_add')</strong>
							@else
								<strong>@lang('users.title_edit')</strong>
							@endif
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
			@if (!isset($id))
				<form action="{{ route('admin_uadd', App::getLocale()) }}" method="POST" class="form-horizontal">
			@else
				<form action="{{ route('admin_uedit', [App::getLocale(), $id]) }}" method="POST" class="form-horizontal">
			@endif
			@csrf
			<div class="row">
				<div class="col-lg-12">
					@if ($errors->has('city_id'))
						<div class="alert alert-danger">{{ $errors->first('city_id') }}</div>
					@endif
					@if ($errors->has('last_name'))
						<div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
					@endif
					@if ($errors->has('name'))
						<div class="alert alert-danger">{{ $errors->first('name') }}</div>
					@endif
					@if ($errors->has('email'))
						<div class="alert alert-danger">{{ $errors->first('email') }}</div>
					@endif
					@if ($errors->has('phone'))
						<div class="alert alert-danger">{{ $errors->first('phone') }}</div>
					@endif
					@if ($errors->has('password'))
						<div class="alert alert-danger">{{ $errors->first('password') }}</div>
					@endif
				</div>
				<div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>@lang('users.box_general')</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_group') <span class="req"></span></label>
								<div class="col-sm-10">
                                    <input id="groups_input" type="text" name="groups" value="{{$ing}}" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed" style="display: none;"></div>
							<div class="form-group" style="display: none;">
								<label class="col-sm-2 control-label">@lang('users.form_role')</label>
								<div class="col-sm-10">
									<select name="role" class="form-control">
										<option value="user" @if (old('role', $rec->role) == 'user') selected @endif>@lang('users.one')</option>
										<option value="admin" @if (old('admin', $rec->role) == 'admin') selected @endif>@lang('users.admin')</option>
									</select>
								</div>
							</div>
							<div class="hr-line-dashed" style="display: none;"></div>
							<div class="form-group" style="display: none;">
								<label class="col-sm-2 control-label">@lang('users.form_city') <span class="req">*</span></label>
                                <div class="col-sm-10"><input type="text" name="city_id" value="1" class="form-control"></div>
							</div>
							<div class="hr-line-dashed"></div>
                            <div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_addr') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="address" placeholder="@lang('users.form_addr_placeholder')" value="{{ old('address', $rec->address) }}" class="form-control"></div>
                            </div>
							<div class="hr-line-dashed"></div>
                            <div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_lname') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="last_name" value="{{ old('last_name', $rec->last_name) }}" class="form-control"></div>
                            </div>
							<div class="hr-line-dashed"></div>
                            <div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_name') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="name" value="{{ old('name', $rec->name) }}" class="form-control"></div>
                            </div>
							<div class="hr-line-dashed"></div>
                            <div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_email') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="email" value="{{ old('email', $rec->email) }}" class="form-control"></div>
                            </div>
							<div class="hr-line-dashed"></div>
                            <div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_phone') <span class="req">*</span></label>
								<div class="col-sm-10"><input type="text" name="phone" value="{{ old('phone', $rec->phone) }}" class="form-control"></div>
                            </div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_comment')</label>
								<div class="col-sm-10">
									<textarea name="comment" class="form-control">{{ old('comment', $rec->comment) }}</textarea>
								</div>
							</div>
							<div class="hr-line-dashed" style="display: none;"></div>
							<div class="form-group" style="display: none;">
								@if (!isset($id))
									<label class="col-sm-2 control-label">@lang('users.table_cstatus')</label>
									<div class="col-sm-10">
										<select name="status" class="form-control">
											<option value="pause" @if (old('status', $rec->status) == 'pause') selected @endif>@lang('users.paused')</option>
											<option value="active" @if (old('status', $rec->status) == 'active') selected @endif>@lang('users.active')</option>
										</select>
									</div>
								@else
									<label class="col-sm-2 control-label">@lang('users.table1_coupon')</label>
									<div class="col-sm-10">
										@if ($coupon)
											<p><a href="/admin/coupon/info/{{ $coupon->id }}">{{ $coupon->number }}</a></p>
										@else
											<div class="alert alert-warning">@lang('users.table1_notfound')</div>
										@endif
									</div>
								@endif
							</div>
                        </div>
                    </div>
				</div>
				<div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>@lang('users.box_pass')</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
						<div class="ibox-content">
							@if (isset($id))
								<div class="alert alert-info">@lang('users.msg_pass')</div>
							@endif
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_pass')</label>
								<div class="col-sm-10">
									<input type="text" name="password" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">@lang('users.form_pass1')</label>
								<div class="col-sm-10">
									<input type="text" name="password_confirmation" class="form-control">
								</div>
							</div>
                        </div>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-sm-4">
							@if (!isset($id))
								<button class="btn btn-primary" type="submit">@lang('users.button_add')</button>
							@else
								<button class="btn btn-primary" type="submit">@lang('users.button_save')</button>
							@endif
						</div>
					</div>
				</div>
			</div>
			</form>
        </div>
        <div class="footer">
            <div class="pull-right">

            </div>
            <div>
            </div>
        </div>
        </div>
@endsection
