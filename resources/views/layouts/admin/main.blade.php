<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-translate-customization" content="b6703fd8b1d84044-bd4dd2611d59f8dc-g6a8fe920a18c8f60-d"></meta>
    <title>{{ $page_title }}</title>
    <link href="/adm/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" rel="stylesheet">
    <link href="/adm/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/adm/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
    <link href="/adm/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/adm/css/animate.css" rel="stylesheet">
    <link href="/adm/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.tagsinput-revisited.css">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="/adm/css/custom.css?v2">


</head>
<body class="">
<div id="google_translate_element"></div>
<form id="logout_form" action="/logout" method="POST" style="display: none;">
    @csrf
</form>
<div class="modal fade" id="change_ava" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-info">@lang('common.select_image')</div>
                <form action="{{ route('admin_main1') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="avatar" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('common.cancel')</button>
                <button type="submit" class="btn btn-primary">@lang('common.save')</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">

						<a href="javascript:void(0);" data-toggle="modal" data-target="#change_ava">
							@if (!Auth()->user()->avatar)
                                <img alt="image" class="img-circle ava" src="/adm/img/profile_small.jpg"/>
                            @else
                                <img alt="image" class="img-circle ava" src="/{{ Auth()->user()->avatar }}">
                            @endif
						</a>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">
								@if (Auth()->user()->role_id == 1)
                                            ADMINISTRATOR
                                        @endif
                                        @if (Auth()->user()->role_id == 2)
                                            [1] DISTRIBUTOR OEM
                                        @endif
                                        @if (Auth()->user()->role_id == 3)
                                            [2] AREAMASTER
                                        @endif
                                        @if (Auth()->user()->role_id == 4)
                                            [3] DIRECTOR
                                        @endif
                                        @if (Auth()->user()->role_id == 5)
                                            [4] PRACTITIONER
                                        @endif
							</strong><br/>
                                    {!! Auth()->user()->empcodeLink !!}

                            </span>
                            </span>
                        </a>

                        <div class="dropdown">
                        @if (Auth()->user()->isAdmin())
                            <button class="text-muted text-xs block btn btn-danger" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white; margin-top: 10px; width:100%;">@lang('common.button_create') <b class="caret"></b></button>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs" aria-labelledby="dropdownMenuButton">
                                @if (Auth::user()->has_right('cats/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/cats/add">@lang('common.create_cat')</a></li>
                                @endif
                                @if (Auth::user()->has_right('courses/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/courses/add">@lang('common.create_course')</a></li>
                                @endif
                                @if (Auth::user()->has_right('sounds/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/sounds/add">@lang('common.create_sound')</a></li>
                                @endif
                                @if (Auth::user()->has_right('users/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/users/add">@lang('common.create_user')</a></li>
                                @endif
                                @if (Auth::user()->has_right('emp/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/emp/add">@lang('common.create_emp')</a></li>
                                @endif
                                @if (Auth::user()->has_right('coupon/add'))
                                    <li><a href="/{{ App::getLocale() }}/admin/coupon/add">@lang('common.create_coupon')</a></li>
                                @endif
                                <li><a href="javascript:void(0);" class="logout_do">@lang('common.logout')</a></li>
                            </ul>
                        @endif
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                {{-- Основная информация --}}
                    <li @if (Route::current()->getName() == 'admin_main') class="active" @endif><a href="/{{ App::getLocale() }}/admin"><i
                                class="fa fa-info"></i> <span class="nav-label">@lang('common.menu_dashboard')</span></a></li>

                {{-- Сотрудники --}}
                @if(Auth::user()->has_right('emp'))
                    <li @if (Route::current()->getName() == 'admin_emp' or Route::current()->getName() == 'admin_eedit1' or Route::current()->getName() == 'admin_eadd' or Route::current()->getName() == 'admin_eedit' or Route::current()->getName() == 'admin_edit_user') class="active" @endif>
                <a href="/{{ App::getLocale() }}/admin/emp"><i class="fas fa-address-book"></i> <span class="nav-label">@lang('common.menu_emp')</span><span class="fa arrow"></span></a>
                    </li>
                @endif

                {{-- Пользователи --}}
                    <li @if (Route::current()->getName() == 'admin_users' or Route::current()->getName() == 'admin_uedit1' or Route::current()->getName() == 'admin_logs' or Route::current()->getName() == 'admin_uadd' or Route::current()->getName() == 'admin_uedit') class="active" @endif>
                    <a href="/{{ App::getLocale() }}/admin/users"><i class="fa fa-user"></i> <span
                                    class="nav-label">@lang('common.menu_users')</span><span class="fa arrow"></span></a>
                    </li>

                {{--  Купоны / Подписки --}}
                    <li @if (Route::current()->getName() == 'admin_coupon' or Route::current()->getName() == 'admin_couadd' or Route::current()->getName() == 'admin_couedit') class="active" @endif>

                        <a href="/"><i class="fas fa-barcode"></i> <span class="nav-label">@lang('common.menu_coupon')</span><span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li @if (Route::current()->getName() == 'admin_coupon') class="active" @endif><a
                                            href="/{{ App::getLocale() }}/admin/coupon">@lang('common.menu_list')</a></li>
                            @if (Auth()->user()->isAdmin())
                                <li @if (Route::current()->getName() == 'cs') class="active" @endif><a
                                            href="/{{ App::getLocale() }}/admin/coupon/settings">@lang('common.menu_coupon_set')</a></li>
                            @endif
                        </ul>
                    </li>


                {{-- Категории --}}
                    <li @if (Route::current()->getName() == 'admin_cats' or Route::current()->getName() == 'admin_cedit1' or Route::current()->getName() == 'admin_cadd' or Route::current()->getName() == 'admin_cedit') class="active" @endif>
                        <a href="/{{ App::getLocale() }}/admin/cats/"><i class="fas fa-file-audio"></i> <span class="nav-label">@lang('common.menu_cats')</span> <span
                                    class="fa arrow"></span></a>
                    </li>

                {{-- Курсы --}}
                    <li @if (Route::current()->getName() == 'admin_courses' or Route::current()->getName() == 'admin_coadd' or Route::current()->getName() == 'admin_coedit1' or Route::current()->getName() == 'admin_coedit') class="active" @endif>
                    @if (Auth::user()->has_right('cats') or Auth::user()->has_right('cats/') or Auth::user()->has_right('cats/add'))
                        <li><a href="/{{ App::getLocale() }}/admin/courses/"><i class="far fa-file-audio"></i> <span
                                        class="nav-label">@lang('common.menu_courses')</span></a></li>
                        @endif
                        </li>


                {{-- Звуки --}}
                    @if (Auth()->user()->isAdmin())
                        <li @if (Route::current()->getName() == 'admin_sounds' or Route::current()->getName() == 'admin_sedit1' or Route::current()->getName() == 'admin_sadd' or Route::current()->getName() == 'admin_sedit') class="active" @endif>
                            <a href="/{{ App::getLocale() }}/admin/sounds/"><i class="fa fa-music"></i> <span class="nav-label">@lang('common.menu_sounds')</span>
                                <span class="fa arrow"></span></a>
                        </li>
                    @endif


                {{-- Плейлисты --}}
                        <li @if (Route::current()->getName() == 'admin_playlist' or Route::current()->getName == 'admin_user_playlist') class="active" @endif>
                            <a href="/{{ App::getLocale() }}/admin/playlist"><i class="fas fa-file-medical-alt"></i> <span
                                            class="nav-label">@lang('common.menu_playlist')</span></a>
                        </li>


                {{-- Группы --}}
                        <li @if (Route::current()->getName() == 'admin_groups' or Route::current()->getName() == 'admin_gadd' or Route::current()->getName() == 'admin_gedit') class="active" @endif>

                            <a href="/{{ App::getLocale() }}/admin/groups"><i class="far fa-clipboard"></i> <span
                                        class="nav-label">@lang('common.menu_groups')</span><span class="fa arrow"></span></a>
                        </li>

            </ul>

        </div>
    </nav>
    @yield('content')
</div>

<!-- Mainly scripts -->
<script src="/adm/js/jquery-3.1.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="/adm/js/bootstrap.min.js"></script>
<script src="/adm/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/adm/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="/adm/js/inspinia.js"></script>
<script src="/adm/js/plugins/pace/pace.min.js"></script>

<!-- SUMMERNOTE -->
<script src="/adm/js/plugins/summernote/summernote.min.js"></script>

<!-- Data picker -->
<script src="/adm/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="/js/jquery.tagsinput-revisited.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!--script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script-->
<script src="//cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
    var DEBUG = 1;
    $(document).ready(function () {

        $('.dataTable').each(function(){
            //if(!$(this).find('td').length) return true;
            $(this).addClass('dt-responsive').parent().addClass('table-responsive');
            var table = $(this).DataTable({
                //responsive: true,
                //scrollX: true,
                paging: false,
                searching: false,
                aaSorting: [], // убираем сортировку 1го столбца по умолчанию
                columnDefs: [{
                    orderable: false, // отключаем сортировку у столбцов из атрибута таблицы (data-nosort="0,1,6")
                    targets: ($(this).attr('data-nosort') || "").split(',').map(Number)
                }],
            });

            table.on( 'draw', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
            } );

            if($(this).outerHeight() > $(window).height()) {
                new $.fn.dataTable.FixedHeader(table);
            }

        });


        var fixedHeader = false;
        var fixedHeaderBody = false;

        // position sticky c горизонтальной прокруткой в safari не работает, поэтому нужен костыль
        setTimeout(function(){

            var tableParent = $('.table-responsive');

            function scrollFix(el){
                if(fixedHeader.length) {
                    fixedHeaderBody.css({
                        'margin-left': tableParent.scrollLeft() * -1
                    });
                }
                var left = tableParent.scrollLeft();

                if(el && el.length){
                    var left = el.scrollLeft();
                    el.find('.stickyEmulation').css({
                        'left': left,
                    });
                }
                else{

                    $('.stickyEmulation').css({
                        'left': left,
                    });
                }
                $('.fixedHeader-floating .stickyEmulation').css({
                    'left': left,
                });

            }

            function theadFix(){
                $('thead').each(function(){
                    var needTransform = false;
                    var parentFixed = $(this).parents('.fixedHeader-floating').get(0);

                    if(parentFixed){
                        LOG('fixed');
                        var parentTableId = $(parentFixed).attr('aria-describedby');
                        var parentThead = $('#'+parentTableId).parent().find('.dataTable thead');
                        LOG(parentThead);
                        if( ($(parentThead).attr('style') || '').indexOf('transform') >= 0){
                            needTransform = true;
                        }
                    }

                    if(parentFixed){
                        if(needTransform){
                            $(this).css({'transform': 'translateX(-25px)'});
                        }
                    }
                    else{
                        var parent = $(this).parents('.table-responsive').get(0);

                        if(
                            parent && parent.offsetWidth < parent.scrollWidth &&
                            ($(this).attr('style') || '').indexOf('transform') < 0  // еще не передвинут
                        ){
                            $(this).css({'transform': 'translateX(-25px)'});
                        }
                    }

                });

            }

            function fixedHeaderDetect(){
                fixedHeader = $('.fixedHeader-floating');
                if(fixedHeader.length) {
                    fixedHeaderBody = $('.fixedHeader-floating thead');
                    fixedHeader.css({
                        left: tableParent.offset().left - 1,
                        width: tableParent.width() + 15,
                    });
                    fixedHeaderBody.find('th:nth-child(1), th:nth-child(2)').css({
                        'position': 'relative',
                        'left': 0,
                    }).addClass('stickyEmulation');
                }
                theadFix();
                scrollFix();
            }

            function scrolledDetect(){
                tableParent.each(function(){
                    var scrolled = $(this)[0].offsetWidth < $(this)[0].scrollWidth;
                    if(scrolled){
                        $(this).find('thead th:nth-child(1), thead th:nth-child(2), tbody td:nth-child(1), tbody td:nth-child(2)').css({
                            'position': 'relative',
                            'left': 0,
                        }).addClass('stickyEmulation');
                        //$(this).find('tbody td:nth-child(3)').attr('style', 'padding-left: 5px !important');
                        $(this).find('tbody').attr('style', 'transform: translateX(-25px);');
                        theadFix();
                    }
                });
            }

            tableParent.off().on('scroll', function(){
                scrollFix($(this));
            });

            // появляется фиксированный заголовок таблицы в новом элементе DOM
            $(document).on('DOMNodeInserted', function(e) {
                setTimeout(function(){
                    fixedHeaderDetect();
                }, 10);
            });

            scrolledDetect()
            fixedHeaderDetect();

        }, 100);




        $('#delete_avatar').click(function(){
            if(confirm('@lang('users.ava_delete')')){
                $('input[name="delete_avatar"]').prop('checked', true);
                $(this).closest('form').submit();
                return false;
            }
            return false;
        });

        /*$('.coupon_action').click(function () {

            var this_status = $(this).data('st');
            var this_id = $(this).data('id');

            if (typeof this_id == 'undefined') {
                this_id = '';
            }

            var this_action = $(this).attr('href');
            var this_a = $(this);

            $.ajax({

                url: this_action,
                type: 'get',
                success: function (response) {

                    var span_class = 'badge-success';
                    var span_class1 = 'badge-warning';
                    if (this_status == 'На паузе') {

                        span_class = 'badge-warning';
                        span_class1 = 'badge-success';

                    }
					else {

						var d = new Date();
						var da = '0'+d.getDate()+'.0'+d.getMonth()+'.'+d.getFullYear()+' '+d.getHours()+':'+d.getMinutes();
						$('.set_st'+this_id+'_act').text(da);

					}
                    $('.set_st' + this_id).find('span').removeClass(span_class1).addClass(span_class);
                    $('.set_st' + this_id).find('span').html(this_status);

                    $(this_a).hide();
                    $(this_a).next().show();
                    $(this_a).prev().show();

                }

            });

            return false;

        });*/

        $('.summernote').summernote({height: 500});

        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        $('#select_user').change(function () {

            var this_sel = $(this).find('option:selected').data('email');
            $('#selected_user').val(this_sel);

        });

        setTimeout(tagsInputOverflowed, 500);


    });

    function tagsInputOverflowed(){
        $('.tagsinput').each(function(){
            $(this).toggleClass('scrolled', $(this)[0].offsetHeight < $(this)[0].scrollHeight);
        });
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tag-it/2.0/js/tag-it.min.js"></script>

<script>
$(document).ready(function() {

@if (isset($json_official) && isset($json_neof))

    LOG('main $json_official');

    $('input[name=official]').change(function() {

        if ($(this).is(':checked')) {

            console.log({!! $json_official !!});
            $('#courses_id').importTags('');

            $('select[name=user_id]')[0].selectedIndex = 0;
            $('.offdiv').html('<input id="courses_id" type="text" name="courses_id[]" class="form-control">');
            if ($('#courses_id').length) {
                $('#courses_id').tagsInput({
                    'onAddTag': tagsInputOverflowed,
                    'onRemoveTag': tagsInputOverflowed,
                    'interactive': true,
                    'autocomplete': {
                        source: {!! $json_official !!}
                    }
                });
            }
        }
        else {
            $('.offdiv').html('<input id="courses_id" type="text" name="courses_id[]" class="form-control">');
            if ($('#courses_id').length) {
                $('#courses_id').tagsInput({
                    'onAddTag': tagsInputOverflowed,
                    'onRemoveTag': tagsInputOverflowed,
                    'interactive': true,
                    'autocomplete': {
                        source: {!! $json_neof !!}
                    }
                });
            }
        }

    }).trigger('change');

    $('.add_tag').each(function() {
        $('#courses_id').addTag($(this).val());
    });

@elseif (isset($course_ids))

    LOG('main $course_ids');

    if ($('#courses_id').length) {
        $('#courses_id').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,
            'interactive': true,
            'autocomplete': {
                source: {!! $course_ids !!}
            }
        });
    }
    $('.add_tag').each(function() {
        $('#courses_id').addTag($(this).val());
    });

@endif


@if (isset($groups))

    LOG('main $groups');

    if ($('#groups_input').length) {
        $('#groups_input').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'interactive': true,
            'autocomplete': {
                source: {!! $groups !!}
            },

        });

    }

@endif


@if (isset($json_sounds))

    LOG('main $json_sounds');

    if ($('#sounds_input').length) {
        $('#sounds_input').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'interactive': true,
            'autocomplete': {
                source: {!! $json_sounds !!}
            }

        });

    }

@endif

@if (isset($my_sounds))

    LOG('main $my_sounds');

    if ($('#sounds_input').length) {
        $('#sounds_input').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'interactive': true,
            'autocomplete': {
                source: {!! $my_sounds !!}
            }

        });

    }

@endif



	$('input[name=official]').click(function() {

		if ($(this).is(':checked')) {
			$('select[name=user_id]')[0].selectedIndex = 0;
		}

	});

    $('#gaddf1').click(function () {

        var user = $('#user_id').val();
        var user_name = $('#user_id').find('option:selected').text();

        if(user != "0") {

            if ($("#users").val().length > 0) {
                $("#users").val($("#users").val() + "," + user);
            } else {
                $("#users").val(user);
            }

            var html = '<tr><td class="footable-visible">'+user_name+'</td></tr>';
            $('table.members').find('tbody').prepend(html);

            $('#user_id option[value=' + user + ']').remove();

            $(".modal-backdrop").remove();
            $("body").removeClass("modal-open");
            $("body").removeAttr("style");
            $('#addmember').hide();

        }
    });

    if ($('#coupons_id').length) {
        $('#coupons_id').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'interactive': true,
            'autocomplete': {
                source: ['{!! implode("', '", App\Coupon::$couponTitles) !!}'],
            },
            'limit': $('#coupons_id').data('limit'),

        });

    }

    $('.sortable').find('tbody').sortable();
    setInterval(function () {

        $('.sortable').find('tbody').find('tr').each(function (index) {

            $(this).find('input').val(index + 1);

        });

    }, 1000);

    $(document).off('click', 'a.btn-xs').on('click', 'a.btn-xs', function () {

        var this_href = $(this).attr('href');
        if (this_href.indexOf('_record') > 0) {

            if (confirm('@lang('common.delete_confirm')')) {
                return true;
            }
            else {
                return false;
            }

        }
        else {
            return true;
        }

    });
    $(document).on('click', 'a.btn-xs', function () {

        if ($(this).parent().hasClass('text-right')) {
            return true;
        }

        var this_href = $(this).attr('href');
        if (this_href.indexOf('_record') > 0) {

            if (confirm('@lang('common.delete_confirm')')) {
                return true;
            }
            else {
                return false;
            }

        }
        else {
            return true;
        }

    });


@if (isset($json))

    LOG('main $json');

    if ($('#cats_input').length) {

        $('#cats_input').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

        'autocomplete': {
            source: {!! $cats_names !!}
        },

        });
    }
    if ($('#cats_input1').length) {
        $('#cats_input1').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

        'autocomplete': {
        source: {!! $cats_names !!}
        }

        });
    }
    if ($('#cats_input2').length) {
        $('#cats_input2').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

        'autocomplete': {
        source: {!! $cats_names !!}
        }

        });
    }

    if ($('#cats_official').length) {

        $('#cats_official').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! isset($cats_names_official) ? $cats_names_official : '[]' !!}
            }

        });

    }
    if ($('#cats_not_official').length) {

        $('#cats_not_official').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! isset($cats_names_not_official) ? $cats_names_not_official : '[]' !!}
            }

        });

    }

    if ($('#sounds_input').length) {

        $('#sounds_input').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! $json !!}
            }

        });

    }
    if ($('#sounds_input2').length) {
        $('#sounds_input2').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! $json !!}
            }

        });

    }
    if ($('#sounds_input3').length) {
        $('#sounds_input3').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! $json !!}
            }

        });

    }

    $('.add_all').click(function () {

        var obj = {!! $json !!};

        var t_input = $(this).parent().parent().parent().find('input').first().next().attr('id');
        for (var i = 0; i < obj.length; i++) {
            $('#' + t_input).addTag(obj[i]);
        }

    });
    if ($('#courses_id').length) {
        $('#courses_id').tagsInput({
            'onAddTag': tagsInputOverflowed,
            'onRemoveTag': tagsInputOverflowed,

            'autocomplete': {
                source: {!! $json !!}
            }

        });
    }

    $('.tagsinp').change(function () {

        var append = $(this).data('to');
        append = $('#' + append);
        $(append).addTag($(this).val());
        $(this).val('');

    });

    /*
    function googleTranslateElementInit() {
      new google.translate.TranslateElement(
      {
          pageLanguage: 'ru',
          layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT,
          autoDisplay: true
      },
      'google_translate_element'
      );
    }
    */

@endif


@if(isset($cats_names_official))

    LOG('main $cats_names_official');

    $('input[name="official"]').change(function(){
        var officialOnly = $(this).prop('checked');
        $('.formOfficial').toggle(officialOnly);
        $('.formNotOfficial').toggle(!officialOnly);
    }).trigger('change');

@endif

});

function LOG(v) {
    if (!DEBUG)
        return false;

    switch (arguments.length) {
        case 1:
            console.log(arguments[0]);
            break;
        case 2:
            console.log(arguments[0], arguments[1]);
            break;
        case 3:
            console.log(arguments[0], arguments[1], arguments[2]);
            break;
    }
}
</script>
</body>



</html>
