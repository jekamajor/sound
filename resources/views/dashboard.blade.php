@extends('layouts.admin.main')

@section('content')
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">

        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			</div>
            <ul class="nav navbar-top-links navbar-right">
				<li>
					<a href="/{{ App::getLocale() }}/admin/trash"><i class="fa fa-trash"></i> @lang('common.trash')</a>
				</li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-language"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li><a href="javascript:void(0);">Русский</a></li>
                        <li><a href="javascript:void(0);">English</a></li>
					</ul>
				</li>
                <li>
                    <a href="javascript:void(0);" class="logout_do">
                        <i class="fa fa-sign-out"></i> @lang('common.logout')
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>@lang('dashboard.title')</h2>
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
					@if (session('success'))
						<div class="alert alert-danger">{{ session('success') }}</div>
					@endif					
                    <ol class="breadcrumb">
                        <li>
                            <a href="/{{ App::getLocale() }}/admin/">@lang('common.admin_panel')</a>
                        </li>
                        <li class="active">
                            <strong>@lang('dashboard.title')</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
					@if (Auth()->user()->id == 15)
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">&nbsp;</strong>
                             </span> <span class="text-muted text-xs block btn btn-danger" style="color: white; margin-top: 10px;">@lang('coupon.button') <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li><a href="/{{ App::getLocale() }}/admin/coupon">@lang('coupon.list')</a></li>
							<li><a href="/{{ App::getLocale() }}/admin/coupon/add">@lang('common.create_coupon')</a></li>
							<li><a href="/{{ App::getLocale() }}/admin/coupon/settings">@lang('coupon.settings')</a></li>
                        </ul>
					@endif
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ecommerce">
            <div class="row">			
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <table data-nosort="0" class="dataTable footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                                <thead>
                                <tr>
                                    <th data-toggle="true">
										<span>#</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_user')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_nr')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_coupon')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_price')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_expires')</span>
									</th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_status')</span>
									</th>
									<th data-toggle="true"><span>@lang('dashboard.table_invited')</span></th>
									<th data-toggle="true">
										<span>@lang('dashboard.table_activated')</span>
									</th>
                                </tr>
                                </thead>
                                <tbody>
									@if ($list->count())
										<?php $count = 0; ?>
										@foreach ($list as $key => $record)
											@if ($opts[$record->id]['coupon_status'] != 'active')
												@continue
											@endif
											<?php $count++; ?>
											<tr class="footable-odd">
												<td class="footable-visible">{{ $count }}</td>		
												<td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/users/info/{{ $record->id }}">{{ $record->name }}</a></td>
												<td class="footable-visible"><a href="/{{ App::getLocale() }}/admin/coupon/info/{{ $opts[$record->id]['coupon_id'] }}">{{ $opts[$record->id]['coupon_nr'] }}</a></td>
												<td class="footable-visible">
                                                    <span class="badge badge-info">{{ $opts[$record->id]['name'] }}</span>
												</td>
												<td class="footable-visible">{{ $opts[$record->id]['price'] }}</td>
												<td>
													@if (strtotime($opts[$record->id]['date_expires']))
														{{ date('d.m.Y - H:i', strtotime($opts[$record->id]['date_expires'])) }}
													@else
														&mdash;
													@endif
												</td>
												<td>
													@if ($opts[$record->id]['coupon_status'] == 'active')
														<span class="badge badge-success">@lang('coupon.active')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'pause')
														<span class="badge badge-warning">@lang('coupon.pause')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'expires')
														<span class="badge badge-danger">@lang('coupon.expires')</span>
													@endif
													@if ($opts[$record->id]['coupon_status'] == 'na')
														<span class="badge badge-primary">@lang('coupon.na')</span>
													@endif		
													@if ($opts[$record->id]['coupon_status'] == 'ended')
														<span class="badge badge-danger">@lang('coupon.ended')</span>
													@endif														
												</td>
												<td>{!! $opts[$record->id]['invited'] !!}</td>
												<td>
													@if (strtotime($opts[$record->id]['date_activated']))
														{{ date('d.m.Y - H:i', strtotime($opts[$record->id]['date_activated'])) }}
													@else
														&mdash;
													@endif
												</td>											
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="15">@lang('common.nodata')</td>
										</tr>
									@endif
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="footer">
            <div class="pull-right">
                
            </div>
            <div>
                
            </div>
        </div>
        </div>
@endsection