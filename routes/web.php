<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//$credentials = array('email' => 'admin@admin.com', 'password' => '123456');
//$credentials = array('email' => 'dimorl@gmail.com', 'password' => '123456');
//$credentials = array('email' => '3@mail.ru', 'password' => '123456');
//$credentials = array('email' => '4@mail.ru', 'password' => '123456');
//$credentials = array('email' => 'practic@mail.ru', 'password' => '123456');
//Auth::attempt($credentials, true);
/*
Route::get('/clear-cache', function () {
    echo 'config:clear:' . Artisan::call('config:clear') . '<br>';
    echo 'cache:clear:' . Artisan::call('cache:clear') . '<br>';
    echo 'config:cache:' . Artisan::call('config:cache') . '<br>';
    echo 'view:clear:' . Artisan::call('view:clear') . '<br>';
});

$credentials = array('email' => 'admin@admin.com', 'password' => '123456');
Auth::attempt($credentials, true);
*/

/* Удаление записей */
Route::get('/{locale}/admin/delete_record/{table}/{id}', 'AdminController@delete')->name('admin_delete');

Route::get('/admin', 'AdminUsersController@dash')->name('admin_main');
Route::get('/{locale}/admin', 'AdminUsersController@dash')->name('admin_main');
Route::post('/admin/ava', 'AdminUsersController@change_ava')->name('admin_main1');

/* Корзина */
Route::get('/{locale}/admin/trash', 'AdminTrashController@index')->name('admin_trash');
Route::get('/{locale}/admin/trash/restore/{table}/{id}', 'AdminTrashController@restore');
Route::get('/{locale}/admin/trash/delete/{table}/{id}', 'AdminTrashController@delete');

Route::get('/{locale}/admin/deletefg/{id}/{user_id}', 'AdminUsersController@deletefg');

/* Пользователи */
Route::get('/{locale}/admin/users', 'AdminUsersController@index')->name('admin_users');
Route::get('/{locale}/admin/users/add', 'AdminUsersController@add')->name('admin_uadd'); 
Route::post('/{locale}/admin/users/add', 'AdminUsersController@add')->name('admin_uadd');
Route::get('/{locale}/admin/users/edit/{id}', 'AdminUsersController@edit')->name('admin_uedit');
Route::post('/{locale}/admin/users/edit/{id}', 'AdminUsersController@edit')->name('admin_uedit');
Route::get('/{locale}/admin/users/info/{id}', 'AdminUsersController@info')->name('admin_uedit1');
Route::get('/{locale}/admin/users/delete_group/{id}', 'AdminUsersController@delete_group')->name('admin_uedit1');

Route::get('/{locale}/admin/groups', 'AdminUsersController@index_groups')->name('admin_groups');
Route::get('/{locale}/admin/groups/add', 'AdminUsersController@add_g')->name('admin_gadd'); 
Route::post('/{locale}/admin/groups/add', 'AdminUsersController@add_g')->name('admin_gadd');
Route::get('/{locale}/admin/groups/edit/{id}', 'AdminUsersController@edit_g')->name('admin_gedit');
Route::post('/{locale}/admin/groups/edit/{id}', 'AdminUsersController@edit_g')->name('admin_gedit');
Route::get('/{locale}/admin/groups/info/{id}', 'AdminUsersController@info_g')->name('admin_gedit1');

Route::get('/{locale}/admin/roles', 'AdminUsersController@index_roles')->name('admin_roles');
Route::get('/{locale}/admin/roles/add', 'AdminUsersController@add_r')->name('admin_radd'); 
Route::post('/{locale}/admin/roles/add', 'AdminUsersController@add_r')->name('admin_radd');
Route::get('/{locale}/admin/roles/edit/{id}', 'AdminUsersController@edit_r')->name('admin_redit');
Route::post('/{locale}/admin/roles/edit/{id}', 'AdminUsersController@edit_r')->name('admin_redit');
//Route::get('/admin/roles/info/{id}', 'AdminUsersController@info_g')->name('admin_gedit1');
 
/* Сотрудники */
Route::get('/{locale}/admin/emp', 'AdminUsersController@emp_index')->name('admin_emp');
Route::get('/{locale}/admin/emp/add', 'AdminUsersController@add_emp')->name('admin_eadd'); 
Route::post('/{locale}/admin/emp/add', 'AdminUsersController@add_emp')->name('admin_eadd');
Route::get('/{locale}/admin/emp/edit/{id}', 'AdminUsersController@edit_emp')->name('admin_eedit');
Route::post('/{locale}/admin/emp/edit/{id}', 'AdminUsersController@edit_emp')->name('admin_eedit');
Route::get('/{locale}/admin/emp/info/{id}', 'AdminUsersController@info_emp')->name('admin_eedit1');

/* Купоны */
Route::get('/{locale}/admin/coupon', 'AdminCouponController@index')->name('admin_coupon');
Route::get('/{locale}/admin/coupon/add', 'AdminCouponController@add')->name('admin_couadd'); 
Route::post('/{locale}/admin/coupon/add', 'AdminCouponController@add')->name('admin_couadd');
Route::get('/{locale}/admin/coupon/edit/{id}', 'AdminCouponController@edit')->name('admin_couedit');
Route::post('/{locale}/admin/coupon/edit/{id}', 'AdminCouponController@edit')->name('admin_couedit');
Route::get('/{locale}/admin/coupon/info/{id}', 'AdminCouponController@info')->name('admin_couedit1');
Route::get('/{locale}/admin/coupon/pause/{id}', 'AdminCouponController@pause')->name('admin_couedit1');
Route::get('/{locale}/admin/coupon/pause/{id}/{user_id}', 'AdminCouponController@upause')->name('admin_couedit2');
Route::get('/{locale}/admin/coupon/start/{id}', 'AdminCouponController@start')->name('admin_couedit1');
Route::get('/{locale}/admin/coupon/start/{id}/{user_id}', 'AdminCouponController@ustart')->name('admin_couedit3');

Route::get('/{locale}/admin/coupon/settings', 'AdminCouponController@settings')->name('admin_cs');
Route::post('/{locale}/admin/coupon/settings', 'AdminCouponController@settings')->name('admin_cs');

/* Плейлисты */
Route::get('/{locale}/admin/playlist/user/{id}', 'AdminUsersController@user_playlist')->name('admin_user_playlist');
Route::get('/{locale}/admin/playlist', 'AdminUsersController@playlist')->name('admin_playlist');
Route::get('/{locale}/admin/playlist/add/{user_id}', 'AdminUsersController@playlist_add')->name('admin_padd');
Route::post('/{locale}/admin/playlist/add/{user_id}', 'AdminUsersController@playlist_add')->name('admin_padd');
Route::get('/{locale}/admin/playlist/edit/{id}/{user_id}', 'AdminUsersController@playlist_edit')->name('admin_pedit');
Route::post('/{locale}/admin/playlist/edit/{id}/{user_id}', 'AdminUsersController@playlist_edit')->name('admin_pedit');

Route::get('/{locale}/admin/playlist/add', 'AdminUsersController@playlist_add1')->name('admin_padd1');
Route::post('/{locale}/admin/playlist/add', 'AdminUsersController@playlist_add1')->name('admin_padd1');
Route::get('/{locale}/admin/playlist/edit/{id}', 'AdminUsersController@playlist_edit1')->name('admin_pedit1');
Route::post('/{locale}/admin/playlist/edit/{id}', 'AdminUsersController@playlist_edit1')->name('admin_pedit1');
Route::get('/{locale}/admin/playlist/info/{id}', 'AdminUsersController@playlist_info')->name('admin_pedit2');

/* Категории */
Route::get('/{locale}/admin/cats', 'AdminCatController@index')->name('admin_cats');
Route::get('/{locale}/admin/cats/add', 'AdminCatController@add')->name('admin_cadd');
Route::post('/{locale}/admin/cats/add', 'AdminCatController@add')->name('admin_cadd');
Route::get('/{locale}/admin/cats/edit/{id}', 'AdminCatController@edit')->name('admin_cedit');
Route::post('/{locale}/admin/cats/edit/{id}', 'AdminCatController@edit')->name('admin_cedit');
Route::get('/{locale}/admin/cats/info/{id}', 'AdminCatController@info')->name('admin_cedit1');

/* Звуки */
Route::get('/{locale}/admin/sounds', 'AdminSoundController@index')->name('admin_sounds');
Route::get('/{locale}/admin/sounds/add', 'AdminSoundController@add')->name('admin_sadd');
Route::post('/{locale}/admin/sounds/add', 'AdminSoundController@add')->name('admin_sadd');
Route::get('/{locale}/admin/sounds/edit/{id}', 'AdminSoundController@edit')->name('admin_sedit');
Route::post('/{locale}/admin/sounds/edit/{id}', 'AdminSoundController@edit')->name('admin_sedit');
Route::get('/{locale}/admin/sounds/info/{id}', 'AdminSoundController@info')->name('admin_sedit1');

/* Курсы */
Route::get('/{locale}/admin/courses', 'AdminCourseController@index')->name('admin_courses');
Route::get('/{locale}/admin/courses/add', 'AdminCourseController@add')->name('admin_coadd');
Route::post('/{locale}/admin/courses/add', 'AdminCourseController@add')->name('admin_coadd');
Route::get('/{locale}/admin/courses/edit/{id}', 'AdminCourseController@edit')->name('admin_coedit');
Route::post('/{locale}/admin/courses/edit/{id}', 'AdminCourseController@edit')->name('admin_coedit');
Route::get('/{locale}/admin/courses/info/{id}', 'AdminCourseController@info')->name('admin_coedit1');
Route::post('/{locale}/admin/courses/info/{id}', 'AdminCourseController@info')->name('admin_coedit1');